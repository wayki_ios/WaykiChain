

import UIKit
import Alamofire
import SVProgressHUD

enum HUDStyle {
    case none
    case loading
}




// 请求数据
class LHRequest: NSObject {
    
    static let sharedSessionManager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        return Alamofire.SessionManager(configuration: configuration)
    }()
    
    // 注册专用Post
    class func post(url:String,parameters:Dictionary<String,Any>,runHUD:HUDStyle = .loading,success: @escaping ((_ json:JSON)->Void),failure:@escaping ((String)->Void) ){
        // 显示加载器
        if runHUD == .loading{ showHUD() }
        // 参数处理
        let parameter:Parameters = parameters

        Alamofire.request(url, method: .post,
                          parameters: parameter,
                          encoding: JSONEncoding.default,
                          headers:["Accept":"application/json","Content-Type":"application/json"]).responseJSON { (response) in
                            dismissHUD()
                            switch response.result.isSuccess {
                            case true:
                                if let value = response.result.value {
                                    if let dic = JSON(value).dictionary{
                                        if let status = dic["status"]?.int{
                                            if status == 1{
                                                success(JSON(value))
                                            }else{
                                                if let str = dic["error"]?.string{
                                                    failure(str)
                                                }
                                            }
                                        }
                                    }
                                }
                            case false:
                                failure("创建钱包失败".local)
                            }
        }
    }
    
    class func post(url:String,parameters:Dictionary<String,Any>,runHUD:HUDStyle = .loading,success: @escaping ((_ json:JSON)->Void),failure:@escaping (()->Void) ){
        // 显示加载器
        if runHUD == .loading{ showHUD() }
        // 参数处理
        let parameter:Parameters = parameters
      
        Alamofire.request(url, method: .post,
                          parameters: parameter,
                          encoding: JSONEncoding.default,
                          headers:["Accept":"application/json","Content-Type":"application/json"]).responseJSON { (response) in
                dismissHUD()
                switch response.result.isSuccess {
                case true:
                    if let value = response.result.value {
                        if let dic = JSON(value).dictionary{
                            if let status = dic["status"]?.int{
                                if status == 1{
                                    success(JSON(value))
                                }else{
                                    print(response)
                                    failure()
                                }
                            }
                        }
                    }
                case false:
                    failure()
                }
            }
        }
    
    class func get(url:String,parameters:Dictionary<String,Any>,runHUD:HUDStyle = .none,success: @escaping ((_ json:JSON)->Void),failure:@escaping ((_ error:Error)->Void) ){
        // 显示加载器
        if runHUD == .loading{ showHUD() }
        // 参数处理
        let parameter:Parameters = parameters
        Alamofire.request(url, method: .get, parameters: parameter, encoding: URLEncoding.default, headers:nil).responseJSON { (response) in
            dismissHUD()
            switch response.result.isSuccess {
            case true:
                if let value = response.result.value {
                    success(JSON(value))
                }
            case false:
                failure(response.result.error!)
            }
        }
    }
    
}

extension LHRequest{
    class func showHUD(){ SVProgressHUD.show() }
    
    class func dismissHUD(){ SVProgressHUD.dismiss() }
    
}

extension LHRequest{
    //获取最新区块高度
    class func getVaildHeight(symbol:String = "WICC", success: @escaping ((_ vaildHeight:Double,_ address:String,_ appid:String)->Void)){
        
        let path:String = httpPath(path: HTTPPath.获取最新区块高度_G)
        let requestPath:String = path + symbol
        
        LHRequest.get(url: requestPath,parameters: [:],runHUD: .loading, success: { (json) in
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {
                        if let error:String = dic["error"]?.string{
                            //print("error:\(error)")
                            HUD.flash(.label(error), onView: UIApplication.shared.keyWindow, delay: 1.2, completion: nil)
                        }
                    
                        return
                    }else if (dic["result"]?.dictionaryValue) != nil{
                        let json = dic["result"]
                        var height:Double? = 0
                        var address:String? = ""
                        var appid:String? = ""
                        if let val = json!["height"].double{ height = val }
                        if let val = json!["address"].string{ address = val }
                        if let val = json!["appid"].string{ appid = val }

                        let account:NewAccount = AccountManager.getAccount()
                        account.validHeight = height!
                        AccountManager.saveAccount(account: account)
                        success(height!,address!,appid!)
                        
                    }
                    
                }
                
            }
            
        }) { (error) in }
    }

}

extension LHRequest{
    class func getSumWealth(){
        let path:String = httpPath(path: HTTPPath.获取资产汇总_G)
        let address = AccountManager.getAccount().address
        let mHash = AccountManager.getAccount().mHash
        let requestPath = path + mHash + "/\(address)"
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        LHRequest.get(url: getPath,parameters: [:],runHUD: .loading, success: {(json) in

            let models = WealthSumModel.getModels(json: json)
            if models.count == 2{
                let account = AccountManager.getAccount()
                account.wiccSumAmount = WealthSumModel.getModels(json: json)[0].amount
                account.spcSumAmount = WealthSumModel.getModels(json: json)[1].amount
                AccountManager.saveAccount(account: account)
            }
            
        }) { (error) in }
    }
    
    class func getRegId(){
        let path:String = httpPath(path: HTTPPath.获取地址regid_G)
        let address = AccountManager.getAccount().address
        let mHash = AccountManager.getAccount().mHash
        let requestPath = path + mHash + "/" + address
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        LHRequest.get(url: getPath,parameters: [:],runHUD: .none, success: { (json) in
            
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {}
                }
                if let result = dic["result"]?.stringValue{
                    if result.count>0{
                        let account:NewAccount = AccountManager.getAccount()
                        account.regId = result
                        AccountManager.saveAccount(account: account)
                        return
                    }
                }
            }
        }) { (error) in }
    }

}

extension LHRequest {
    
    class func getSumWealth(hud:HUDStyle=HUDStyle.none,spcSumClosure:@escaping (Double)->Void){
        let path:String = httpPath(path: HTTPPath.获取资产汇总_G)
        let address = AccountManager.getAccount().address
        let mHash = AccountManager.getAccount().mHash
        let requestPath = path + mHash + "/\(address)"
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        LHRequest.get(url: getPath,parameters: [:],runHUD: hud, success: {(json) in
            
            let models = WealthSumModel.getModels(json: json)
            if models.count == 2{
                let account = AccountManager.getAccount()
                account.wiccSumAmount = WealthSumModel.getModels(json: json)[0].amount
                account.spcSumAmount = WealthSumModel.getModels(json: json)[1].amount
                AccountManager.saveAccount(account: account)
                for m in models{
                    if m.symbol == coinName{
                        spcSumClosure(m.amount)
                    }
                }
            }
            
        }) { (error) in }
    }
}


