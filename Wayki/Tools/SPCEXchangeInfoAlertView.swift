//
//  SPCEXchangeInfoAlertView.swift
//  Wayki
//
//  Created by sorath on 2018/6/11.
//  Copyright © 2018年 wk. All rights reserved.
//

//import UIKit
//
//class SPCEXchangeInfoAlertView: UIView {
//    var containerView:UIView!
//    var backgroundBtn:UIButton!
//    var sureAction:(()->Void)?
//    var lNum = 0
//    var allNUm = 0
//    var transferM:Int = 0
//
//    init(frame: CGRect,num:Int,allNum:Int,transfer:Int) {
//        super.init(frame: frame)
//        allNUm = allNum
//        lNum = num
//        transferM = transfer
//        containerView = createContainerView()
//        backgroundBtn = createBackBtn()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
//
////MARK:- event
//extension SPCEXchangeInfoAlertView{
//    func show(){
//        let keyWindow = UIApplication.shared.keyWindow
//        self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
//        addSubview(backgroundBtn)
//        addSubview(containerView)
//        
//        for vi in (keyWindow?.subviews)!{
//            if vi.isKind(of: SPCEXchangeInfoAlertView.self){
//                vi.removeFromSuperview()
//            }
//        }
//        keyWindow?.addSubview(self)
//        self.containerView.layer.opacity = 0.5
//        self.containerView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1.0)
//        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//            self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0.35)
//            self.containerView.layer.opacity = 1
//            self.containerView.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
//        }, completion: nil)
//        
//    }
//    
//    @objc func dismiss(){
//        self.layer.opacity = 1.0
//        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//            self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
//            self.containerView.layer.opacity = 0
//            self.containerView.layer.transform = CATransform3DMakeScale(0.6, 0.6, 1.0)
//        }) { (isSuccess) in
//            for vi in self.subviews{
//                vi.removeFromSuperview()
//            }
//            self.removeFromSuperview()
//        }
//        
//    }
//    
//    @objc func confirm(){
//        if sureAction != nil{
//            sureAction!()
//        }
//        dismiss()
//    }
//}
//
//
////MARK:- UI
//extension SPCEXchangeInfoAlertView{
//    
//    func createContainerView() -> UIView {
//        let containerV = UIView()
//        let vSize = getContainerCGSize()
//        containerV.frame = CGRect(x: (ScreenWidth - vSize.width)/2.0, y: (ScreenHeight - vSize.height)*2.0/5.0, width: vSize.width, height: vSize.height)
//        containerV.backgroundColor = UIColor.white
//        containerV.layer.cornerRadius = 6
//        containerV.layer.masksToBounds = true
//        addDetailInfo(superView: containerV)
//        return containerV
//    }
//    
//    func addDetailInfo(superView:UIView){
//        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: superView.width(), height: superView.width()*68/327))
//        titleLabel.text = "SPC" + "兑换".local
//        titleLabel.textColor = UIColor.black
//        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
//        titleLabel.textAlignment = .center
//        superView.addSubview(titleLabel)
//        
//        let detailSpace = scale*6
//        let detailWidth = superView.width() - 2*detailSpace
//        var detailHeight = scale*58
//        if ScreenWidth == 320 {
//            detailHeight = scale*70
//        }
//        let detailTop = 0 + titleLabel.bottom()
//        
//        let detailLabel = UILabel(frame: CGRect(x: detailSpace, y: detailTop, width: detailWidth, height: detailHeight))
//        detailLabel.font = UIFont.boldSystemFont(ofSize: 13)
//        detailLabel.textAlignment = .center
//        detailLabel.numberOfLines = 0
//        superView.addSubview(detailLabel)
//        
//        let numStr = "\(allNUm)"
//        let showStr = "花费\(transferM)SPC兑换一次\"幸运大转盘\"抽奖机会\n每日仅限兑换3次".local// + numStr
//        let attMuStr = NSMutableAttributedString(string: showStr, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 13)])
//        attMuStr.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.RGBHex(0xef3f50)], range: NSMakeRange(showStr.count-numStr.count, numStr.count))
//        attMuStr.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.gray], range: NSMakeRange(0, showStr.count))
//        let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineSpacing = 4
//        paragraphStyle.alignment  =  .center
//        attMuStr.addAttributes([NSAttributedStringKey.paragraphStyle : paragraphStyle], range: NSMakeRange(0, showStr.length()))
//        detailLabel.attributedText = attMuStr
//        
//        
//        let btnSpace = scale*16
//        let btnWidth = (superView.width() - 3*btnSpace)/2
//        let btnHeight = scale*44
//        let btnTop = superView.height() - scale*60
//        
//        let leftBtn = UIButton(type: .custom)
//        leftBtn.frame =  CGRect(x: btnSpace, y: btnTop, width: btnWidth, height: btnHeight)
//        leftBtn.setTitle("取消".local, for: .normal)
//        leftBtn.setTitleColor(UIColor.RGBHex(0x7e7e7e), for: .normal)
//        leftBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
//        leftBtn.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
//        leftBtn.layer.cornerRadius = 6
//        leftBtn.layer.borderWidth = 0.5
//        leftBtn.layer.borderColor = UIColor.RGBHex(0xc9c9c9).cgColor
//        superView.addSubview(leftBtn)
//        
//        
//        let rightBtn = UIButton(type: .custom)
//        rightBtn.frame =  CGRect(x: leftBtn.right()+btnSpace, y: btnTop, width: btnWidth, height: btnHeight)
//        rightBtn.setTitle("确定".local, for: .normal)
//        rightBtn.setTitleColor(UIColor.white, for: .normal)
//        rightBtn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
//        rightBtn.setBackgroundImage(UIImage(named: "assert_transfer_bgd"), for: .normal)
//        rightBtn.layer.cornerRadius = 6
//        rightBtn.clipsToBounds = true
//        rightBtn.addTarget(self, action: #selector(confirm), for: .touchUpInside)
//        superView.addSubview(rightBtn)
//    }
//    
//    
//    
//    func createBackBtn() -> UIButton {
//        let backBtn = UIButton(type: .custom)
//        backBtn.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
//        backBtn.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
//        backBtn.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
//        
//        return backBtn
//    }
//    
//    func getContainerCGSize() -> CGSize {
//        let viewWidth = ScreenWidth - scale*24*2
//        let viewHeight = viewWidth*208.0/327.0
//        return CGSize(width: viewWidth, height: viewHeight)
//    }
//}
