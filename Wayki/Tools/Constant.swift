

import UIKit


let kWindowCV = UIApplication.shared.keyWindow?.rootViewController?.view
let kWindowC = UIApplication.shared.keyWindow?.rootViewController

let ScreenHeight = UIScreen.main.bounds.height
let ScreenWidth = UIScreen.main.bounds.width
let naviHeight:CGFloat = UIDevice.isX() ? 88 : 64
let tabbarHeight:CGFloat = UIDevice.isX() ? 80 : 49

let kFontTypeThin:String =  "PingFangSC-Light"
let kFontTypeMedium:String =  "PingFangSC-Medium"
let kFontType:String = "PingFangSC-Regular"
let kFontItalic:String = "HelveticaNeue-ThinItalic"
let kFontBold:String = "PingFangSC-Semibold"




class Constant: NSObject {

}


