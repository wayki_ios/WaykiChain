//
//  AppPagesControlView.swift
//  Wayki
//
//  Created by sorath on 2018/6/13.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class AppPagesControlView: UIView {
    
    var allNum:Int = 0
    var selectedNum  =  0
    
    var btns:[UIButton] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



//MARK: - UI
extension AppPagesControlView{
    func layoutUI(){
        self.backgroundColor = UIColor.clear
        
    }
    
    func createBtn() ->UIButton{
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: "app_page_s"), for: .normal)
        btn.setImage(UIImage(named: "app_page"), for: .selected)
        self.addSubview(btn)
        return btn
    }
}

extension AppPagesControlView{
    func addBtns(){
        for btn in btns {
            btn.removeFromSuperview()
        }
        let btnHeight:CGFloat = 15
        let btnAllHeight = btnHeight*CGFloat(allNum)
        let firstY = (frame.height - btnAllHeight - naviHeight)/2.0
        
        btns.removeAll()
        if allNum > 0 {
            for i in 1...(allNum){
                let btn = createBtn()
                btn.tag = i
                if i == 1{
                    btn.isSelected = true
                }
                btn.frame = CGRect(x: 0, y: firstY + btnHeight*CGFloat(i-1) , width: frame.width, height: btnHeight)
                btns.append(btn)
            }
        }

    }
    func movetoIndex(index:Int){
        for btn in btns{
            if btn.tag == index{
                btn.isSelected = true
            }else{
                btn.isSelected = false
            }
        }
    }
}
