

import UIKit

@objc protocol InputPwdViewDelegate:NSObjectProtocol{
    func sureBtn(obj:InputPwdView,pwdTf:UITextField)
    func keyboradShow()
    func keyboradHide()
    func closeView()
}

class InputPwdView: UIScrollView {

    weak var alertDelegate:InputPwdViewDelegate?
    
    fileprivate lazy var texttile1 = InputPasswordTextFiled.init()
    fileprivate lazy var addressLabel = UILabel.init()
    fileprivate lazy var errMsgLabel = UILabel.init()
    fileprivate lazy var sure = UIButton.init()
    
    init(frame: CGRect,title:String) {
        super.init(frame: frame)
        configView(frame: frame, title: title)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func configView(frame:CGRect,title:String){
        
        let widthScale = ScreenWidth/375
        let x = 24 * widthScale
        
        let contentWidth = widthScale * 327
        let contentheight = contentWidth * 0.73
        
        let label1 = UILabel.init(frame: CGRect(x: 20, y: contentheight * 0.1666, width: contentWidth-40, height: contentheight * 0.1166))
        label1.text = title
        label1.font = UIFont.systemFont(ofSize: 20)
        label1.textAlignment = .center
        self.addSubview(label1)
        
        
        texttile1.frame = CGRect(x: x, y: label1.bottom()+contentheight*0.129, width: label1.width(), height: 22)
        texttile1.placeholder = "请输入钱包密码".local
        texttile1.text = ""
        texttile1.becomeFirstResponder()
        texttile1.keyboardType = .asciiCapable
        texttile1.isSecureTextEntry = true
        texttile1.font = UIFont.systemFont(ofSize: 16)
        texttile1.delegate = self
        texttile1.returnKeyType = .done
        
        self.addSubview(texttile1)
        
        let line = UIView.init(frame: CGRect(x: x, y: texttile1.bottom() + contentheight * 0.0625, width: contentWidth-2*x, height: 0.5))
        line.backgroundColor = UIColor.init(white: 0.85, alpha: 1)
        self.addSubview(line)
        
        errMsgLabel.frame = CGRect(x: x, y: line.bottom() + 5, width: line.width(), height: 15)
        errMsgLabel.textColor = .red
        errMsgLabel.font = UIFont.systemFont(ofSize: 10)
        errMsgLabel.alpha = 0
        errMsgLabel.text = "密码错误,请重新输入".local
        self.addSubview(errMsgLabel)
        
        
        sure.frame = CGRect(x: contentWidth/4 , y: contentheight - contentheight * 0.2 - contentheight*0.0727, width:contentWidth/2, height: contentheight*0.2)
        sure.setBackgroundImage(UIImage.init(named: "wallet_button_normal"), for: .normal)
        sure.setTitle("确认".local, for: .normal)
        sure.isEnabled = false
        sure.layer.cornerRadius = 6
        sure.clipsToBounds = true
        sure.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        sure.addTarget(self, action: #selector(clicksure), for: .touchUpInside)
        self.addSubview(sure)
        
        let exit = UIButton.init(frame: CGRect(x:contentWidth-44, y: 0, width: 44, height:44))
        exit.setImage(UIImage.init(named: "alert_cancel"), for: .normal)
        exit.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        exit.addTarget(self, action: #selector(exitAlert), for: .touchUpInside)
        self.addSubview(exit)
        
        let label2 = UILabel.init(frame: CGRect(x: 20, y: contentheight + x, width: contentWidth-40, height: contentheight * 0.1166))
        label2.text = "导出私钥".local
        label2.font = UIFont.systemFont(ofSize: 20)
        label2.textAlignment = .center
        self.addSubview(label2)
        
        
        addressLabel.frame = CGRect(x: x, y: label2.bottom() + x, width: contentWidth - 2*x, height: 42)
        addressLabel.text = ""
        addressLabel.textColor = UIColor.RGBHex(0xB2B2B2)
        addressLabel.textAlignment = .center
        addressLabel.font = UIFont.systemFont(ofSize: 16)
        addressLabel.numberOfLines = 2
        self.addSubview(addressLabel)

        let warmming = UILabel.init(frame: CGRect(x: x, y: addressLabel.bottom() + 5, width: contentWidth - 2*x, height: 42))
        warmming.text = "警告：私钥未加密，泄漏风险极大，请保存在安全位置！".local
        warmming.textColor = UIColor.RGBHex(0xEF3F50)
        warmming.font = UIFont.systemFont(ofSize: 10)
        warmming.numberOfLines = 2
        warmming.textAlignment = .center
        self.addSubview(warmming)
        
        
        let exit2 = UIButton.init(frame: CGRect(x:contentWidth-44, y: contentheight, width: 44, height:44))
        exit2.setImage(UIImage.init(named: "alert_cancel"), for: .normal)
        exit2.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        exit2.addTarget(self, action: #selector(exitAlert), for: .touchUpInside)
        self.addSubview(exit2)
        
        let sure2 = UIButton.init(frame: CGRect(x: contentWidth/4 , y: contentheight*2 - contentheight * 0.2 - contentheight*0.0727, width:contentWidth/2, height: contentheight * 0.2))
        sure2.setBackgroundImage(UIImage.init(named: "wallet_button_normal"), for: .normal)
        sure2.setTitle("复制密钥".local, for: .normal)
        sure2.layer.cornerRadius = 6
        sure2.clipsToBounds = true
        sure2.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        sure2.addTarget(self, action: #selector(copySecreate), for: .touchUpInside)
        self.addSubview(sure2)
        
        self.contentSize = CGSize.init(width: contentWidth, height: 2*contentheight)
        self.isScrollEnabled = false
    }
    @objc func copySecreate(){
        UIPasteboard.general.string = addressLabel.text
        exitAlert()
        UmengEvent.eventWithDic(name: "import_privatekey")
        UILabel.showSucceedHUD(text: "已成功复制到剪贴板".local)
    }
    @objc func clicksure(){

        //校验密码
        let pwdIsTrue = AccountManager.getAccount().checkPassword(inputPassword: texttile1.text!)
        if !pwdIsTrue{
            UIView.animate(withDuration: 0.3) {
                self.errMsgLabel.alpha = 1
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.3) {
                UIView.animate(withDuration: 0.3) {
                    self.errMsgLabel.alpha = 0
                }
            }
            return
        }
        
        
        let account = AccountManager.getAccount()
        let helpStr = account.getHelpString(password: texttile1.text!)
        let secreatKet = Bridge.getAddressAndPrivateKey(withHelp: helpStr, password: texttile1.text!)
        let stringKey = secreatKet?.last as? String
        addressLabel.text = stringKey
        if let dele = self.alertDelegate{
            dele.sureBtn(obj: self,pwdTf: texttile1)
        }
        
        
    }
    @objc func exitAlert(){
        AlertManager.shared.closeAlert(contenView: self)
        if let dele = self.alertDelegate{
            dele.closeView()
        }
    }
}
extension InputPwdView:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if let dele = self.alertDelegate{
            dele.keyboradShow()
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str:NSString = textField.text! as NSString
        let showStr = str.replacingCharacters(in: range, with: string)
        if showStr.count == 0{
            sure.isEnabled = false
        }else{
            sure.isEnabled = true
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let dele = self.alertDelegate{
            dele.keyboradHide()
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}
