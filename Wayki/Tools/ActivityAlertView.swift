//
//  ActivityAlertView.swift
//  Wayki
//
//  Created by sorath on 2018/5/28.
//  Copyright © 2018年 JuFeng. All rights reserved.
//

import UIKit

class ActivityAlertView: UIView {
    var imageS:UIImage?
    var activeUrl:String? = ""
    var containerView:UIView!
    var backgroundBtn:UIButton!
    var closeBtn:UIButton!
    var jumpAction:((String)->Void)?
    
    init(frame: CGRect,image:UIImage,activeUrlS:String) {
        super.init(frame: frame)
        imageS = image
        activeUrl = activeUrlS
        containerView = createContainerView()
        backgroundBtn = createBackBtn()
        addCloseBtn()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:- event
extension ActivityAlertView{
    func show(){
        let keyWindow = UIApplication.shared.keyWindow
        
        for vi in (keyWindow?.subviews)!{
            if vi.isKind(of: ActivityAlertView.self){
                vi.removeFromSuperview()
            }
        }

        self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
        addSubview(backgroundBtn)
        addSubview(containerView)
        
        keyWindow?.addSubview(self)
        self.containerView.layer.opacity = 0.5
        self.containerView.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1.0)
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0.8)
            self.containerView.layer.opacity = 1
            self.containerView.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        }, completion: nil)
        
    }
    
    @objc func dismiss(){
        self.layer.opacity = 1.0
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
            self.containerView.layer.opacity = 0
            self.closeBtn.layer.transform = CATransform3DMakeScale(0.6, 0.6, 1.0)
            self.containerView.layer.transform = CATransform3DMakeScale(0.6, 0.6, 1.0)
        }) { (isSuccess) in
            for vi in self.subviews{
                vi.removeFromSuperview()
            }
            self.removeFromSuperview()
        }
        
    }
    
    @objc func confirm(){
        if jumpAction != nil{
            jumpAction!(activeUrl!)
        }
        dismiss()
    }
}


//MARK:- UI
extension ActivityAlertView{

    func createContainerView() -> UIView {
        let containerV = UIView()
        let vSize = getContainerCGSize()
        containerV.frame = CGRect(x: (ScreenWidth - vSize.width)/2.0, y: (ScreenHeight - vSize.height)*5/10.0, width: vSize.width, height: vSize.height)
        containerV.backgroundColor = UIColor.white
        containerV.layer.cornerRadius = 10
        containerV.layer.masksToBounds = true
        addDetailInfo(superView: containerV)
        return containerV
    }
    func addCloseBtn(){
        closeBtn = UIButton(type: .custom)
        let closeBtnWidth = ScreenWidth*60.0/750.0
        closeBtn.frame = CGRect(x: containerView.right() - closeBtnWidth, y: containerView.top()-closeBtnWidth-scale*10, width: closeBtnWidth, height: closeBtnWidth)
        closeBtn.setBackgroundImage(UIImage(named: "active_close"), for: .normal)
        closeBtn.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        backgroundBtn.addSubview(closeBtn)
    }
    
    func addDetailInfo(superView:UIView){

        
        let bgImageView = UIImageView()

        bgImageView.frame = CGRect(x: 0, y: 0, width: superView.width(), height: superView.height())
        superView.addSubview(bgImageView)

        if imageS != nil {
            bgImageView.image = imageS
        }
    
        
        bgImageView.isUserInteractionEnabled = true
        let clickTop = superView.height()*5.0/6.0
        let clickHeight = superView.height()/6.0
        let clickView = UIView(frame: CGRect(x: 0, y: clickTop, width: superView.width(), height: clickHeight))
        let tap = UITapGestureRecognizer(target: self, action: #selector(confirm))
        clickView.addGestureRecognizer(tap)
        bgImageView.addSubview(clickView)
        
    }
    
   
    
    func createBackBtn() -> UIButton {
        let backBtn = UIButton(type: .custom)
        backBtn.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
        backBtn.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
        //backBtn.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        
        return backBtn
    }
    
    func getContainerCGSize() -> CGSize {
        let scale = (imageS?.size.height)!/(imageS?.size.width)!
        let viewWidth = ScreenWidth*416.0/512.0
        var viewHeight = viewWidth*scale
        if viewHeight>ScreenHeight {
            viewHeight = ScreenHeight
        }
        
        return CGSize(width: viewWidth, height: viewHeight)
    }
}
