

import UIKit


// MARK:- 语言本地化
extension String {
    var local: String {
        return NSLocalizedString(self, comment: self)
    }
    
    func localS() -> String {
        return NSLocalizedString(self, comment: self)
    }
    
    func underLine()->NSAttributedString {
        let attrs = [NSAttributedStringKey.font:UIFont(name: kFontType, size: 13) as Any,
                     NSAttributedStringKey.foregroundColor:UIColor.white,
                     NSAttributedStringKey.underlineStyle:1] as [NSAttributedStringKey : Any]
        return NSAttributedString(string: self, attributes: attrs)
    }
    
//    / 字符串解密
//    func encryptingString(str:String)->String{
//        return SecurityUtil.encryptAESData(str)
//    }
//
//    /// 字符串加密
//    func decryptingString(str:String)->String {
//        return SecurityUtil.decryptAESData(str)
//    }
    
    //MARK:- 去除字符串两端的空白字符
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    //MARK:- 字符串长度
    func length() -> Int {
        return self.characters.count
    }
    
    func indexOf(_ target: Character) -> Int? {
        return self.index(of: target)?.encodedOffset
    }
    
    func subString(to: Int) -> String {
        let endIndex = String.Index.init(encodedOffset: to)
        let subStr = self[self.startIndex..<endIndex]
        return String(subStr)
    }
    
    func subString(from: Int) -> String {
        let startIndex = String.Index.init(encodedOffset: from)
        let subStr = self[startIndex..<self.endIndex]
        return String(subStr)
    }
    
    func subString(start: Int, end: Int) -> String {
        let startIndex = String.Index.init(encodedOffset: start)
        let endIndex = String.Index.init(encodedOffset: end)
        return String(self[startIndex..<endIndex])
    }
    
    func subString(range: Range<String.Index>) -> String {
        return String(self[range.lowerBound..<range.upperBound])
    }
    
    func removeLost0() ->String{
        let testNumber = self
        var outNumber = testNumber
        var i = 1
        if testNumber.contains("."){
            while i < testNumber.count{
                if outNumber.last == "0"{
                    outNumber.removeLast()
                    i = i + 1
                }else{
                    break
                }
            }
            if outNumber.hasSuffix("."){
                outNumber.removeLast()
            }
            return outNumber
        }
        else{
            return testNumber
        }
    }
    
    
    func toFloat()->(CGFloat){
        let string = self
        var cgFloat: CGFloat = 0
        if let doubleValue = Double(string){
            cgFloat = CGFloat(doubleValue)
        }
        return cgFloat
    }
    
    
    func toInt()->(Int){
        let string = self
        var int: Int?
        if let doubleValue = Int(string) {
            int = Int(doubleValue)
        }
        
        if int == nil{ return 0}
        return int!
    }
    
    func formartInputMoney(inputMoney:String,inputCharacter:String) -> Bool {
        let characters = ".0123456789"
        if (characters.range(of: inputCharacter) != nil)||(inputCharacter == "") {
            if (inputMoney.contains("."))&&(inputCharacter.contains(".")) {
                
                return false
            }
            
            return true
        }else{
            return false
        }
        
    }

    
}


// MARK:- 设备判断器
extension UIDevice {
    
    class func isX() -> Bool {
        if UIScreen.main.bounds.height == 812 {return true}
        return false
    }
    
    /** 返回：5，6，p,x */
    class func model() -> String{
        switch UIScreen.main.bounds.size.width {
        case 320:
            return "5"
        case 375:
            if UIScreen.main.bounds.height == 812 {return "x"}
            return "6"
        case 414:
            return "p"
        default:
            return "x"
        }
    }
    
}

// MARK:- 颜色
extension UIColor {
    
    class func RGBHex(_ rgbValue:Int,alpha:CGFloat=1.0) ->(UIColor){
        return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,alpha: alpha)
    }
    
    class func RGB(r:CGFloat,g:CGFloat,b:CGFloat,alpha:CGFloat=1.0) ->UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alpha)
    }
    
    class func grayColor(v:CGFloat)->UIColor{
        return UIColor(red: v/255.0, green: v/255.0, blue: v/255.0, alpha: 1.0)
    }
    
    /* 绿色字体 */
    class func green_deep()->UIColor{return RGBHex(0x0eb856)}
    /* 新版红色 */
    class func red_new()->UIColor{return RGBHex(0xef3f50)}
    
    class func lightGray() ->UIColor{return RGBHex(0xb2b2b2)}
}

// MARK:- 日期
extension Date{
    
    static func year()->String{
        return String(describing:  self.calender().year!)
    }
    
    static func month()->String{
        return String(describing:  self.calender().month!)
    }
    
    static func day()->String{
        return String(describing: self.calender().day!)
    }
    
    static func hour()->String{
        return String(describing: self.calender().hour!)
    }
    
    static func min()->String{
        return String(describing: self.calender().minute!)
    }
    
    private static func calender()->DateComponents{
        let calendar = Calendar(identifier: .gregorian)
        var comps: DateComponents = DateComponents()
        comps = calendar.dateComponents([.year,.month,.day, .weekday, .hour, .minute,.second], from: Date())
        return comps
    }
    func dateString()->String {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd HH:mm"
        return formater.string(from: self)
    }
    
    func monthString()->String {
        let formater = DateFormatter()
        formater.dateFormat = "yyyy-MM-dd"
        return formater.string(from: self)
    }
    
    func hourString()->String {
        let formater = DateFormatter()
        formater.dateFormat = "HH:mm:ss"
        return formater.string(from: self)
    }
    func dayString()->String {
        let formater = DateFormatter()
        formater.dateFormat = "MM-dd HH:mm"
        //let subs = formater.string(from: self).components(separatedBy: " ")
        return formater.string(from: self)
    }
    
    //获取东八区时间
    static func gUTCFormatDate(date:Date) ->Date{
    
        let dateFormatter = InstanceTools.instance.dateFormatter
        let datestr = dateFormatter?.string(from: date)
    
        return dateFormatter!.date(from: datestr!)!
    }
    
    
}


// MARK:-
extension UITableView{
    func adaptiOS11(table:UITableView){
        if #available(iOS 11.0, *) {
            table.contentInsetAdjustmentBehavior = .never
            table.contentInset = UIEdgeInsetsMake(0, 0, 49, 0)//导航栏如果使用系统原生半透明的，top设置为64
            table.scrollIndicatorInsets = table.contentInset
            table.estimatedRowHeight = 0;
            table.estimatedSectionHeaderHeight = 0;
            table.estimatedSectionFooterHeight = 0;
        }
    }
}

// MARK:- 动画
extension CATransition{
    class func push() -> CATransition{
        let tran = CATransition.init()
        tran.duration = 0.2
        tran.timingFunction = CAMediaTimingFunction.init(name: kCAAlignmentCenter)
        tran.type = kCAFillModeForwards
        tran.subtype = kCATruncationStart
        return tran
    }
}


//MARK: - Array
extension Array where Element: Equatable {
    mutating func remove(_ object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

import SVProgressHUD

extension UILabel {
    
    class func showSucceedHUD(text:String){
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.showSuccess(withStatus: text)
    }
   
    class func showFalureHUD(text:String){
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.showError(withStatus: text)
    }
    class func showTextHUD(text:String){
        SVProgressHUD.setMinimumDismissTimeInterval(0.5)
        SVProgressHUD.showInfo(withStatus: text)
    }
}

extension UIImage{
    
    class func QRwithString(string: String, imageName: String) -> UIImage{
        
        let stringData = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        //创建一个二维码的滤镜
        let qrFilter = CIFilter(name: "CIQRCodeGenerator")
        qrFilter?.setValue(stringData, forKey: "inputMessage")
        qrFilter?.setValue("H", forKey: "inputCorrectionLevel")
        let qrCIImage = qrFilter?.outputImage
        
        // 创建一个颜色滤镜,黑白色
        let colorFilter = CIFilter(name: "CIFalseColor")!
        colorFilter.setDefaults()
        colorFilter.setValue(qrCIImage, forKey: "inputImage")
        
        colorFilter.setValue(CIColor(red: 0, green: 0, blue: 0), forKey: "inputColor0")
        colorFilter.setValue(CIColor.init(red: 0, green: 0, blue: 0, alpha: 0), forKey: "inputColor1")
        // 返回二维码image
        let codeImage = UIImage(ciImage: (colorFilter.outputImage!.transformed(by: CGAffineTransform(scaleX: 5, y: 5))))
        
        // 中间一般放logo
        if let iconImage = UIImage(named: imageName) {
            let rect = CGRect(x: 0, y: 0, width: codeImage.size.width, height: codeImage.size.height)
            
            UIGraphicsBeginImageContext(rect.size)
            codeImage.draw(in: rect)
            let avatarSize = CGSize(width: rect.size.width*0.25, height: rect.size.height*0.25)
            
            let x = (rect.width - avatarSize.width) * 0.5
            let y = (rect.height - avatarSize.height) * 0.5
            iconImage.draw(in: CGRect(x: x, y: y, width: avatarSize.width, height: avatarSize.height))
            
            let resultImage = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            return resultImage!
        }
        return codeImage
    }
    
}

extension NSObject{
   /* 获取对象对于的属性值，无对于的属性则返回NIL
    
    - parameter property: 要获取值的属性
    
    - returns: 属性的值
    */
    func getValueOfProperty(property:String)->AnyObject?{
        let allPropertys = self.getAllPropertys()
        if(allPropertys.contains(property)){
            return self.value(forKey: property) as AnyObject
            
        }else{
            return nil
        }
    }
    
    /**
     设置对象属性的值
     
     - parameter property: 属性
     - parameter value:    值
     
     - returns: 是否设置成功
     */
   @discardableResult func setValueOfProperty(property:String,value:AnyObject)->Bool{
        let allPropertys = self.getAllPropertys()
        if(allPropertys.contains(property)){
            self.setValue(value, forKey: property)
            return true
            
        }else{
            return false
        }
    }
    
    /**
     获取对象的所有属性名称
     
     - returns: 属性名称数组
     */
    func getAllPropertys()->[String]{
        
        var result = [String]()
        var count:UInt32 = 0
        //获取类属性列表

        let buff = class_copyPropertyList(object_getClass(self), &count)
        let countInt = Int(count)
        for i in 0..<countInt {
            //temp 获取属性
            let temp = buff![i]
            //获取属性名
            let tempPro = property_getName(temp)
            //转换为string
            let proper = String(utf8String: tempPro)
            result.append(proper!)
        }
        

        
        return result
    }
    
    
    //下载图片
    class  func downloadImage(url: URL, completion: @escaping((UIImage)->Void)) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                let image = UIImage(data: data)
                completion(image!)
            }
        }
    }
    
    class  func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}

