

#import <UIKit/UIKit.h>
#import "TableViewAnimationKitConfig.h"

@interface UITableView (XSAnimationKit)

/**
 show the tableView animation 
 
 @param animationType type of animation to show TableView
 */
- (void)xs_showTableViewAnimationWithType:(XSTableViewAnimationType )animationType;


@end
