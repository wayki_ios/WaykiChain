

import UIKit

///防止多次点击抽奖界面，推出多个控制器
var viewCount:Int = 0

//最小锁仓金额
let minLPAmount:Double = 1

let coinName = "WKD" //"WKD"

// MARK:- 配置主机信息
let netpro = "https://"
let hostName = ""
let port = ":"

let allimagePath:String = ""
var nodeURL = ""

// 配置服务器
func httpPath(path:HTTPPath)->String{ return  netpro+hostName+port+path.rawValue }

// MARK:- 配置的通用URL
let 获取更多WiccURL = "https://XXX.html"
let 帮助中心URL = "https://XXXp.html"

let appVersion:String =  Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String

// MARK:- 配置管理员
let 蒲公英AppID:String = ""
let 友盟AppKey:String = ""
let 盒子AppKey:String = ""


// MARK:- App的三方库账号配置
class ConfigureManager: NSObject {
    
    static func configureLibrary(){
        ConfigureManager.配置友盟()
        ConfigureManager.配置更新 { (ifNeededAlertActive) in }
        ConfigureManager.配置蒲公英()
        ConfigureManager.配置3DTouch()
        ConfigureManager.配置盒子()
    }

}

// MARK:- 配置盒子、蒲公英、友盟
extension ConfigureManager{
    
    static func 配置盒子(){
        let hezi = HeziSDKManager.sharedInstance()
        hezi?.configureKey(盒子AppKey)
        hezi?.openDebug(false)
        hezi?.setNavigationBarBackgroundColor(.white)
        hezi?.setNavigationTinterColor(.black)
        hezi?.setNavagitionItemWithBack(nil, share: UIImage.init(), reflush: nil)
        hezi?.initialize()
    }
    
    static func 配置蒲公英(){
        PgyManager.shared().start(withAppId:蒲公英AppID )
        PgyManager.shared().isFeedbackEnabled = false
    }
    
    static func 配置友盟(){
        UMConfigure.initWithAppkey(友盟AppKey, channel: "官网")
        UMConfigure.setEncryptEnabled(true)
        UMConfigure.setLogEnabled(true)
    }
    
}

// MARK:- 配置3DTouch
extension ConfigureManager{
    
    static func 配置3DTouch(){
        let qrCode = UIApplicationShortcutItem(type: "receive", localizedTitle: "收款",localizedSubtitle: "收款地址",icon: UIApplicationShortcutIcon.init(templateImageName: "touch_qr"))
        let transfer = UIApplicationShortcutItem(type: "transfer", localizedTitle: "转账",localizedSubtitle: "快速转账",icon: UIApplicationShortcutIcon.init(templateImageName: "touch_transfer"))
        UIApplication.shared.shortcutItems = [qrCode,transfer]
    }

    static func handle3DTouchAction(type:String){// 3D Touch 事件处理
        if AccountManager.getAccount().address == ""{ return }
        if type == "receive"{
            let c = MyQRcodeC()
            NavigationManager.shared.drawerC?.closeDrawer(animated: true, completion: nil)
            NavigationManager.shared.centerC?.pushViewController(c, animated: false)
        }else if type == "transfer" {
            let c = MyTransferC()
            NavigationManager.shared.drawerC?.closeDrawer(animated: true, completion: nil)
            NavigationManager.shared.centerC?.pushViewController(c, animated: false)
        }
    }

}

// MARK:- 配置更新
enum UpdateType:Int{
    case 强制 = 0
    case 提示 = 1
}

class UpdateM:NSObject {
    var 内容:String = ""
    var 强制升级:Bool = false
    var 版本名称:String = ""
    var 更新时间:String = ""
    var 下载路径:String = ""
    var 版本号:String = ""
    
    class func analysisData(json:JSON)->UpdateM{
        let model = UpdateM()
        if let dic = json.dictionary{
            if let status = dic["status"]?.int{
                if status == 1{ // 获取数据成功
                    if let result = dic["result"]?.dictionary{
                        if let content = result["content"]?.string{
                            model.内容 = content.replacingOccurrences(of: "<p>", with: "\r\n")
                        }
                        if let content = result["forceupdate"]?.bool{
                            model.强制升级 = content
                        }
                        if let content = result["name"]?.string{
                            model.版本名称 = content
                        }
                        if let content = result["time"]?.string{
                            model.更新时间 = content
                        }
                        if let content = result["url"]?.string{
                            model.下载路径 = content
                        }
                        if let content = result["version"]?.string{
                            model.版本号 = content
                        }
                    }
                }
            }
        }
        return model
    }
}

extension ConfigureManager{
    
    class func 配置更新(complete:@escaping((Bool)->Void)){
        let path = httpPath(path: .获取最新移动版本_G)+"2"
        LHRequest.get(url: path, parameters: [:], success: {  (json) in
            showAlert(model: UpdateM.analysisData(json: json), complete: { (isUpdateSuccess) in
                //若果更新点击取消 或者 不需要更新，则返回需要弹出活动弹框
                //不更新 isUpdateSuccess返回false
                if isUpdateSuccess == false{
                        complete(true)
                }
            })
        }) { (error) in }
    }
    
    class func showAlert(model:UpdateM,complete:@escaping((Bool)->Void)){
        if model.版本号 == appVersion {
            complete(false)
            return
        }
        
        let cancleAction = UIAlertAction(title: "取消".local, style: .cancel) { (action) in
            if model.强制升级 == true{
                showAlert(model: model, complete: complete)
                return
            }
            complete(false)
        }
        
        let sureAction = UIAlertAction(title: "更新".local, style: .default) { (action) in
            if let url = URL(string: model.下载路径){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
            complete(true)
        }

        let alertC = UIAlertController(title: "版本更新".local, message: model.内容, preferredStyle: .alert)
        alertC.addAction(cancleAction)
        alertC.addAction(sureAction)
        UIApplication.shared.keyWindow?.rootViewController?.present(alertC, animated: true, completion: nil)
    }
}

// MARK:- 友盟统计
class UmengEvent {
    ///计数事件
    class func eventWithDic(name:String,dic:[String:Any]? = nil){
        if dic == nil{
            MobClick.event(name)
            return
        }
        MobClick.event(name, attributes: dic)
    }
    ///计算事件
    class func eventWithDicAndCount(name:String,dic:[String:Any],number:Int){
        let numberkey = "__ct__"
        let d = NSMutableDictionary.init(dictionary: dic)
        d.setValue(String.init(format: "%d", number), forKey: numberkey)
        MobClick.event(name, attributes: d as! [AnyHashable : Any])
    }
}
