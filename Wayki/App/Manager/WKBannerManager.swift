//
//  WKBannerManager.swift
//  Wayki
//
//  Created by louis on 2018/4/8.
//  Copyright © 2018年 JuFeng. All rights reserved.
//

import UIKit

class WKBannerM:NSObject{
    var 内容:String = ""
    var 标题:String = ""
    var 图片路径:String = ""
    var 上传时间:String = ""
    
    class func getModels(json:JSON)->[WKBannerM]{
        var array:[WKBannerM] = []
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{if status != 1 {return array }}
            if let results = dic["result"]?.array{
                for res in results {
                    let model = WKBannerM()
                    model.getModel(json:res)
                    array.append(model)
                }
            }
        }
        return array
    }
    
    func getModel(json:JSON){
        if let sep = json["detail"].string{
            if 内容.contains("="){
                内容 =  sep + AccountManager.getAccount().address
            }else{
                内容 =  sep
            }
        }
        if let sep = json["title"].string{ 标题 =  sep }
        if let sep = json["uploadpath"].string{ 图片路径 =  sep }
        if let sep = json["uploadtime"].string{
            上传时间 =  sep
            UserDefaults.standard.set(sep, forKey: "app_bannsers_updatatime")
            UserDefaults.standard.synchronize()
        }
    }
}


class WKBannerManager: NSObject {
    class func getGuideBanners(names:@escaping (([WKBannerM])->Void)) {
        LHRequest.get(url: httpPath(path: .获取启动页Banner_G),parameters: [:], runHUD: .none, success: { (json) in
            names(WKBannerM.getModels(json: json))
        }) { (error) in}
    }
    
    class func getAppBannser(names:@escaping (([WKBannerM])->Void)){
        LHRequest.get(url: httpPath(path: .获取应用Banner_G),parameters: [:], runHUD: .none, success: { (json) in
            print(json)
            names(WKBannerM.getModels(json: json))
        }) { (error) in}
//        LHRequest.get(url: "http://localhost:3000/activity/list", parameters:[:], success: { (json) in
//            print(json)
//            names(WKBannerM.getModels(json: json))
//        }) { (err) in}
    }
    
}
