

import UIKit

class AlertManager: NSObject {

    static let shared = AlertManager()
    
    fileprivate lazy var alertV = UIView.init()
    
    
    func showAlert(contenView:UIView,onView:UIView,headImg:UIImageView){
        self.alertV.frame = onView.bounds
        self.alertV.backgroundColor = UIColor.init(white: 0, alpha: 0.5)
        alertV.alpha = 0
        
        alertV.addSubview(contenView)
        alertV.addSubview(headImg)
        UIView.animate(withDuration: 0.3) {
            self.alertV.alpha = 1
            contenView.transform = CGAffineTransform.init(scaleX: 1, y: 1)
        }
        
        UIApplication.shared.keyWindow!.addSubview(alertV)
    }
    
    func closeAlert(contenView:UIView){
        UIView.animate(withDuration: 0.3, animations: {
            self.alertV.alpha = 0
        }) { (ok) in
            contenView.removeFromSuperview()
            contenView.removeAllSubviews()
            self.alertV.removeFromSuperview()
        }
    }
    
}
