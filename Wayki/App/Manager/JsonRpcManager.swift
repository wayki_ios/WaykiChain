//
//  JsonRpcManager.swift
//  Wayki
//
//  Created by louis on 2018/6/23.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit


extension Dictionary {
    
    static func wiccBalance(address:String)->[String:Any]{
        return ["jsonrpc":"2.0","id":"curltext","method":"getbalance","params":[address]] as [String:Any]
    }
    
    // MARK:- 区块链信息
    static func blockInformation()->[String:Any]{
        return ["jsonrpc":"2.0","id":"curltext","method":"getblockchaininfo"] as [String:Any]
    }
    
    // MARK:- 账户WICC信息
    static func accountInformation(address:String) ->[String:Any]{
        return ["jsonrpc":"2.0","id":"curltext","method":"getaccountinfo","params":[address]] as [String:Any]
    }
    
    // MARK:- 传输签名 - 激活、转账、投注通用
    static func register(signHex:String)->[String:Any] {
        return ["jsonrpc":"2.0","id":"curltext","method":"submittx","params":[signHex]] as [String:Any]
    }
    
    // MARK:- 账户Token信息
    static func accountTokenInformation(address:String,contractAddress:String) ->[String:Any]{
        return ["jsonrpc":"2.0","id":"curltext","method":"getaccountinfo","params":[contractAddress,address]] as [String:Any]
    }
    
}


class JsonRpcManager: NSObject {
    
    // 获取WICC余额
    class func getBalance(succeed:@escaping ((Int64,String)->Void),failed:@escaping (String)->Void){
        let address = AccountManager.getAccount().address
        let dic = Dictionary<String, Any>.accountInformation(address: address)
        LHRequest.postJsonRPC(url: nodeURL, parameters: dic, success: { (json) in
            if let dic = json.dictionary{
                if let result = dic["result"]?.dictionary{
                    var regessID = ""
                    var balanceC:Int64 = 0
                    if let regID = result["RegID"]?.string { regessID = regID }
                    if let balance = result["Balance"]?.int64 { balanceC = balance }
                    succeed(balanceC,regessID)
                }
            }
        }) { (str) in
            failed("获取账户信息失败".local)
        }
    }
    
    // 获取区块高度
    class func getBlockHeight(succeed:@escaping ((Int64)->Void),failed:@escaping (String)->Void){
        let dic = Dictionary<String, Any>.blockInformation()
        LHRequest.postJsonRPC(url: nodeURL, parameters: dic, success: { (json) in
            if let dic = json.dictionary{
                if let result = dic["result"]?.dictionary{
                    var blocks:Int64 = 0
                    if let blockH = result["blocks"]?.int64 { blocks = blockH }
                    succeed(blocks)
                }
            }
        }) { (str) in
            failed("获取区块高度失败".local)
        }
    }
    
}



// MARK:- 锁仓-查询仓库
extension JsonRpcManager{
    // 锁仓
    class func lockedPosition(signHex:String,succeed:@escaping ((String)->Void),failed:@escaping ((String)->Void)){
        let dic = Dictionary<String, Any>.register(signHex: signHex)
        LHRequest.postJsonRPC(url: nodeURL, parameters: dic, success: { (json) in
            print(json)
            if let dic = json.dictionary{
                if let result = dic["result"]?.dictionary{
                    if let hash = result["hash"]?.string {
                        succeed(hash)
                        return
                    }
                }
            }
            failed("数据返回有误".local)
        }) { (str) in
            failed(str)
        }
    }
    
}


import Alamofire
extension LHRequest{
    class func postJsonRPC(url:String,parameters:Dictionary<String,Any>,runHUD:HUDStyle = .loading,success: @escaping ((_ json:JSON)->Void),failure:@escaping ((String)->Void) ){
        // 显示加载器
        if runHUD == .loading{ showHUD() }
        // 参数处理
        Alamofire.request(url, method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default,
                          headers:["content-type":"application/json"]).responseJSON { (response) in
                            print(response)
                            dismissHUD()
                            switch response.result.isSuccess {
                            case true:
                                if let value = response.result.value {
                                    if let dic = JSON(value).dictionary{
                                        if let status = dic["error"]{
                                            if status == nil {
                                                success(JSON(value))
                                            }else{
                                                failure("\(status)")
                                            }
                                        }
                                    }
                                }
                            case false:
                                failure("failure".local)
                            }
        }
    }
}


//utf8编码
//            JsonRpcManager.lockedPosition(signHex: hex, succeed: { (hash) in
//                if hex.count>0{
//                    UILabel.showSucceedHUD(text: "锁仓已提交，请等待区块确认".local)
//                    //self.perform(#selector(self.popVC), with: nil, afterDelay: 2)
//                }else{
//                    UILabel.showFalureHUD(text: "锁仓失败，请稍后重试".local)
//                }
//
//            }) { (str) in
//                UILabel.showFalureHUD(text: str)
//            }

