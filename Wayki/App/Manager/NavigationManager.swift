
import UIKit

// 回调事件
enum LeftActionType:Int{
    
    case Bet = 1
    case Transfer = 2
    case Wallet = 3
    case HelpCenter = 4
    
    case SPC = 11
    case WICC = 12
    case QRCode = 13
    
    case lock = 20
    case exchange = 21
    case lucky = 22
    
}

let leftWidth = ScreenWidth*(314/375)

class NavigationManager: NSObject {
    static let shared = NavigationManager()
    var drawerC :DrawerController? = nil
    var rightC : RightC? = nil
    var leftC : LeftC? = nil
    var centerC : UINavigationController? = nil
    
    func configureControlers()->DrawerController{
        
        rightC = RightC()
        leftC = LeftC()
        centerC = UINavigationController(rootViewController: CenterC())
        centerC?.navigationBar.isHidden = true
        
        leftC?.leftAction = { [weak self] type in  self?.handleLeftAction(type: type)  }
        
        drawerC = DrawerController(centerViewController: centerC!,leftDrawerViewController: leftC!,rightDrawerViewController: rightC!)
        drawerC?.maximumLeftDrawerWidth = leftWidth
        drawerC?.maximumRightDrawerWidth = 0.1
        drawerC?.openDrawerGestureModeMask = .bezelPanningCenterView
        drawerC?.closeDrawerGestureModeMask = .all
        return drawerC!
    }
    
    
}


// MARK:- 主界面左侧栏跳转事件
extension NavigationManager:HeziTriggerActivePageDelegate{
    func handleLeftAction(type:LeftActionType){
        var c:UIViewController?
        switch type {
        // 功能选项
        case .lock:
            
            break
        case .Bet:
            c = MyBetC()
            break
        case .Transfer:
            c = MyTransferC()
            break
        case .Wallet:
            c = MyWallectC()
            break
        case .HelpCenter:
            break
        // 我的资产
        case .QRCode:
            c = MyQRcodeC()
            break
        case .SPC:
            c = MyAssertC()
            (c as! MyAssertC).wicc_spc = coinName
            break
        case .WICC:
            c = MyAssertC()
            (c as! MyAssertC).wicc_spc = "WICC"
            break
        case .exchange:
            c = EX_MainC()
            break
        case .lucky:
            break
        }
        if c != nil{
           c?.hidesBottomBarWhenPushed = true
            NavigationManager.shared.drawerC?.closeDrawer(animated: true, completion: nil)
            NavigationManager.shared.centerC?.pushViewController(c!, animated: false)

        }
    }

}

