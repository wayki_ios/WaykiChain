

import UIKit

class GuideManager: NSObject {

    class func configureGuidePage(window:UIWindow){
        UIApplication.shared.statusBarStyle = .lightContent
        window.backgroundColor = UIColor.white
        window.frame = UIScreen.main.bounds
        
        if AccountManager.getAccount().address == ""{  //加载创建钱包
            window.rootViewController = UINavigationController(rootViewController: WalletMainC() )
        }else{
            window.rootViewController = NavigationManager.shared.configureControlers()
        }
        window.makeKeyAndVisible()
    }
    
}
