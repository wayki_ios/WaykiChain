//
//  APpGameRefreshTimeManger.swift
//  Wayki
//
//  Created by sorath on 2018/6/25.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit
//主页面数据刷新机制
class APPGameRefreshTimeManager: NSObject {
    
    var refreshTime:Date = Date(timeIntervalSince1970: 0)  //内存存储的上一次刷新时间
    
    var updateSpaceTime:Double!  //刷新间隔时间
    var timerSpaceTime:Double!  //定时刷新时间
    
    var timer:Timer?
    var block:(() ->Void)?
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(becomeIn), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //注册
    func regist(uTime:Double = 60*60,tTime:Double = 5*60,neededUpdate:@escaping(() ->Void)){
        updateSpaceTime = uTime
        timerSpaceTime = tTime
        block = neededUpdate
        setTimer()
    }
    
    //注销
    func cancel(){
        timer?.invalidate()
    }
    
    //从后台进入前台
    @objc func becomeIn(){
        repeats()
    }
    
    
    //定时器
    private func setTimer(){
        timer = Timer.scheduledTimer(timeInterval: timerSpaceTime, target: self, selector: #selector(repeats), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.commonModes)
        timer?.fire()
        
    }
    
    @objc private func repeats(){
        let  IfNeededRefresh =  checkIfNeededRefresh()
        if IfNeededRefresh {
            block!()
        }
    }
    
    
    //存储时间,但不存储到本地
    private func saveTime(){
        refreshTime = Date()
    }
    
    //判断是否需要更新
    private func checkIfNeededRefresh() ->Bool{
        var isNeededRefresh = false
        let typeRefreshTime = refreshTime
        
        //如果上次存储时间和现在间隔1个小时，则需要刷新
        if typeRefreshTime.timeIntervalSince1970 + updateSpaceTime <= Date().timeIntervalSince1970 {
            isNeededRefresh = true
        }
        
        //如果需要刷新，则先记录当前时间
        if isNeededRefresh == true {
            saveTime()
        }
        
        return isNeededRefresh
    }

}
