//
//  ActiviteManager.swift
//  Wayki
//
//  Created by sorath on 2018/7/2.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class ActiviteManager: NSObject {

    //仅判断是否激活
    class func checkIsActivity() ->Bool{
        let account = AccountManager.getAccount()
        if account.regId.count>2{
            return true
        }else{
            
            if account.regId == " "{
                UILabel.showFalureHUD(text: "激活中，请等待区块确认".local)
                LHRequest.getRegId()
                return false
            }else{
     
                return false
            }
        }
    }
    
    //检验是否激活,如果未激活，则执行激活程序
    @discardableResult
    class func checkIsActivityAndActivity(vc:UIViewController)  ->Bool{
        let account = AccountManager.getAccount()
        if account.regId.count>2{
            return true
        }else{
            
            if account.regId == " "{
                UILabel.showFalureHUD(text: "激活中，请等待区块确认".local)
                LHRequest.getRegId()
                return false
            }else{
                let alertView = UIAlertController.init(title: "提示".local, message: "系统检测到您尚未激活钱包，请先激活".local, preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction.init(title: "激活", style: .default, handler: { (action) in
                    self.activiteWallet(vc: vc)
                })
                alertView.addAction(ok)
                vc.present(alertView, animated: true, completion: nil)
                return false
            }
        }
    }
    
    //弹出输入密码框
    class func activiteWallet(vc:UIViewController){
        UmengEvent.eventWithDic(name: "activate_wallet")
        let a = AccountManager.getAccount()
        if a.regId.count>2 {
            
        }else{
            
            AlertInputPwdView.shared.show().sureBlock = { pwd in
                //先获取高度，再激活钱包
                LHRequest.getVaildHeight { (height, address, appid) in
                    self.activiteRequest(height: height,pwd: pwd)
                }

            }
        }
    }
    
    //激活接口
    class func activiteRequest(height:Double,pwd:String){
        
        let amount = AccountManager.getAccount().wiccSumAmount
        
        if amount<0.0011{
            UILabel.showFalureHUD(text: "账户余额不足，无法激活".local)
            return
        }
        let acccount = AccountManager.getAccount()
        let path:String = httpPath(path: HTTPPath.激活地址_P)
        let mHash = acccount.mHash
        let helpStr = acccount.getHelpString(password: pwd)
        let signHex = Bridge.getActrivateHex(withHelpStr: helpStr, withPassword: pwd, fees: 0.0001, validHeight: height)
        
        let requestPath = path + mHash
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        var pa:[String:Any] = [:]
        
        pa["signHex"] = signHex
        pa["txRemark"] = ""
        
        LHRequest.post(url: getPath, parameters: pa,runHUD:.loading, success: { (json) in
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {
                        UILabel.showFalureHUD(text: "激活失败,请重试".local)
                    }else{
                        acccount.regId = " "
                        AccountManager.saveAccount(account: acccount)
                        UmengEvent.eventWithDic(name: "activate_readyForSure")
                        UILabel.showSucceedHUD(text: "激活中，待区块链确认".local)

                    }
                }
            }
            
        }) {() in
            UILabel.showFalureHUD(text: "激活失败,请重试".local)
        }
        
    }
    
}
