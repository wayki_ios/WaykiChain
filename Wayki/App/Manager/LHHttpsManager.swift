

import UIKit

enum HTTPPath:String {
    
    case 校验地址是否报名 =  "/gameactivity/checksignupwithaddress/"
    case 查询抽奖次数_G = "/api/gameactivity/selectLuckyCountByAddress?"//address=address

    case 篮球竞猜投注_P = "/api/lottery/basketball/betting"
    case 篮球赛事_G = "/api/lottery/basketball/list"
    case 篮球赛事_单次_G = "/api/lottery/basketball/detail/"
    case 篮球投注记录_G = "/api/lottery/basketball/orderrecords/" // {address}
    case 篮球投注详情_G = "/api/lottery/basketball/orderdetails/" // {address}/{orderid}

    
    case 足球竞猜投注_P = "/api/lottery/football/betting"
    case 足球赛事_G = "/api/lottery/football/list"
    case 足球赛事_单次_G = "/api/lottery/football/detail/"
    case 足球投注详情_G = "/api/lottery/football/orderdetails/" // {address}/{orderid}
    case 足球投注记录_G = "/api/lottery/football/orderrecords/" // {address}
    
    case 改变币种是否关注_P = "/api/assets/changeFollowCoin/" //{mhash}/{address}
    case 获取关注币列表_G = "/api/assets/followcoins/" //{mhash}/{address}
    case 发起转账交易_P = "/api/assets/sendTx/"  // {mhash}/{symbol}
    case 获取资产汇总_G = "/api/assets/summary/" //{mhash}/{address}
    case 交易详情_G =  "/api/assets/txDetails/" //{address}/{txid}
    case 交易记录_P = "/api/assets/txRecords/"  //{address}
    
    case 获取应用Banner_G = "/api/common/getAppBanner"
    case 获取最新区块高度_G = "/api/common/getTxParams/"//{symbol}
    case 获取最新移动版本_G = "/api/common/getLastMobileVersion/" //{apptype} app类型(1.安卓,2.苹果)
    case 获取启动页Banner_G =  "/api/common/getStarterBanner"
    
    case 创建钱包_P = "/api/wallet/createwallet"
    case 获取地址regid_G = "/api/wallet/getRegid/" //{mhash}/{address}
    case 激活地址_P =  "/api/wallet/regaddr/" //{mhash}
    
    case 锁仓合约_G = "/sysconfig/lockposition"
    case 节点配置_G = "/sysconfig/publicnodeurl"
    case 锁仓记录_G = "/lockposition/getlockrecord"
    case 兑换信息_G = "/coinexchange/getExhangeConf"
    case 查询转账结果 = "/api/assets/getTxStatusByTxid/"
    
}



class LHHttpsManager: NSObject {
    static let imagePath = ""
    static let uploadImagePath = ""
    
}

//定义一个结构体，存储认证相关信息
struct IdentityAndTrust {
    var identityRef:SecIdentity
    var trust:SecTrust
    var certArray:AnyObject
}




