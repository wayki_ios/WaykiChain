

import UIKit

let languageKey = "languageKey"

class LanguageManager: NSObject {
    
    
    static let shared = LanguageManager()
    private override init() {
        super.init()
        choiceLanguage()
    }
    
}

// MARK:- 初始化语言
extension LanguageManager{
    
}


extension LanguageManager{
    // MARK:- 确认语言
    func choiceLanguage(){
        if UserDefaults.standard.string(forKey: languageKey) == nil{
            let arr:[String] = UserDefaults.standard.value(forKey: "AppleLanguages") as! [String]
            let language = arr.first!.split(separator: "-").first
            UserDefaults.standard.set(language, forKey: "AppleLanguages")
        }
    }

}
