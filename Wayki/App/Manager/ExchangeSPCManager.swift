//
//  ExchangeSPCManager.swift
//  Wayki
//
//  Created by sorath on 2018/6/11.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class ExchangeSPCManager: NSObject {
    
    ///判断是否需要兑换(默认已激活),transferM:spc消耗数量
    class func checkIsExchange(password:String = "",transferM:Double){
        let account = AccountManager.getAccount()
        let checkTime = account.exClickTime
        
        //至少2s才能点击兑换一次
        if checkTime.timeIntervalSince1970 + 2 < Date().timeIntervalSince1970{
            account.exClickTime = Date()
            checkRequest { (isEx,tAddress) in
                if isEx{
                    //弹框显示信息
//                    let v = SPCEXchangeInfoAlertView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight), num: 1, allNum: 2, transfer: 20)
//                    v.show()
//                    v.sureAction = { () in
                        //判断 spc wicc 数量是否足够
                        if AccountManager.getAccount().wiccSumAmount < 0.015{
                            UILabel.showFalureHUD(text: "无足够WICC".local)
                            return
                        }
                        
                        if AccountManager.getAccount().spcSumAmount < transferM{
                            UILabel.showFalureHUD(text: "无足够".local + coinName )
                            return
                        }
                        //确定,判断是否存在密码
                        if password == ""{
                            //输入密码，然后调用转账等接口
                            AlertInputPwdView.shared.show().sureBlock = { pwd in
                                exchangeSPC(password: pwd, transferM: transferM, destAddr: tAddress)
                            }
                        }else{
                            //调用转账等接口
                            exchangeSPC(password: password, transferM: transferM, destAddr: tAddress)
                        }
                        
                    //}
                    
                }else{
                    UILabel.showFalureHUD(text: "兑换失败".local)
                    
                }
            }
            

        }else{
            UILabel.showFalureHUD(text: "操作过于频繁".local)
        }

    }
    
    

    
}

//MARK: - Request
extension ExchangeSPCManager {

    //接口 请求次数
    class private func checkRequest(status:@escaping ((Bool,String) ->Void)){
        let path:String = httpPath(path: HTTPPath.查询抽奖次数_G)
        let requestPath = path + "address=" + AccountManager.getAccount().address
        LHRequest.get(url: requestPath, parameters: [:], runHUD: .none, success: { (json) in
            print("json:\(json)")
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {}
                }
                if let result = dic["result"]?.dictionary{
                    if result.count>0{
                        
                        let toAdminAddress = result["toAdminAddress"]?.string

                            status(true,toAdminAddress!)

                        return
                    }
                }
            }
            UILabel.showFalureHUD(text: "请求失败，请稍后重试".local)

        }) { (error) in
            UILabel.showFalureHUD(text: "请求失败，请稍后重试".local)
        }
        
    }
    
    //兑换spc为抽奖次数的接口
    class private func exchangeSPC(password:String,transferM:Double,destAddr:String){
        
        let account = AccountManager.getAccount()
        
        let helpStr = account.getHelpString(password: password)
        let mHash = account.mHash
        let regId = account.regId
        let fees = 0.01 + Double(Bridge.getRandomMaxValue(500000, minValue: 0))/100000000
        LHRequest.getVaildHeight(symbol: coinName) { (vaildHeight, _, appid) in
            //签名，从api中获取
            var hex:String! = ""
            hex = Bridge.getTransfetSPCHex(withHelpStr: helpStr, withPassword: password, fees:fees , validHeight: vaildHeight, srcRegId: regId, appId:appid,destAddr: destAddr, transferValue: transferM)
            let path:String = httpPath(path: HTTPPath.发起转账交易_P)
            
            let requestPath = path + mHash + "/" + coinName
            var pa:[String:Any] = [:]
            pa["signHex"] = hex
            pa["txRemark"] = ""
            LHRequest.post(url: requestPath, parameters: pa,runHUD: .loading, success: {(json) in
               
             
                if let hash = json["txhash"].string{
                    UserDefaults.standard.set(hash, forKey: "txhash")
                    NotificationCenter.default.post(name: Notification.Name.init("duihuanchenggong"), object: nil)
                }
                
                UILabel.showTextHUD(text: "请求已提交，请等待区块确认".local)
                
            }) { () in
                UILabel.showFalureHUD(text: "兑换失败".local)
            }
        }
    }

    
    
    //获取日排名和抽奖次数的接口
    class func requestCheck(address:String,success:@escaping ((_ transCounts:Int,_ dayOrder:Int,_ toAdminAddress :String) ->Void)){
        let path:String = httpPath(path: HTTPPath.查询抽奖次数_G)
        let requestPath = path + "address=" + address
        LHRequest.get(url: requestPath, parameters: [:], runHUD: .none, success: { (json) in
            print("json:\(json)")
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {}
                }
                if let result = dic["result"]?.dictionary{
                    if result.count>0{
                        let transCounts = result["transCounts"]?.int
                        let dayOrder = result["dayOrder"]?.int
                        let toAdminAddress = result["toAdminAddress"]?.string
                        success(transCounts!,dayOrder!,toAdminAddress!)
                        return
                    }
                }
            }
            
        }) { (error) in
            UILabel.showFalureHUD(text: "请求失败，请稍后重试".local)
        }
    }
}
