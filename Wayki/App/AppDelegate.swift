

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        GuideManager.configureGuidePage(window: self.window!)
        ConfigureManager.configureLibrary()
        return true
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        ConfigureManager.handle3DTouchAction(type: shortcutItem.type)
    }
    
    func application(_ application: UIApplication, shouldAllowExtensionPointIdentifier extensionPointIdentifier: UIApplicationExtensionPointIdentifier) -> Bool {
        return false
    }
}

 
extension AppDelegate{
    
    //确定转账
    //startTransfer(0.01, "", 0.2, "123456789")
    func startTransfer(_ fees:Double,_ destAddr:String,_ transferM:Double,_ pwd:String){
        LHRequest.getVaildHeight(symbol: coinName) { (vaildHeight, _, appid) in
            //签名，从api中获取
            print(vaildHeight)
            let account = AccountManager.getAccount()
            let helpString = account.getHelpString(password: pwd)
            var hex:String! = ""
            hex = Bridge.getLockHex(withHelpStr: helpString, blockHeight: vaildHeight, regessID: account.regId, appId: "400550-1", destAddr: "WiGAkZEGUph5aK52XbLCu1PLqN7fkuCfSA", transferValue: transferM)
            
            let path:String = httpPath(path: HTTPPath.发起转账交易_P)
            let mhash = AccountManager.getAccount().mHash
            let requestPath = path + mhash + "/" + "WICC"
            let pa:[String:Any] = ["signHex":hex,"txRemark":""]
            LHRequest.post(url: requestPath, parameters: pa,runHUD: .loading, success: { (json) in
                print(json)
                UILabel.showSucceedHUD(text: "锁仓已提交，请等待区块确认".local)
            }) { () in
                UILabel.showFalureHUD(text: "锁仓失败，请稍后重试".local)
            }
        }

    }
    
}
