

import UIKit
import Kingfisher

class GuidanceC: FoundationC {
    var myView: UIScrollView? = nil
    var pageControl:UIPageControl? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        configureView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.barTintColor = UIColor.red
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.barTintColor = UIColor.RGB(r: 51, g: 54, b: 105)
    }
    
}


extension GuidanceC{
    
    func configureNavigationBar(){
//        stateBarIsBlack = true
//        self.naviBar?.backgroundColor = UIColor.clear
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func configureView(){
        
        myView = UIScrollView()
        myView?.frame = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)
        myView?.contentSize = CGSize(width: ScreenWidth*3, height: ScreenHeight)
        myView?.bounces = false
        myView?.showsVerticalScrollIndicator = false
        myView?.showsHorizontalScrollIndicator = false
        myView?.delegate = self
        myView?.isPagingEnabled = true
        myView?.isScrollEnabled = true
        view.addSubview(myView!)

        for index in 0..<3 {
            let imageV = UIImageView(frame: CGRect(x: ScreenWidth*CGFloat(index), y: 0, width: ScreenWidth, height: ScreenHeight))
            imageV.image = UIImage(named:"guide_page\(index+1)_\(UIDevice.model())")
            imageV.contentMode = .scaleToFill
            myView?.addSubview(imageV)
            
            if index == 2{
                let btn = UIButton(frame: CGRect(x: ScreenWidth*2+30, y: ScreenHeight-200, width: ScreenWidth-60, height: 200))
                btn.backgroundColor = UIColor.black
                btn.setTitleColor(UIColor.white, for: .normal)
                btn.backgroundColor = UIColor.clear //RGB(r: 210, g: 80, b: 73)
                btn.layer.cornerRadius = 6
                btn.addTarget(self, action: #selector(removeGuidePage), for: .touchUpInside)
                myView?.addSubview(btn)
            }
        }
        
        pageControl = UIPageControl(frame: CGRect(x: 0, y: ScreenHeight-145, width: ScreenWidth, height: 20))
        pageControl?.currentPage = 0
        pageControl?.numberOfPages = 3
        pageControl?.pageIndicatorTintColor = UIColor.RGB(r: 234, g: 234, b: 234)
        pageControl?.backgroundColor = UIColor.clear
        pageControl?.layer.borderColor = UIColor.black.cgColor
        pageControl?.layer.borderWidth = 0.5
        pageControl?.isHidden = true
        pageControl?.currentPageIndicatorTintColor = UIColor.RGB(r: 210, g: 80, b: 73) //RedColor()
        pageControl?.addTarget(self, action: #selector(pageChanged), for: .valueChanged)
        view.addSubview(pageControl!)

    }
    
    @objc func removeGuidePage(){
//        let tabBarC = FoundationTabC();
//        tabBarC.tabBar.tintColor = UIColor.RGBColorFromHex(0xfc5534)
//        UIApplication.shared.keyWindow?.rootViewController = tabBarC.defulatTabBar()
        UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: WalletMainC())
    }
    
    @objc func pageChanged(){
        let rect = CGRect(x: CGFloat((pageControl?.currentPage)!)*ScreenWidth, y: 0, width: ScreenWidth, height: ScreenHeight)
        myView?.scrollRectToVisible(rect, animated: true)
    }
}



extension GuidanceC:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(fabs(scrollView.contentOffset.x/ScreenWidth))
    }
}

