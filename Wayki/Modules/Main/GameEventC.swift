

import UIKit


class GameEventC: FoundationC {
    
    var datas:[AppEventBetM] = []
    var collectionView:UICollectionView!
    var currentPage:Int = 1
    var backScrollView:UIScrollView?
    var updatedNoDate:(()->Void)?
    var swipeAction:((Int)->Void)?
    private let betMessage = BetMessage()
    private var layout = UICollectionViewFlowLayout()//CardLayout()
    private let refreshModel = APPGameRefreshTimeManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        buildV()
        addPageView()
        configureRefresh()

    }
    

}

// MARK:- 请求数据
extension GameEventC{
    
    func requestData(runHUD:Bool=false){
        let typepath = HTTPPath.篮球赛事_G
        let requestpath = httpPath(path: typepath) + "?page=\(currentPage)&rows=40"
        LHRequest.get(url: requestpath,parameters: [:],runHUD: runHUD ? .loading:.none, success: {[weak self] (json) in
            self?.handleData(json: json)
            if ((self?.collectionView?.mj_header) != nil){
                self?.collectionView?.mj_header.endRefreshing()
            }
        }) {[weak self] (error) in
            if self?.updatedNoDate != nil{ self?.updatedNoDate!() }
            UILabel.showFalureHUD(text: "网络异常，请重试")
            self?.collectionView?.mj_header.endRefreshing()
        }
    }
    
    func handleData(json:JSON){
        //print(json)
        let new:[AppEventBetM] = AppEventBetM.getModels(json: json)
        datas = new
        datas.sort { (s1:AppEventBetM, s2:AppEventBetM) -> Bool in
            return s1.比赛时间_秒 < s2.比赛时间_秒
        }
        datas.sort { (s1:AppEventBetM, s2:AppEventBetM) -> Bool in
            return s1.是否热门 > s2.是否热门
        }
        if datas.count > 0 {
            layout.scrollDirection = .vertical
        }else{
            layout.scrollDirection = .horizontal
        }
        
        collectionView.reloadData()
        
        if updatedNoDate != nil{ updatedNoDate!() }
        page.allNum = datas.count
        page.addBtns()
    }
    
    @objc func refreshHead(){
        
        if #available(iOS 10.0, *) {
            let imp = UIImpactFeedbackGenerator.init(style: .light)
            imp.impactOccurred()
        }
        
        requestData(runHUD: false)
        
    }
    
    func configureRefresh(){
        refreshModel.regist{
            //print("事件需要刷新")
            self.requestData(runHUD: true)
        }
    }
}

// MARK:- 创建界面
extension GameEventC{
    private func buildV(){
        
        let headerH:CGFloat = naviHeight+10
        view.frame = CGRect(x: 0, y: headerH, width: ScreenWidth, height: ScreenHeight-headerH)
//        backScrollView = UIScrollView(frame: CGRect(x: 0, y:0 , width: ScreenWidth, height: ScreenHeight-headerH))
//        let header = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(refreshHead))
//        header?.stateLabel.isHidden = true
//        header?.lastUpdatedTimeLabel.isHidden = true
//        header?.alpha = 1
//        backScrollView?.mj_header = header
//        view.addSubview(backScrollView!)
        
//        layout.scale = 1
//        layout.edgeInset = UIEdgeInsets(top: 0, left:0, bottom: 0, right:0)
        layout.itemSize = CGSize(width: ScreenWidth, height: view.height())
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        collectionView = UICollectionView(frame: CGRect(x: 0, y:0 , width: ScreenWidth, height: ScreenHeight-headerH),collectionViewLayout:layout)
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = UIColor.clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(GameEventCollCell.self, forCellWithReuseIdentifier: "CellID")
        collectionView.decelerationRate = 0.6
        collectionView?.delaysContentTouches = false
        view.addSubview(collectionView!)
        
        let leftGesture = UISwipeGestureRecognizer(target: self, action: #selector(leftGestureAction))
        leftGesture.direction = .left
        view.addGestureRecognizer(leftGesture)
        
        let rightGesture = UISwipeGestureRecognizer(target: self, action: #selector(rightGestureAction))
        rightGesture.direction = .right
        view.addGestureRecognizer(rightGesture)
        
        
        let header = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(refreshHead))
        header?.stateLabel.isHidden = true
        header?.lastUpdatedTimeLabel.isHidden = true
        header?.alpha = 1
        collectionView?.mj_header = header
        
    }
    
    @objc func leftGestureAction(){
        if swipeAction != nil{
            swipeAction!(0)
        }
    }
    
    @objc func rightGestureAction(){
        if swipeAction != nil{
            swipeAction!(1)
        }
    }
}

//MARK: - 代理事件
extension GameEventC: UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:GameEventCollCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as! GameEventCollCell
        cell.index = indexPath
        cell.configureModel(model: datas[indexPath.row])
        cell.betAction = {[weak self] (index,type,bet) in self?.betAction(index: index, type: type, bet: bet) }
        return cell
    }
    
//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        for cell in collectionView.visibleCells {
//            (cell as! GameEventCollCell).hideBar()
//        }
//    }
//    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        for cell in collectionView.visibleCells{
//            (cell as! GameEventCollCell).showBar()
//        }
//        
//    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let curPage = scrollView.contentOffset.y/scrollView.height()
        page.movetoIndex(index: Int(curPage+0.5) + 1)
    }
    
}

// MARK:- 界面事件
extension GameEventC{

    func currentMoveToIndex(int:Int){
        collectionView.scrollToItem(at: IndexPath(item: int, section: 0), at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }
    
    func betAction(index:Int,type:Int,bet:Int){
        let account = AccountManager.getAccount()
        if account.regId.count>2{
            
        }else{
            
            if account.regId == " "{
                UILabel.showFalureHUD(text: "激活中，请等待区块确认")
                LHRequest.getRegId()
                return
            }else{
                let alertView = UIAlertController.init(title: "提示", message: "系统检测到您尚未激活钱包，请先激活", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction.init(title: "激活", style: .default, handler: { (action) in
                    NavigationManager.shared.leftC?.激活账户(button: UIButton())
                    
                })
                alertView.addAction(ok)
                self.present(alertView, animated: true, completion: nil)
                return
            }
        }
        
        
        let model = datas[index]
        let v = APPGameBetView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight), type: .事件)
        
        v.setSumAmount(amount: AccountManager.getAccount().spcSumAmount)
        v.truple = (index:index,type:type,bet:bet)
        v.setTitleText(timeStr: model.截止时间, betStr: model.玩法列表[type].投注项列表[bet].投注项名称)
        v.betBlock = { [weak self](betCount,truple:(index:Int,type:Int,bet:Int)) in
            //print("betcount:%d---index:%d----type:%d---bet:%d",betCount,truple.type,truple.index,truple.bet)
            self?.handleBet(index: index, type: type, bet: bet, betCount: betCount)
        }
        v.closeBlock = {[weak self] () in
            for cell in (self?.collectionView.visibleCells)! {
                (cell as! GameEventCollCell).betBSAnimation()
            }
        }
        v.show()
    }

}

extension GameEventC {
    
    // 投注出输入密码框
    func handleBet(index:Int,type:Int,bet:Int,betCount:Int){
        let account:NewAccount = AccountManager.getAccount()
        if account.wiccSumAmount<0.016 {
                UILabel.showFalureHUD(text: "无足够WICC支付小费".local)
            return
        }
        
        betMessage.betCount = betCount
        betMessage.dataIndex = index
        betMessage.gameType = type
        betMessage.betType = bet
        let accountTup = AccountManager.getAccount().checkBetIfNeededInputPassword()
        if accountTup.isNeededInput {
            AlertInputPwdView.shared.show().sureBlock = {[weak self] (pwd) in
                AccountManager.getAccount().updateTimeAndPwd(inputTime: Date().timeIntervalSince1970, pwd: pwd)
                self?.sureAction(password:pwd)
            }
        }else{
            LHRequest.getVaildHeight(symbol: coinName) { [weak self] (height, address, appid) in
                self?.judgeBet(height: height, address: address, appID: appid, helpString: accountTup.helpStr)
            }
        }
    }
    
    func sureAction(password:String){
        LHRequest.getVaildHeight(symbol: coinName) { [weak self]  (height, address, appid)  in
            self?.judgeBet(height: height, address: address, appID: appid, helpString: AccountManager.getAccount().getHelpString(password: password))
        }
    }
    
    func judgeBet(height:Double,address:String,appID:String,helpString:String){ // 投注请求
        let path = HTTPPath.篮球竞猜投注_P
        let mhash = AccountManager.getAccount().mHash
        let requestPath = httpPath(path: path)+"/"+mhash+"/"+String(describing: self.datas[betMessage.dataIndex].赛事id主键)
        let sinHex = Bridge.getBetSign(withHelp: helpString,oldPassword: "",
                                       blockHeight: height,
                                       regID: AccountManager.getAccount().regId,
                                       lotteryID: self.datas[betMessage.dataIndex].赛事唯一ID,
                                       destAddress: appID,
                                       gameType: 0,
                                       playType: Int32(datas[betMessage.dataIndex].玩法列表[betMessage.gameType].玩法类型),
                                       betType: Int32(datas[betMessage.dataIndex].玩法列表[betMessage.gameType].投注项列表[betMessage.betType].投注项),
                                       betCount: Int32(betMessage.betCount))
        let dic = ["signHex": sinHex!,"txRemark": ""] as [String:Any]
        LHRequest.post(url: requestPath, parameters: dic, success: { [weak self] (json) in
            self?.handleBetData(json: json)
        }) {
            UILabel.showFalureHUD(text: "投注失败！".local)
        }
    }
    
    // 投注请求
    func handleBetData(json:JSON){
        if let dic = json.dictionary {
            if let status = dic["status"]?.int{
                if status == 1{
                    UmengEvent.eventWithDic(name: "bet_click", dic: ["type":"envent"])
                    UILabel.showSucceedHUD(text: "投注成功,等待区块确认！".local)
                    HomeUserInfoManager.show(v: UIApplication.shared.keyWindow!, imgName: "2")
                    return
                }
            }
        }
        UILabel.showFalureHUD(text: "投注失败！".local)
    }
    
}

