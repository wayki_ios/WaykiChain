
import UIKit

class AlertInputPwdView :NSObject {
    
    fileprivate lazy var contentV = UIScrollView.init()
    fileprivate lazy var texttile1 = InputPasswordTextFiled.init()
    fileprivate lazy var addressLabel = UILabel.init()
    fileprivate lazy var errMsgLabel = UILabel.init()
    fileprivate lazy var sure = UIButton.init()
    
    
    var sureBlock:((_ pwd:String)->Void)? = nil
    
    
    static var shared = AlertInputPwdView()
    
    func show()->AlertInputPwdView{
        let widthScale = ScreenWidth/375
        let x = 24 * widthScale
        
        let contentWidth = widthScale * 327
        let contentheight = contentWidth * 0.73
        
        
        contentV.frame = CGRect(x: x , y: ScreenHeight/2-contentheight/2, width: contentWidth , height: contentheight)
        contentV.backgroundColor = UIColor.white
        contentV.layer.cornerRadius = 10
        
        self.contentV.transform = CGAffineTransform.init(scaleX: 0, y: 0)
        
        let label1 = UILabel.init(frame: CGRect(x: 20, y: contentheight * 0.1666, width: contentWidth-40, height: contentheight * 0.1166))
        label1.text = "钱包密码".local
        label1.font = UIFont.systemFont(ofSize: 20)
        label1.textAlignment = .center
        contentV.addSubview(label1)
  
        texttile1.frame = CGRect(x: x, y: label1.bottom()+contentheight*0.129, width: label1.width(), height: 22)
        texttile1.placeholder = "请输入钱包密码".local
        texttile1.text = ""
        texttile1.becomeFirstResponder()
        texttile1.keyboardType = .asciiCapable
        texttile1.isSecureTextEntry = true
        texttile1.font = UIFont.systemFont(ofSize: 16)
        texttile1.delegate = self
        texttile1.returnKeyType = .done
        
        contentV.addSubview(texttile1)
        
        let line = UIView.init(frame: CGRect(x: x, y: texttile1.bottom() + contentheight * 0.0625, width: contentWidth-2*x, height: 0.5))
        line.backgroundColor = UIColor.init(white: 0.85, alpha: 1)
        contentV.addSubview(line)
        
        errMsgLabel.frame = CGRect(x: x, y: line.bottom() + 5, width: line.width(), height: 15)
        errMsgLabel.textColor = .red
        errMsgLabel.font = UIFont.systemFont(ofSize: 10)
        errMsgLabel.alpha = 0
        errMsgLabel.text = "密码错误,请重新输入".local
        contentV.addSubview(errMsgLabel)
        
        
        sure.frame = CGRect(x: contentWidth/4 , y: contentheight - contentheight * 0.2 - contentheight*0.0727, width:contentWidth/2, height: contentheight*0.2)
        sure.setBackgroundImage(UIImage.init(named: "wallet_button_normal"), for: .normal)
        sure.setTitle("确认".local, for: .normal)
        sure.isEnabled = false
        sure.layer.cornerRadius = 6
        sure.clipsToBounds = true
        sure.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        sure.addTarget(self, action: #selector(clicksure), for: .touchUpInside)
        contentV.addSubview(sure)
        
        let exit = UIButton.init(frame: CGRect(x:contentWidth-44, y: 0, width: 44, height:44))
        exit.setImage(UIImage.init(named: "alert_cancel"), for: .normal)
        exit.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        exit.addTarget(self, action: #selector(exitAlert), for: .touchUpInside)
        contentV.addSubview(exit)
        AlertManager.shared.showAlert(contenView: self.contentV, onView: UIApplication.shared.keyWindow!,headImg: UIImageView())
        return self
    }
    @objc func clicksure(){
        
        //校验密码
        let pwdIsTrue = AccountManager.getAccount().checkPassword(inputPassword: texttile1.text!)
        if !pwdIsTrue{
            UIView.animate(withDuration: 0.3) {
                self.errMsgLabel.alpha = 1
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.3) {
                UIView.animate(withDuration: 0.3) {
                    self.errMsgLabel.alpha = 0
                }
            }
            return
        }
        exitAlert()
        
        if let b = sureBlock{
            b(texttile1.text!)
        }
        
        
    }
    @objc func exitAlert(){
        
        AlertManager.shared.closeAlert(contenView: self.contentV)
        
    }
    
}
extension AlertInputPwdView:UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            if ScreenHeight == 812{
                self.contentV.frame.origin.y = ScreenHeight-330-self.contentV.height()
                
            }else{
                self.contentV.frame.origin.y = ScreenHeight-271-self.contentV.height()
                
            }
            
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str:NSString = textField.text! as NSString
        let showStr = str.replacingCharacters(in: range, with: string)
        if showStr.count == 0{
            sure.isEnabled = false
        }else{
            sure.isEnabled = true
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3) {
            self.contentV.frame.origin.y = ScreenHeight/2-self.contentV.height()/2
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}
