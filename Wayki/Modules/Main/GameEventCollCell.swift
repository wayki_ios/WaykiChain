

import UIKit


class GameEventCollCell: UICollectionViewCell {
    var index:IndexPath!
    var betAction:((Int,Int,Int)->Void)?
    var eventModel:AppEventBetM = AppEventBetM()
    private var betBar:GameBetBar!
    private var bgdIV:UIImageView!
    private var poolL:UILabel!
    private var countDownL:UILabel!
    private var gameType:Int = 0 // 胜平负
    private var titleL:UILabel!
    private var evidenceL:UILabel!
    private var countdownView:GameCommonCountdownView?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView(frame:frame)
    }
    
}

//MARK: - 界面
extension GameEventCollCell{
    
    func buildView(frame:CGRect){
        // 投注项 位置
        var betbarH:CGFloat = ScreenWidth/4+40
        // 球队图标高度
        let imageW:CGFloat = 45*scale
        // 背景图高度
        let bgdIVH:CGFloat = ScreenWidth*0.95
        var bgdIVY:CGFloat = imageW+30 //height()-betbarH-bgdIVH
        
        if UIDevice.isX() {
            betbarH = ScreenWidth/4+60
            bgdIVY = imageW+60
        }
        
        betBar = GameBetBar(frame: CGRect(x: 0, y: height()-betbarH, width: ScreenWidth, height:betbarH))
        betBar.betAction = { [weak self] (tag) in self?.betAction(int: tag) }
        addSubview(betBar)
        
        //--------------------- Level 1 -----------------------//
        bgdIV = UIImageView(frame: CGRect(x: 0, y: bgdIVY, width: ScreenWidth, height: bgdIVH*0.95))
        bgdIV.image = UIImage(named: "game_event_select_bgd")
        bgdIV.isUserInteractionEnabled = true
        bgdIV.contentMode = .scaleAspectFit
        addSubview(bgdIV)
        
        //--------------------- Level 2 -----------------------//
        titleL = UILabel(frame: CGRect(x: 40*scale, y: 3, width: ScreenWidth-80*scale, height: bgdIVH/4))
        titleL.font = UIFont(name: kFontTypeMedium, size: 15)
        titleL.textColor = UIColor.black
        titleL.numberOfLines = 3
        bgdIV.addSubview(titleL)
        
        evidenceL = UILabel(frame: CGRect(x: 40*scale, y: titleL.height(), width: ScreenWidth-70*scale, height: 30))
        evidenceL.font = UIFont(name: kFontTypeThin, size: 12)
        evidenceL.textColor = UIColor.white
        evidenceL.numberOfLines = 0
        evidenceL.textAlignment = .center
        bgdIV.addSubview(evidenceL)
        
        //--------------------- Level 3 -----------------------//
        
        let level4Y:CGFloat = 230/320*bgdIVH/0.95
        let poolTitleL = UILabel(frame: CGRect(x: ScreenWidth*(61/375), y: level4Y, width: 50, height: 28*scale))
        poolTitleL.text = "总奖池".local
        poolTitleL.textAlignment = .center
        poolTitleL.textColor = UIColor.RGBHex(0xffffff, alpha: 0.6)
        poolTitleL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(poolTitleL)
        
        let spcL = UILabel(frame: CGRect(x: ScreenWidth-ScreenWidth*(61/375)-50, y: level4Y, width: 50, height: 28*scale))
        spcL.text = coinName
        spcL.textAlignment = .center
        spcL.textColor = UIColor.RGBHex(0xffffff, alpha: 0.6)
        spcL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(spcL)
        
        poolL = UILabel(frame: CGRect(x: 0, y: level4Y, width: ScreenWidth, height: 28*scale))
        poolL.textAlignment = .center
        poolL.textColor = UIColor.white
        poolL.font = UIFont(name: kFontTypeMedium, size: 20)
        bgdIV.addSubview(poolL)
        
        //--------------------- Level 5 -----------------------//
        let level5Y:CGFloat = bgdIVH*0.95-40*scale // level4Y + 28*viewScale
        let level5H:CGFloat = 40*scale
        
        let viewX:CGFloat = ScreenWidth*(16/375)
        let viewW:CGFloat = ScreenWidth-2*viewX
        let viewV = UIView(frame: CGRect(x:viewX, y: level5Y, width: viewW, height: level5H))
        bgdIV.addSubview(viewV)
        let cvTop = -bgdIVY/2.0 - 10
        countdownView = GameCommonCountdownView(frame: CGRect(x: 0, y: cvTop, width: ScreenWidth, height: 20))
        bgdIV.addSubview(countdownView!)
    }
    
}

// MARK:- 配置数据
extension GameEventCollCell{
    func configureModel(model:AppEventBetM){
        eventModel = model
        evidenceL.text = model.事件依据
        poolL.text = "\(model.奖池金额)"
        
        titleL.text = model.事件主体
        titleL.frame.size.height = NSString.getHeightWithText(model.事件主体, width: ScreenWidth-80*scale, fontSize: 16, lineSpacing: 8)
        titleL.attributedText = NSAttributedString.getEventContentString(text: model.事件主体)
        
        betBar.setModels(models: model.玩法列表, betGameType: gameType)
//        countdownView?.setTime(time: model.比赛时间_秒)
        countdownView?.setOnlyTime(time: model.比赛时间_秒)
    }
    
    func hideBar(){
        UIView.animate(withDuration: 0.3) {
            self.betBar.alpha = 0
        }
    }
    
    func showBar(){
        UIView.animate(withDuration: 0.3) {
            self.betBar.alpha = 1
        }
    }
    
    //按钮放大缩小动画
    func betBSAnimation(){
        if self.betBar.alpha == 1{  
            self.betBar.btnBSAnimation()
        }
    }
}

// MARK:- 回掉数据
extension GameEventCollCell{
    @objc func typeAction(button:UIButton){
        gameType = button.tag
    }
    
    func betAction(int:Int){
        if betAction != nil{ betAction!(index.row,gameType,int) }
    }
    
}
