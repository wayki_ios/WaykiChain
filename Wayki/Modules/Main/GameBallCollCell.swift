

import UIKit

class GameBallCollCell: UICollectionViewCell {
    var cellIndex:IndexPath!
    var betAction:((Int,Int,Int)->Void)?
    var footModel:AppFootballM = AppFootballM()
    var basketModel:AppBasketballM = AppBasketballM()
    
    private var betBar:GameBetBar!
    private var bgdIV:UIImageView!
    private var buttons:[UIButton] = []
    private var rightTeamIV:UIImageView!
    private var leftTeamIV:UIImageView!
    private var rightTeamL:UILabel!
    private var leftTeamL:UILabel!
    private var compationL:UILabel!
    private var poolL:UILabel!
    private var countDownL:UILabel!
    private var viewScale:CGFloat = scale//327/375
    private var gameType:Int = 0 // 胜平负
    
    private var tyeBgdIs:[String] = ["game_type_left","game_type_center","game_type_center","game_type_right"]
    private var basketIs:[String] = ["game_basket_type_left","game_basket_type_center","game_basket_type_center","game_basket_type_right"]
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView(frame:frame)
    }
    
}

//MARK: - 界面
extension GameBallCollCell{

    
    func buildView(frame:CGRect){
        // 投注项 位置
        var betbarH:CGFloat = ScreenWidth/4+40
        // 球队图标高度
        let imageW:CGFloat = 45*scale
        // 背景图高度
        let bgdIVH:CGFloat = ScreenWidth*0.95
        var bgdIVY:CGFloat = imageW+30 //height()-betbarH-bgdIVH
        
        if UIDevice.isX() {
            betbarH = ScreenWidth/4+60
            bgdIVY = imageW+60
        }
        
        
        betBar = GameBetBar(frame: CGRect(x: 0, y: height()-betbarH, width: ScreenWidth, height:betbarH))
        betBar.betAction = { [weak self] (tag) in self?.betAction(int: tag) }
        addSubview(betBar)
        
        let tX:CGFloat = ScreenWidth*(15/375)
        let tW:CGFloat = ScreenWidth/2-tX
        let tH:CGFloat = tW*(291/494)
        let deltY:CGFloat = betbarH*(98/263)
            
        rightTeamIV = UIImageView(frame: CGRect(x: ScreenWidth-tX-tW, y: bgdIVY-tH+deltY, width: tW, height: tH))
        rightTeamIV.image = UIImage(named: "bet_right_logo")
        addSubview(rightTeamIV)
        
        leftTeamIV = UIImageView(frame: CGRect(x: tX, y: bgdIVY-tH+deltY, width: tW, height: tH))
        leftTeamIV.image = UIImage(named: "bet_left_logo")
        addSubview(leftTeamIV)
        
        //--------------------- Level 1 -----------------------//
        bgdIV = UIImageView(frame: CGRect(x: 0, y: bgdIVY, width: ScreenWidth, height: bgdIVH*0.95))
        bgdIV.isUserInteractionEnabled = true
        bgdIV.image = UIImage(named: "bet_ball_bgd")
        bgdIV.contentMode = .scaleAspectFit
        addSubview(bgdIV)
        
        //--------------------- Level 2 -----------------------//
        compationL = UILabel(frame: CGRect(x: 0, y: 35*viewScale, width: ScreenWidth, height: 28*viewScale))
        compationL.textAlignment = .center
        compationL.textColor = UIColor.RGBHex(0x483f3f, alpha: 0.95)
        compationL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(compationL)
        
        //--------------------- Level 3 -----------------------//
        let labelY:CGFloat = 35*viewScale + 30*viewScale
        rightTeamL = UILabel(frame: CGRect(x: 0, y: labelY, width: ScreenWidth/2, height: 28*viewScale))
        rightTeamL.textAlignment = .center
        rightTeamL.textColor = UIColor.white
        rightTeamL.font = UIFont(name: kFontBold, size: 18)
        bgdIV.addSubview(rightTeamL)
        
        leftTeamL = UILabel(frame: CGRect(x: ScreenWidth/2, y:labelY, width: ScreenWidth/2, height: 28*viewScale))
        leftTeamL.textAlignment = .center
        leftTeamL.textColor = UIColor.white
        leftTeamL.font = UIFont(name: kFontBold, size: 18)
        bgdIV.addSubview(leftTeamL)
        
        let centerH:CGFloat = 28*scale
        let centerW:CGFloat = 15*scale
        let centerIV = UIImageView(frame: CGRect(x: ScreenWidth/2-centerW/2, y: labelY , width: centerW, height:centerH))
        centerIV.image = UIImage(named: "game_list_vs")
        centerIV.contentMode = .scaleAspectFit
        bgdIV.addSubview(centerIV)
        
        //--------------------- Level 4 -----------------------//
        
        let level4Y:CGFloat = 205/320*bgdIVH/0.95
        poolL = UILabel(frame: CGRect(x: ScreenWidth*(51/375), y: level4Y, width: 50, height: 28*viewScale))
        poolL.text = "总奖池".local
        poolL.textAlignment = .center
        poolL.textColor = UIColor.RGBHex(0xffffff, alpha: 0.6)
        poolL.font = UIFont(name: kFontTypeMedium, size: 13)
        
        bgdIV.addSubview(poolL)
        
        let spcL = UILabel(frame: CGRect(x: ScreenWidth-ScreenWidth*(51/375)-50, y: level4Y, width: 50, height: 28*viewScale))
        spcL.text = coinName
        spcL.textAlignment = .center
        spcL.textColor = UIColor.RGBHex(0xffffff, alpha: 0.6)
        spcL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(spcL)

        compationL = UILabel(frame: CGRect(x: 0, y: level4Y, width: ScreenWidth, height: 28*viewScale))
        compationL.textAlignment = .center
        compationL.textColor = UIColor.white
        compationL.font = UIFont(name: kFontTypeMedium, size: 20)
        bgdIV.addSubview(compationL)
        
        //--------------------- Level 5 -----------------------//
        let level5Y:CGFloat = bgdIVH*0.95-40*viewScale // level4Y + 28*viewScale
        let level5H:CGFloat = 40*viewScale
        
        let viewX:CGFloat = ScreenWidth*(16/375)
        let viewW:CGFloat = ScreenWidth-2*viewX
        let viewV = UIView(frame: CGRect(x:viewX, y: level5Y, width: viewW, height: level5H))
        bgdIV.addSubview(viewV)
        
        for i in 0...3{
            let buttonW:CGFloat = viewW/4
            let button = UIButton(frame: CGRect(x:buttonW*CGFloat(i), y: 0, width:buttonW, height: level5H))
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.white, for: .selected)
            button.backgroundColor = UIColor.clear
            button.tag = i
            if i == 0{
                button.isSelected = true
            }
            button.addTarget(self, action: #selector(typeAction(button:)), for: .touchUpInside)
            viewV.addSubview(button)
            buttons.append(button)
        }
    }
    
}

// MARK:- 配置数据
extension GameBallCollCell{

//        if type == .足球 {
//            bgdIV.image = UIImage(named: "game_football_select_bgd")
//            for (index,button) in buttons.enumerated(){
//                button.setTitle("123", for: .normal)
//                button.setBackgroundImage(UIImage(named: tyeBgdIs[index]), for: .selected)
//            }
//        }else{
//            bgdIV.image = UIImage(named: "game_basketball_select_bgd")
//            for (index,button) in buttons.enumerated(){
//                button.setTitle("123", for: .normal)
//                button.setBackgroundImage(UIImage(named: basketIs[index]), for: .selected)
//            }
//        }

    
    func configureModel(model:AppFootballM){
        footModel = model
        compationL.text = model.联赛名称
        rightTeamL.text = model.客队名称
        leftTeamL.text = model.主队名称
        poolL.text = "\(model.奖池金额)"
        betBar.setModels(models: model.玩法列表, betGameType: gameType)
        for (index,mo) in model.玩法列表.enumerated() {
            buttons[index].setTitle(mo.玩法名称, for: .normal)
            buttons[index].setTitle(mo.玩法名称, for: .selected)
        }
        for btn in buttons{
            btn.isSelected = false
        }
        
        buttons[0].isSelected = true
    }
    func configureModel(model:AppBasketballM){
        basketModel = model
        compationL.text = model.联赛名称
        rightTeamL.text = model.客队名称
        leftTeamL.text = model.主队名称
        poolL.text = "\(model.奖池金额)"
        betBar.setModels(models: model.玩法列表, betGameType: gameType)
        
        for (index,mo) in model.玩法列表.enumerated() {
            buttons[index].setTitle(mo.玩法名称, for: .normal)
            buttons[index].setTitle(mo.玩法名称, for: .selected)
        }
        for btn in buttons{
            btn.isSelected = false
        }
        
        buttons[0].isSelected = true
        
    }
    
    func hideBar(){
        UIView.animate(withDuration: 0.5) {
            self.betBar.alpha = 0
        }
    }
    
    func showBar(){
        UIView.animate(withDuration: 0.5) {
            self.betBar.alpha = 1
        }
    }

}

// MARK:- 回掉数据
extension GameBallCollCell{
    @objc func typeAction(button:UIButton){
        for btn in buttons {btn.isSelected = false }
        button.isSelected = true
        gameType = button.tag
        betBar.setModels(models:footModel.玩法列表 , betGameType: button.tag)
    }
    
    func betAction(int:Int){
        if betAction != nil{ betAction!(cellIndex.row,gameType,int) }
    }
    
}
