
import UIKit

class GameFootCollCell: UICollectionViewCell {
    
    var cellIndex:IndexPath!
    var betAction:((Int,Int,Int)->Void)?
    var footModel:AppFootballM = AppFootballM()
    
    private var betBar:GameBetBar!
    private var bgdIV:UIImageView!
    private var buttons:[UIButton] = []
    private var rightTeamIV:UIImageView!
    private var leftTeamIV:UIImageView!
    private var rightTeamL:UILabel!
    private var leftTeamL:UILabel!
    private var compationL:UILabel!
    private var poolL:UILabel!
    private var viewScale:CGFloat = scale//327/375
    private var gameType:Int = 0 // 胜平负
    private var tyeBgdIs:[String] = ["game_type_left","game_type_center","game_type_center","game_type_right"]
    private var countdownView:GameCommonCountdownView?
    private var footIs:[String] = ["game_type_left","game_type_center","game_type_center","game_type_right"]
    private var showBarFiratTime:Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView(frame:frame)
    }
}

//MARK: - 界面
extension GameFootCollCell{
    
    
    func buildView(frame:CGRect){
        
        // 投注项 位置
        var betbarH:CGFloat = ScreenWidth/4+20
        // 球队图标高度
        let imageW:CGFloat = 45*viewScale
        // 背景图高度
        let bgdIVH:CGFloat = ScreenWidth*0.95
        var bgdIVY:CGFloat = imageW+60 //height()-betbarH-bgdIVH
        
        if UIDevice.isX() {
            betbarH = ScreenWidth/4+40
            bgdIVY = imageW+80
        }
        
        // 投注栏
        betBar = GameBetBar(frame: CGRect(x: 0, y: height()-betbarH, width: ScreenWidth, height:betbarH))
        betBar.betAction = { [weak self] (tag) in self?.betAction(int: tag) }
        addSubview(betBar)
        
        /// 主背景
        bgdIV = UIImageView(frame: CGRect(x: 0, y: bgdIVY, width: ScreenWidth, height: bgdIVH*0.95))
        bgdIV.isUserInteractionEnabled = true
        bgdIV.contentMode = .scaleAspectFit
        bgdIV.image = UIImage(named: "game_football_select_bgd")
        addSubview(bgdIV)
        
        // 计时器
        let cvTop = -bgdIVY/2.0 - 34
        countdownView = GameCommonCountdownView(frame: CGRect(x: 0, y:cvTop, width: ScreenWidth, height: 20))
        bgdIV.addSubview(countdownView!)
        
        //--------------------- Level 1 -----------------------//
        rightTeamIV = UIImageView(frame: CGRect(x: bgdIV.width()-80*viewScale-imageW, y: -imageW, width: imageW, height: imageW))
        //rightTeamIV.image = UIImage(named: "White_default_down")
        bgdIV.addSubview(rightTeamIV)
        
        leftTeamIV = UIImageView(frame: CGRect(x:80*viewScale , y: -imageW, width: imageW, height: imageW))
        //leftTeamIV.image = UIImage(named: "White_default_down")
        bgdIV.addSubview(leftTeamIV)
        
        //--------------------- Level 2 -----------------------//
        compationL = UILabel(frame: CGRect(x: 0, y: 38*viewScale, width: ScreenWidth, height: 28*viewScale))
        compationL.textAlignment = .center
        compationL.textColor = UIColor.RGBHex(0x483f3f, alpha: 0.95)
        compationL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(compationL)
        
        //--------------------- Level 3 -----------------------//
        let labelY:CGFloat = 35*viewScale + 30*viewScale
        rightTeamL = UILabel(frame: CGRect(x: ScreenWidth/2, y: labelY, width: ScreenWidth/2, height: 28*viewScale))
        rightTeamL.textAlignment = .center
        rightTeamL.textColor = UIColor.white
        rightTeamL.font = UIFont(name: kFontBold, size: 16)
        bgdIV.addSubview(rightTeamL)
        
        leftTeamL = UILabel(frame: CGRect(x: 0, y:labelY, width: ScreenWidth/2, height: 28*viewScale))
        leftTeamL.textAlignment = .center
        leftTeamL.textColor = UIColor.white
        leftTeamL.font = UIFont(name: kFontBold, size: 16)
        bgdIV.addSubview(leftTeamL)
        
        let centerH:CGFloat = 28*scale
        let centerW:CGFloat = 15*scale
        let centerIV = UIImageView(frame: CGRect(x: ScreenWidth/2-centerW/2, y: labelY , width: centerW, height:centerH))
        centerIV.image = UIImage(named: "game_list_vs")
        centerIV.contentMode = .scaleAspectFit
        bgdIV.addSubview(centerIV)
        
        //--------------------- Level 4 -----------------------//
        
        let level4Y:CGFloat = 205/320*bgdIVH/0.95
        let poolTitleL = UILabel(frame: CGRect(x: ScreenWidth*(51/375), y: level4Y, width: 50, height: 28*viewScale))
        poolTitleL.text = "总奖池".local
        poolTitleL.textAlignment = .center
        poolTitleL.textColor = UIColor.RGBHex(0xffffff, alpha: 0.6)
        poolTitleL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(poolTitleL)
        
        let spcL = UILabel(frame: CGRect(x: ScreenWidth-ScreenWidth*(51/375)-50, y: level4Y, width: 50, height: 28*viewScale))
        spcL.text = coinName
        spcL.textAlignment = .center
        spcL.textColor = UIColor.RGBHex(0xffffff, alpha: 0.6)
        spcL.font = UIFont(name: kFontTypeMedium, size: 13)
        bgdIV.addSubview(spcL)
        
        poolL = UILabel(frame: CGRect(x: 0, y: level4Y, width: ScreenWidth, height: 28*viewScale))
        poolL.textAlignment = .center
        poolL.textColor = UIColor.white
        poolL.font = UIFont(name: kFontTypeMedium, size: 20)
        bgdIV.addSubview(poolL)
        
        //--------------------- Level 5 -----------------------//
        let level5Y:CGFloat = bgdIVH*0.95-40*viewScale // level4Y + 28*viewScale
        let level5H:CGFloat = 40*viewScale
        
        let viewX:CGFloat = ScreenWidth*(16/375)
        let viewW:CGFloat = ScreenWidth-2*viewX
        let viewV = UIView(frame: CGRect(x:viewX, y: level5Y, width: viewW, height: level5H))
        bgdIV.addSubview(viewV)
        
        for i in 0...3{
            let buttonW:CGFloat = viewW/4
            let button = UIButton(frame: CGRect(x:buttonW*CGFloat(i), y: 0, width:buttonW, height: level5H))
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.white, for: .selected)
            button.backgroundColor = UIColor.clear
            button.tag = i
            button.titleLabel?.font = UIFont(name: kFontType, size: 13)
            button.setBackgroundImage(UIImage(named: footIs[i]), for: .selected)
            if i == 0{
                button.isSelected = true
                UmengEvent.eventWithDic(name: "footerball_playType",dic: ["playType":"0"])
            }
            button.addTarget(self, action: #selector(typeAction(button:)), for: .touchUpInside)
            viewV.addSubview(button)
            buttons.append(button)
        }

    }
    
}

// MARK:- 配置数据
extension GameFootCollCell{
    
    func configureModel(model:AppFootballM){
        showBarFiratTime = true
        footModel = model
        compationL.text = model.联赛名称
        rightTeamL.text = model.客队名称
        leftTeamL.text = model.主队名称
        poolL.text = "\(model.奖池金额)"
        //默认刷新时，总是选择第一个选项（胜平负）
        gameType = 0
        betBar.setModels(models: model.玩法列表, betGameType: gameType)
//        countdownView?.setTime(time: model.比赛时间_秒)
        countdownView?.setOnlyTime(time: model.比赛时间_秒)

        if let url = URL(string: model.客队图标){ rightTeamIV.kf.setImage(with: url, placeholder: UIImage(named: "game_visit_icon"), options: nil, progressBlock: nil, completionHandler: nil) }else{rightTeamIV.image = UIImage(named: "game_visit_icon")}
        if let url = URL(string: model.主队图标){ leftTeamIV.kf.setImage(with: url,placeholder: UIImage(named: "game_host_icon"), options: nil, progressBlock: nil, completionHandler: nil) }else{leftTeamIV.image = UIImage(named: "game_host_icon")}
        
        for (index,mo) in model.玩法列表.enumerated() {
            buttons[index].setTitle(mo.玩法名称, for: .normal)
            buttons[index].setTitle(mo.玩法名称, for: .selected)
        }

        for btn in buttons{ btn.isSelected = false }
        buttons[0].isSelected = true
    }
    
    func hideBar(){
        if showBarFiratTime {
            UIView.animate(withDuration: 0.3) {
                self.betBar.alpha = 0
            }
        }
    }
    
    func showBar(){
        UIView.animate(withDuration: 0.3) {
            self.betBar.alpha = 1
        }
    }
    
    //按钮放大缩小动画
    func betBSAnimation(){
        if self.betBar.alpha == 1{
            self.betBar.btnBSAnimation()
        }
        bgdIV.animationBigSmall(time: 0.5, transform3D: [CATransform3DMakeScale(0.95, 0.95, 1.0),CATransform3DMakeScale(1.03, 1.03, 1.0),CATransform3DMakeScale(0.98, 0.98, 1.0),CATransform3DMakeScale(1, 1, 1.0)])
    }
}

// MARK:- 回掉数据
extension GameFootCollCell{
    @objc func typeAction(button:UIButton){
        
        for btn in buttons {btn.isSelected = false }
        UmengEvent.eventWithDic(name: "footerball_playType",dic: ["playType":"\(button.tag)"])
        button.isSelected = true
        gameType = button.tag
        betBar.setModels(models:footModel.玩法列表 , betGameType: button.tag)
    }
    
    func betAction(int:Int){
        if betAction != nil{ betAction!(cellIndex.row,gameType,int) }
    }
    

    
}


