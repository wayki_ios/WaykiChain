
import UIKit

class AppEventBetM: NSObject {
    var 投注时间:String = "" //判断是否已投注
    var 赛事唯一ID:String = "" // lid 赛事唯一ID,组装投注合约时要用
    
    var 币符号:String = "" // symbol ，例如：WICC
    var 赛事id主键:Int = 0 // lotteryid 赛事id主键,投注时要作为参数上传
    /// 合约地址
    var 事件主体:String = "" // league
    var 事件主体的高度:CGFloat = 0
    var 事件列表主体高度:CGFloat = 0
    
    var 事件依据:String = "" // lnum

    var 比赛时间_秒:Double = 0 // bjdate (string
    var 截止时间:String = "" // bjdate (string
    var 选项A名称:String = "" // hometeam
    var 主题banner:String = "" // hometeamicon
    
    var 选项B名称:String = "" // visitingteam
    var 客队图标:String = "" // visitingteamicon
    var 玩法列表:[AppEventTypeM] = []    // playtypes
    var 投注次数:Int = 0 // betnum integer
    var 是否热门:Int = 0 // hot (true:热门,false:非热门)
    var 奖池金额:Int = 0 // jackpot
    var 废弃:Bool = false // 0:不废除，1，废除
    
    
    class func getModels(json:JSON)->[AppEventBetM]{
        var array:[AppEventBetM] = []
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{if status != 1 {return array }}
            if let results = dic["result"]?.array{
                for res in results {
                    let model = AppEventBetM()
                    model.getModel(json:res)
                    if !model.废弃{
                        array.append(model)
                    }
                }
            }
        }
        return array
    }
    
    func getModel(json:JSON){
        
        玩法列表 = AppEventTypeM.getModels(json: json["playtypes"])
        
        if let sep = json["league"].string{
            废弃 = sep.range(of: "【") == nil
            if 废弃{ return }
        }
        if let sep = json["symbol"].string{ 币符号 =  sep }
        if let sep = json["lid"].string{ 赛事唯一ID =  sep }
        if let sep = json["league"].string{
            事件主体 = sep
            let hi = labelHeight(text: 事件主体, font: UIFont.boldSystemFont(ofSize: 13), width: (ScreenWidth - ScreenWidth*120.0/750.0)/1.2-20)
            事件主体的高度 = hi > 60 ? 60:hi + 20
        }
        if let sep = json["lnum"].string{ 事件依据 = sep }
        
        if let sep = json["bjdate"].double{
            截止时间 = Date(timeIntervalSince1970: TimeInterval(sep/1000)).dayString()
        }
        if let sep = json["bjdate"].double{ 比赛时间_秒 = sep/1000}
        if let sep = json["lotteryid"].int{ 赛事id主键 =  sep }
        if let sep = json["hometeam"].string{
            选项A名称 =  sep
            玩法列表[0].投注项列表[0].投注项名称 = 选项A名称
        }
        if let sep = json["hometeamicon"].string{ 主题banner =  sep }
        if let sep = json["visitingteam"].string{
            选项B名称 =  sep
            玩法列表[0].投注项列表[1].投注项名称 = 选项B名称
        }
        if let sep = json["visitingteamicon"].string{ 客队图标 =  sep }
        
        if let sep = json["betnum"].int{ 投注次数 =  sep }
        if let sep = json["hot"].int{ 是否热门 =  sep }
        if let sep = json["jackpot"].int{ 奖池金额 =  sep }
        
    }
    
    class func getSingleModel(json:JSON)->AppEventBetM{
        let model = AppEventBetM()
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{if status != 1 {return model }}
            model.getModel(json:dic["result"]!)
        }
        return model
    }
}



// 胜平负 + 大小
class AppEventTypeM:NSObject{ // playtypes
    var 投注项列表:[AppEventBetTypeM] = [] // bettypes
    var 玩法名称:String = ""       // name 玩法名称
    var 玩法类型:Int = 0 // playtype (用于组成投注合约报文)
    
    class func getModels(json:JSON) -> [AppEventTypeM]{
        var array:[AppEventTypeM] = []
        if let models = json.array {
            for m in models{
                let model = AppEventTypeM()
                model.getModel(json: m)
                array.append(model)
            }
        }
        return array
    }
    
    func getModel(json:JSON){
        投注项列表 = AppEventBetTypeM.getModels(json: json["bettypes"])
        if let sep = json["name"].string{ 玩法名称 =  sep }
        if let sep = json["playtype"].int{ 玩法类型 =  sep }
    }
    
}

// 胜 平 负
class AppEventBetTypeM:NSObject{ // bettypes
    var 投注项赔率:Double = 0 // betodds
    var 投注项:Int = 0       // bettype 用于组成投注合约报文
    var 投注项名称:String = "" // name
    var 投注项支持率:Double = 0 // sprate
    class func getModels(json:JSON) -> [AppEventBetTypeM]{
        var array:[AppEventBetTypeM] = []
        if let models = json.array {
            for m in models{
                let model = AppEventBetTypeM()
                model.getModel(json: m)
                array.append(model)
            }
        }
        return array
    }
    
    func getModel(json:JSON){
        if let sep = json["betodds"].double{ 投注项赔率 =  sep }
        if let sep = json["bettype"].int{ 投注项 =  sep }
        if let sep = json["name"].string{ 投注项名称 =  sep }
        if let sep = json["sprate"].double{ 投注项支持率 =  sep }
    }
}
