

import UIKit

class CenterC: UIViewController {
    var gameType:GameType = .足球
    private var switcher:GameHeaderSwitcher!
    private var bgdImageV:UIImageView!
    
    private var footC:GameFootballC!
    private var basktC:GameBasketballC!
    private var eventC:GameEventC!
    private let footBgdI:String = "main_foot"
    private let basketBgdI:String = "main_basket"
    private let eventBgdI:String = "main_event"
    private let animationDuration:Double = 0.3
    private var leftBtn:UIButton!
    private var rightBtn:UIButton!
    private var activityImage:UIImageView!
    private var activityIs:[String] = ["bet_main_activity_1","bet_main_activity_2"]
    private var timer:Timer?
    private var redBuddageIV:UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        创建主界面()
        配置导航条()
//        CheckManager.checkIfSignUp { }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       //switcher.shakerAnimation(time: 0.4, iValue: [0,2,-2,0], keyTimes: [0,0.3,0.7,1])
        animation()
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
}

extension CenterC{
    
    private func animation(){
        switcher.animationBigSmall()
        leftBtn.animationBigSmall()
        rightBtn.animationBigSmall()
        if gameType == .足球 {
            footC.view.animationBigSmall(range: 0.03)
        }else if gameType == .篮球{
            basktC.view.animationBigSmall(range: 0.03)
        }else if gameType == .事件 {
            eventC.view.animationBigSmall(range: 0.03)
        }
    }
    
    private func addTimer(){
        timer = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(activityAnimation), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.commonModes)
        timer?.fire()
    }
    
    @objc private func activityAnimation(){
        
    }
    
}

extension CenterC{
    func 创建主界面(){
        bgdImageV = UIImageView(frame: CGRect(x: -1, y: -1, width:view.bounds.size.width+2 , height: ScreenHeight+2))
        bgdImageV.image = UIImage(named: "main_foot"+device())
        view.clipsToBounds = true
        view.addSubview(bgdImageV)
    }
    
    func 配置导航条(){
        leftBtn = UIButton(frame: CGRect(x: 0, y: naviHeight-50, width: 50, height: 50))
        leftBtn.layer.cornerRadius = 15
        leftBtn.setImage(UIImage(named: "main_my"), for: .normal)
        leftBtn.setImage(UIImage(named: "main_my"), for: .highlighted)
        leftBtn.addTarget(self, action: #selector(CenterC.leftButtonAction), for: .touchUpInside)
        view.addSubview(leftBtn)
        
        redBuddageIV = UIImageView(frame: CGRect(x: 35, y: 12, width: 8, height: 8))
        redBuddageIV.image = UIImage(named: "left_people_red")
        leftBtn.addSubview(redBuddageIV)
        
        if UserDefaults.standard.bool(forKey: "left_people_new"){
            redBuddageIV.isHidden = true
        }
        
        rightBtn = UIButton(frame: CGRect(x: ScreenWidth-50, y: naviHeight-50, width: 50, height: 50))
        rightBtn.layer.cornerRadius = 15
        rightBtn.setImage(UIImage(named: "main_game"), for: .normal)
        rightBtn.setImage(UIImage(named: "main_game"), for: .highlighted)
        rightBtn.addTarget(self, action: #selector(CenterC.rightButonAction), for: .touchUpInside)
        view.addSubview(rightBtn)
        
        switcher = GameHeaderSwitcher(frame: CGRect(x: 70, y: naviHeight-50, width: ScreenWidth-140, height: 50))
        switcher.setTile()
        switcher.switchAction = {[weak self] index in self?.切换投注类型(int: index) }
        
        view.addSubview(switcher)
        
        footC = GameFootballC()
        footC.updatedNoDate = {[weak self] in self?.changeBackgroundImage()}
        footC.swipeAction = {[weak self] index in  self?.切换投注类型(int: index)}
        
        basktC = GameBasketballC()
        basktC.updatedNoDate = {[weak self] in self?.changeBackgroundImage()}
        basktC.swipeAction = {[weak self] index in  self?.切换投注类型(int: index)}
        
        eventC = GameEventC()
        eventC.updatedNoDate = {[weak self] in self?.changeBackgroundImage()}
        eventC.swipeAction = {[weak self] index in  self?.切换投注类型(int: index)}
        
        view.addSubview(footC.view)
        view.addSubview(basktC.view)
        view.addSubview(eventC.view)
        
        self.basktC.view.alpha = 0
        self.eventC.view.alpha = 0
        
        addTimer()
        activityAnimation()
        
        HomeUserInfoManager.show(v: self.view, imgName: "1")
        
        
    }
    
    
}

// MARK:- 顶部事件
extension CenterC{
    
    
    private func 切换投注类型(int:Int){
        switcher.moveToIndex(index: int)
        UIView.animate(withDuration: animationDuration) {
            self.footC.view.alpha = 0
            self.basktC.view.alpha = 0
            self.eventC.view.alpha = 0
        }
        
        if int == 0 {
            gameType = .足球
            UmengEvent.eventWithDic(name: "click_betType", dic: ["type":"football"])
            UIView.animate(withDuration: animationDuration) {
                self.changeBackgroundImage()
                self.footC.view.alpha = 1
            }
        }else if int == 1 {
            gameType = .篮球
            UmengEvent.eventWithDic(name: "click_betType", dic: ["type":"basketball"])
            UIView.animate(withDuration: animationDuration) {
                self.changeBackgroundImage()
                self.basktC.view.alpha = 1
            }
        }else {
            gameType = .事件
            UmengEvent.eventWithDic(name: "click_betType", dic: ["type":"event"])
            UIView.animate(withDuration: animationDuration) {
                self.changeBackgroundImage()
                self.eventC.view.alpha = 1
            }
        }
    }
    
    private func changeBackgroundImage(){
        if gameType == .足球 {
            if self.footC.datas.count > 0{
                self.bgdImageV.image = UIImage(named: self.footBgdI+self.device())
            }else{
                self.bgdImageV.image = UIImage(named: self.footBgdI+"_nodata"+self.device())
            }
        }else if gameType == .篮球{
            if self.basktC.datas.count > 0{
                self.bgdImageV.image = UIImage(named: self.basketBgdI+self.device())
            }else{
                self.bgdImageV.image = UIImage(named: self.basketBgdI+"_nodata"+self.device())
            }
        }else if gameType == .事件 {
            if self.eventC.datas.count > 0{
                self.bgdImageV.image = UIImage(named: self.eventBgdI+self.device())
            }else{
                self.bgdImageV.image = UIImage(named: self.eventBgdI+"_nodata"+self.device())
            }
        }
    }
    
    private func device()->String{
        return UIDevice.isX() ? "_x":""
    }
    
    @objc func rightButonAction(){
        let c = GameListC()
        if gameType == GameType.事件{
            c.gameType = .事件
            c.setDatas(eventData: eventC.datas)
        }else if gameType == GameType.篮球{
            c.gameType = .篮球
            c.setDatas(basketballData: basktC.datas)
        }else{
            c.gameType = .足球
            c.setDatas(footballData: footC.datas)
        }
        c.cellAction = {[weak self] (index) in self?.currentMoveToIndex(index: index) }
        navigationController?.pushViewController(c, animated: true)
    }
    
    @objc func leftButtonAction(){
        if !UserDefaults.standard.bool(forKey: "left_people_new"){
            UserDefaults.standard.set(true, forKey:"left_people_new")
            redBuddageIV.isHidden = true
        }
        
        
        NavigationManager.shared.drawerC?.toggleLeftDrawerSide(animated: true, completion: nil)
    }
    
    func currentMoveToIndex(index:Int){
        if gameType == GameType.事件{
            eventC.currentMoveToIndex(int: index)
        }else if gameType == GameType.篮球{
            basktC.currentMoveToIndex(int: index)
        }else{
            footC.currentMoveToIndex(int: index)
        }
    }
    
}



