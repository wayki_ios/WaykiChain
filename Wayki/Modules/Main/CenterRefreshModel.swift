

import UIKit

@objc protocol CenterRefreshProtocol {
    
}

class CenterRefreshModel: NSObject {
    var type:GameType =  GameType.足球
    var isRefresh:Bool = false
    var currentPage:Int = 1
    var datas:[Any] = []

    private var basketballDatas:[AppBasketballM] = []
    private var footballDatas:[AppFootballM] = []
    private var eventDatas:[AppEventBetM] = []
}

extension CenterRefreshModel{
    func requestBaskerballData(runHUD:Bool=false){
        let typepath = HTTPPath.篮球赛事_G
        let requestpath = httpPath(path: typepath) + "?page=\(currentPage)&rows=35"
        LHRequest.get(url: requestpath,parameters: [:],runHUD: runHUD ? .loading:.none, success: {[weak self] (json) in
            self?.handleBaskerballData(json: json)
        }) { (error) in }
    }
    
    func handleBaskerballData(json:JSON){

        let new:[AppBasketballM] = AppBasketballM.getModels(json: json)
        if isRefresh {
            //如果是刷新
            basketballDatas = []
            basketballDatas += new
        }else{
            if new.count == 0 { //没有新数据了
                currentPage = currentPage-1 == 0 ? 1:currentPage-1
            }else{ //
                basketballDatas += new
            }
  
        }
        basketballDatas.sort { (s1:AppBasketballM, s2:AppBasketballM) -> Bool in
            return s1.比赛时间_秒 < s2.比赛时间_秒
        }
        basketballDatas.sort { (s1:AppBasketballM, s2:AppBasketballM) -> Bool in
            return s1.是否热门 > s2.是否热门
        }
      
    }
}
