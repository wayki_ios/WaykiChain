

import UIKit

// MARK:- 标题切换器
class GameHeaderSwitcher:FoundationV{
    private var buttons:[UIButton] = []
    private var line:UIView!
    private let normals:[String] = ["game_type_foot","game_type_basket","game_type_wiki"]
    private let selecteds:[String] = ["game_type_foot_selected","game_type_basket_selected","game_type_wiki_selected"]
    
    
    var switchAction:((Int)->Void)?
    override func buildView(frame: CGRect) {
        for i in 0...2{
            let btn = UIButton(frame: CGRect(x: width()/3*CGFloat(i), y: 0, width: width()/3, height: height()))
            btn.setTitleColor(UIColor.white, for: .selected)
            btn.setTitleColor(UIColor.RGB(r: 255, g: 255, b: 255,alpha: 0.4), for: .normal)
            btn.tag = i
            btn.titleLabel?.font = UIFont(name: kFontTypeThin, size: 15)
            if UIDevice.model() == "5"{
                btn.titleLabel?.font = UIFont(name: kFontTypeMedium, size: 12)
            }
            btn.addTarget(self, action: #selector(buttonAction(btn:)), for: .touchUpInside)
            addSubview(btn)
            buttons.append(btn)
        }
        line = UIView(frame: CGRect(x: (width()/3-14)/2, y: height(), width: 14, height: 2))
        line.backgroundColor = UIColor.RGBHex(0xef3f50)
        addSubview(line)
        buttons[0].isSelected = true
    }
    
    @objc func buttonAction(btn:UIButton){
        moveToIndex(index: btn.tag)
        if switchAction != nil{
            switchAction!(btn.tag)
        }
    }
    
    /// 设置标题
    func setTile(titles:[String]=["足球".local,"篮球".local,"事件".local]){
        for (index,btn) in buttons.enumerated(){
            if titles.count > index{
                btn.setTitle(titles[index], for: .normal)
                btn.setTitle(titles[index], for: .selected)
            }
        }
    }
    
    /// 切换到对应的按钮
    func moveToIndex(index:Int){
        for btn in buttons{ btn.isSelected = false }
        let lineX:CGFloat = self.width()/3*CGFloat(index)+(self.width()/3-14)/2
        if index < buttons.count{
            UIView.animate(withDuration: 0.2) {
                self.line.frame.origin.x = lineX
            }
            buttons[index].isSelected = true
        }
    }
    
}
