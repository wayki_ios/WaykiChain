

import UIKit


class LeftC: UIViewController {
    
    private var wiccCountL:UILabel!
    private var spcCountL:UILabel!
    private var 激活按钮:UIButton!
    private var totalCountL:UILabel!
    private var addressL:UILabel!
    private var newIVs:[UIImageView] = []
    
    
    var leftAction:((LeftActionType)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        创建主界面()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getWealthSum()
        let account = AccountManager.getAccount()
        if account.regId.count>0 &&  account.regId.count<3{
            self.getRegId()
            
        }
    }
    
}


// MARK:- 界面搭建
extension LeftC{
    
    func 创建主界面(){
        let backgroundIV = UIImageView(frame: CGRect(x: 0, y: 0, width:view.bounds.size.width , height: ScreenHeight))
        backgroundIV.image = UIImage(named: "my_bgd")
        view.addSubview(backgroundIV)
        
        创建资产页()
        创建功能模块()
        self.getWealthSum()
        self.getRegId()
 
    }
    
    func 创建资产页(){
        let assertX:CGFloat = leftWidth*(0/314)
        let assertW:CGFloat = leftWidth - assertX*2
        let assertY:CGFloat = view.bounds.height*(60/667)
        let assertH:CGFloat = assertW*(390/622)
        let assertIV = UIImageView(frame: CGRect(x: assertX, y: assertY, width: assertW, height: assertH))
        assertIV.image = UIImage(named: "my_assert")
        assertIV.isUserInteractionEnabled = true
        view.addSubview(assertIV)
        
        
        let bgdV = UIView(frame: CGRect(x: assertIV.width()*(46/622),
                                        y: assertIV.height()*(32/388),
                                        width: assertIV.width()*(530/622),
                                        height: assertIV.height()*(294/388)))
        assertIV.addSubview(bgdV)
        
        let titleX:CGFloat = 10
        let titleH:CGFloat = bgdV.height()*(40/150)
//        let titleW:CGFloat = 70
        let titleY:CGFloat = 0
        
        let str = "总资产(CNY)".local
        let w = NSString.getWidthWithText(str, height: 19, fontSize: 12) + 10
        let titleL = UILabel(frame: CGRect(x: titleX, y: titleY, width:w , height:titleH))
        titleL.backgroundColor = UIColor.clear
        titleL.textColor = .white
        titleL.text = "总资产(CNY)".local
        titleL.font = UIFont(name: "PingFangSC-Medium", size: 12)
        bgdV.addSubview(titleL)
        
        激活按钮 = UIButton(frame: CGRect(x: titleL.right() + 10, y: titleY, width:bgdV.width()-titleL.right() - 10, height:titleH))

        let attr = NSMutableAttributedString.init(string: "激活账户".local)
        let number = NSNumber(integerLiteral: NSUnderlineStyle.styleSingle.rawValue)
        attr.addAttributes([NSAttributedStringKey.underlineStyle:number,NSAttributedStringKey.foregroundColor:UIColor.white], range: NSRange.init(location: 0, length: "激活账户".local.count))
        激活按钮.setAttributedTitle(attr, for: .normal)
        激活按钮.contentHorizontalAlignment = .left
        激活按钮.titleLabel?.font = UIFont(name: kFontTypeThin, size: 11)
        激活按钮.addTarget(self, action: #selector(LeftC.激活账户(button:)), for: .touchUpInside)
        激活按钮.isHidden = true
        bgdV.addSubview(激活按钮)
        
        // 总资产
        let totalCountX:CGFloat = titleX
        let totalCountY:CGFloat = bgdV.height()*(50/148)
        let totalCountW:CGFloat = bgdV.width()*2/3
        let totalCountH:CGFloat = bgdV.height()*(25/148)
        
        let totalSyL = UILabel(frame: CGRect(x:totalCountX, y: totalCountY, width:10, height:totalCountH))
        totalSyL.backgroundColor = UIColor.clear
        totalSyL.font = UIFont(name: kFontTypeMedium, size: 12)
        totalSyL.textColor = UIColor.white
        totalSyL.text = "¥"
        totalSyL.font = UIFont(name: kFontTypeThin, size: 14)
        bgdV.addSubview(totalSyL)
        
        totalCountL = UILabel(frame: CGRect(x:totalCountX+10, y: totalCountY, width:totalCountW-10, height:totalCountH))
        totalCountL.backgroundColor = UIColor.clear
        totalCountL.font = UIFont(name: "Helvetica-Bold", size: 18)
        totalCountL.textColor = UIColor.white
        bgdV.addSubview(totalCountL)
        
        // 地址
        let addressX:CGFloat = titleX
        let addressW:CGFloat = bgdV.width()*2/3
        let addressY:CGFloat = totalCountH+totalCountY
        let addressH:CGFloat = 20
        addressL = UILabel(frame: CGRect(x:addressX, y: addressY, width:addressW, height:addressH))
        addressL.backgroundColor = UIColor.clear
        addressL.font = UIFont(name: "PingFangSC-Thin", size: 11)
        addressL.textColor = UIColor.white
        bgdV.addSubview(addressL)
        
        // 二维码
        let addressButton = UIButton(frame: CGRect(x: bgdV.width()-40, y: totalCountY+5, width:30, height: 30))
        addressButton.setImage(UIImage(named: "main_qrcode"), for: .normal)
        addressButton.setImage(UIImage(named: "main_qrcode"), for: .highlighted)
        addressButton.tag = 0
        addressButton.addTarget(self, action: #selector(LeftC.资产事件(btn:)), for: .touchUpInside)
        bgdV.addSubview(addressButton)
        
        // WICC
        let countY:CGFloat = addressH + addressY
        let countH:CGFloat = bgdV.height()-addressY-addressH
        let countW:CGFloat = bgdV.width()/2
        let wiccButton = UIButton(frame: CGRect(x:0, y: countY, width: countW, height:countH))
        wiccButton.tag = 1
        wiccButton.addTarget(self, action: #selector(LeftC.资产事件(btn:)), for: .touchUpInside)
        bgdV.addSubview(wiccButton)
        
        wiccCountL = UILabel(frame: CGRect(x: 0, y: countH/2-20, width: countW, height: 20))
        wiccCountL.font = UIFont(name: kFontTypeMedium, size: 15)
        wiccCountL.textAlignment = .center
        wiccCountL.textColor = UIColor.white
        wiccButton.addSubview(wiccCountL)
        
        let wiccL = UILabel(frame: CGRect(x: 0, y: countH/2, width: countW, height: 20))
        wiccL.text = "WICC"
        wiccL.font = UIFont(name: kFontItalic, size: 12)
        wiccL.textAlignment = .center
        wiccL.textColor = UIColor.white
        wiccButton.addSubview(wiccL)
        
        // SPC
        let spcButton = UIButton(frame: CGRect(x: countW, y: countY, width: countW, height: countH))
        spcButton.tag = 2
        spcButton.addTarget(self, action: #selector(LeftC.资产事件(btn:)), for: .touchUpInside)
        bgdV.addSubview(spcButton)

        spcCountL = UILabel(frame: CGRect(x: 0, y: countH/2-20, width: countW, height: 20))
        spcCountL.font = UIFont(name: kFontTypeMedium, size: 15)
        spcCountL.textAlignment = .center
        spcCountL.textColor = UIColor.white
        spcButton.addSubview(spcCountL)

        let spcL = UILabel(frame: CGRect(x: 0, y: countH/2, width: countW, height: 20))
        spcL.text = coinName
        spcL.font = UIFont(name: kFontItalic, size: 12)
        spcL.textAlignment = .center
        spcL.textColor = UIColor.white
        spcButton.addSubview(spcL)

        let line = UIView(frame: CGRect(x: -0.25, y: countH/2-15, width: 0.5, height: 30))
        line.backgroundColor = UIColor.white
        spcButton.addSubview(line)
        
        
    }
    
    func 创建功能模块(){
        let distance:CGFloat =  ScreenHeight*(20/667)
        let images = ["my_lock","my_exchange","my_lucky","my_bet","my_wallet","my_transfer","my_helpcenter"]
        let titles = ["我的竞猜".local,"钱包管理".local,"立即转帐".local]

        for i in 0..<titles.count{
            let Y:CGFloat = ScreenHeight*(240/667)+(distance+28)*CGFloat(i)
            创建按钮(frame: CGRect(x: 0, y: Y, width: ScreenWidth, height: 50),image:images[i],title: titles[i],tag:i)
        }
      
    }
    
    func 创建按钮(frame:CGRect ,image:String,title:String,tag:Int){
        
        let button = UIButton(frame: frame)
        button.setBackgroundImage(UIImage(named: "left_height_bgd"), for: .highlighted)
        button.imageView?.alpha = 0.2
        button.tag = tag
        button.addTarget(self, action: #selector(LeftC.模块事件(btn:)), for: .touchUpInside)
        view.addSubview(button)
        //leftButtons.append(button)
        
        let X = leftWidth*(30/314)
        let imageV = UIImageView(frame:CGRect(x:X, y: (frame.size.height-30)/2, width: 30, height: 30) )
        imageV.image = UIImage(named: image)
        button.addSubview(imageV)
        
        let titleL = UILabel(frame: CGRect(x: X+50, y:(frame.size.height-30)/2, width: 150, height: 30))
        titleL.text = title
        titleL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        titleL.font = UIFont(name: "PingFangSC-Medium", size: 17)
        button.addSubview(titleL)
        view.addSubview(button)
        if tag < 3{
            let newIV = UIImageView(frame:CGRect(x:leftWidth-80, y: (frame.size.height-15)/2, width: 30, height: 15) )
            newIV.image = UIImage(named: "left_first_new")
            button.addSubview(newIV)
            newIVs.append(newIV)
            showNewImageV(iV: newIV, tag: tag)
        }
        
    }

    func showNewImageV(iV:UIImageView,tag:Int){
        if tag == 0{
           iV.isHidden = UserDefaults.standard.bool(forKey: "lock_plan_new")
        }else if tag == 1{
           iV.isHidden = UserDefaults.standard.bool(forKey: "lock_exchange_new")
        }else if tag == 2{
           iV.isHidden = UserDefaults.standard.bool(forKey: "lock_lucky_new")
        }
    }
    
    func hideNewImageV(tag:Int){
        if tag == 0{
            UserDefaults.standard.set(true, forKey: "lock_plan_new")
            newIVs[0].isHidden = true
        }else if tag == 1{
            UserDefaults.standard.set(true, forKey: "lock_exchange_new")
            newIVs[1].isHidden = true
        }else if tag == 2{
            UserDefaults.standard.set(true, forKey: "lock_lucky_new")
            newIVs[2].isHidden = true
        }
    }
    
    
}

// MARK:- 事件处理
extension LeftC{
    
    func 配置界面数据(result:[JSON]){
        if let firstDic = result.first?.dictionary{
            var num:Float = 0
            var price:Float = 0
            if let amount = firstDic["amount"]?.float{
                num = amount
            }
            if let p = firstDic["price"]?.float{
                price = p
            }
            totalCountL.text = String(format: "%.2f",num * price)
            wiccCountL.text = String(format: "%.2f",num)
        }
        if let lastDic = result.last?.dictionary{
            if let amount = lastDic["amount"]?.float{
                spcCountL.text = String(format: "%.2f",amount)
            }
        }
        
        addressL.text = AccountManager.getAccount().address
    }
    
    @objc func 激活账户(button:UIButton){
        
        UmengEvent.eventWithDic(name: "activate_wallet")
        let a = AccountManager.getAccount()
        if a.regId.count>2 {
            
        }else{
            
            AlertInputPwdView.shared.show().sureBlock = {[weak self] pwd in
                //先获取高度，再激活钱包
                LHRequest.getVaildHeight { (height, address, appid) in
                    self?.actrivateWallet(height,pwd: pwd)
                }

            }
        }
        
    }
    
    
    @objc func 资产事件(btn:UIButton){
        if leftAction == nil { return }
        switch btn.tag {
        case 0:
            leftAction!(.QRCode)
            break
        case 1:
            leftAction!(.WICC)
            break
        case 2:
            leftAction!(.SPC)
            break
        default:
            print("")
        }
    }
    
    @objc func 模块事件(btn:UIButton){
        if leftAction == nil { return }
        hideNewImageV(tag: btn.tag)
        switch btn.tag {
        case 0:
            leftAction!(.Bet)
            break
        case 1:
            leftAction!(.Wallet)
            break
        case 2:
            leftAction!(.Transfer)
            break
        case 3:
            leftAction!(.HelpCenter)
            break
        default:
            print("")
        }
    }
}
////网络请求分类
extension LeftC{
    // 资产汇总
    func getWealthSum(){

        let account = AccountManager.getAccount()
        let path:String = httpPath(path: HTTPPath.获取资产汇总_G)
        let address = account.address
        
        
        let mHash = account.mHash
        let requestPath = path + mHash + "/\(address)"
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        
        LHRequest.get(url: getPath,parameters: [:],runHUD: .none, success: {[weak self] (json) in
            if let dic = json.dictionary{
                if let status = dic["status"]?.int{
                    if status == 1{
                        if let arr = dic["result"]?.array{
                            self?.配置界面数据(result: arr)
                            let models = WealthSumModel.getModels(json: json)
                            if models.count == 2{
                                let account = AccountManager.getAccount()
                                account.wiccSumAmount = WealthSumModel.getModels(json: json)[0].amount
                                account.spcSumAmount = WealthSumModel.getModels(json: json)[1].amount
                                AccountManager.saveAccount(account: account)
                            }
                        }
                    }
                }
            }

        }) { (error) in }
    }
    //获取regId
    func getRegId(){
        
        let path:String = httpPath(path: HTTPPath.获取地址regid_G)
        let address = AccountManager.getAccount().address
        let mHash =  AccountManager.getAccount().mHash
        let requestPath = path + mHash + "/" + address
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        
        LHRequest.get(url: getPath,parameters: [:],runHUD: .none, success: {[weak self] (json) in
            
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {
                        
                    }}
                if let result = dic["result"]?.stringValue{
                    if result.count>0{
                        let account:NewAccount = AccountManager.getAccount()
                        account.regId = result
                        AccountManager.saveAccount(account: account)
                        self?.激活按钮.isHidden = true
                        return
                    }
                }
            }
            
            if AccountManager.getAccount().regId.count>0 {
                self?.激活按钮.isHidden = true
            }else{
                self?.激活按钮.isHidden = false
            }
            
        }) { (error) in }
    }
    //激活钱包
    func actrivateWallet(_ height:Double,pwd:String){
        
        let amount = AccountManager.getAccount().wiccSumAmount
        if amount == 0 {
            UILabel.showFalureHUD(text: "账户余额为0，无法激活".local)
            return
        }
        let acccount = AccountManager.getAccount()
        let path:String = httpPath(path: HTTPPath.激活地址_P)
        let mHash = acccount.mHash
        let helpStr = acccount.getHelpString(password: pwd)
        let signHex = Bridge.getActrivateHex(withHelpStr: helpStr, withPassword: pwd, fees: 0.0001, validHeight: height)
        
        let requestPath = path + mHash
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        var pa:[String:Any] = [:]
        
        pa["signHex"] = signHex
        pa["txRemark"] = ""
        
        LHRequest.post(url: getPath, parameters: pa,runHUD:.loading, success: { [weak self](json) in
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {
                        UILabel.showFalureHUD(text: "激活失败,请重试".local)
                    }else{
                        self?.激活按钮.isHidden = true
                        acccount.regId = " "
                        AccountManager.saveAccount(account: acccount)
                        UmengEvent.eventWithDic(name: "activate_readyForSure")
                        UILabel.showSucceedHUD(text: "激活中，待区块链确认".local)
                    }
                }
            }
            
        }) {() in
            UILabel.showFalureHUD(text: "激活失败,请重试".local)
        }
        
    }
    
}

