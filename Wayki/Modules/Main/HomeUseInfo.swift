//
//  HomeUseInfoManager.swift
//  Wayki
//
//  Created by lcl on 2018/6/26.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit


class HomeUserInfoManager {
    ///判断是否为新的版本
    class func is_new_version()->Bool {
        if let version = UserDefaults.standard.value(forKey: "homeinfo") as? String{
            if version != appVersion {
                return true
            }
            return false
        }else{
            UserDefaults.standard.set(appVersion, forKey: "homeinfo")
            return true
        }
        
    }
    ///是不是第一次投注
    class func is_new_bet()->Bool {
        if let version = UserDefaults.standard.value(forKey: "betinfo") as? String{
            if version != appVersion {
                return true
            }
            return false
        }else{
            UserDefaults.standard.set(appVersion, forKey: "betinfo")
            return true
        }
        
    }
    class func show(v:UIView,imgName:String){
        if imgName == "2"{
            if is_new_bet(){
                HomeUseInfo.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)).show(v: v, imgName: imgName)
                UserDefaults.standard.set(appVersion, forKey: "betinfo")
            }
            return
        }
        if is_new_version(){
            
            HomeUseInfo.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight)).show(v: v, imgName: imgName)
            UserDefaults.standard.set(appVersion, forKey: "homeinfo")
        }
        
    }
}

class HomeUseInfo: UIView {
    
    fileprivate let imgView = UIImageView.init()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupView(){
        imgView.frame = CGRect(x: 0, y: 0, width:ScreenWidth, height: ScreenHeight)
        imgView.alpha = 0
        self.backgroundColor = UIColor.clear
        
        
    }
    ///imgName:1是首页，2是下注成功后
    func show(v:UIView,imgName:String){
        
        if UIDevice.isX(){
            if imgName == "1"{
                imgView.image = UIImage.init(named: "homeinfoX1")
            }else{
                imgView.image = UIImage.init(named: "homeinfoX2")
            }
        }else{
            if imgName == "1"{
                imgView.image = UIImage.init(named: "homeinfo_1")
            }else{
                imgView.image = UIImage.init(named: "homeinfo_2")
            }
        }
        
        self.addSubview(imgView)
        UIView.animate(withDuration: 0.3) {
            self.imgView.alpha = 1
        }
        v.addSubview(self)
    }
    @objc func closeView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.imgView.alpha = 0
        }) { (ok) in
            self.removeFromSuperview()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        closeView()
    }
}
