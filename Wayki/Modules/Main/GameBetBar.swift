

import UIKit


class GameBetBar: FoundationV {
    
    var betAction:((Int)->Void)?
    private var buttons:[UIButton] = []
    private var titleLs:[UILabel] = []
    private var titles:[String] = []

    override func buildView(frame: CGRect) {
        
        for i in 0...2{
            let buttonW:CGFloat = frame.width/4
            let distance:CGFloat = (ScreenWidth-buttonW*3)/4
            let button = UIButton(frame: CGRect(x: distance+(buttonW+distance)*CGFloat(i), y: 0, width:buttonW, height: buttonW))
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitleColor(UIColor.white, for: .selected)
            button.titleLabel?.font = UIFont(name: kFontTypeThin, size: 12)
            button.backgroundColor = UIColor.clear
            button.tag = i
            button.addTarget(self, action: #selector(action(button:)), for: .touchUpInside)
            addSubview(button)
            buttons.append(button)
            
            let title = UILabel(frame: CGRect(x: button.x(), y: button.height(), width: buttonW, height: 20*scale))
            title.font = UIFont(name: "Helvetica", size: 12*scale)
            title.textAlignment = .center
            title.textColor = UIColor.white
            addSubview(title)
            titleLs.append(title)
        }
    }
}

extension UIButton{
    func redBig(){
        titleEdgeInsets = UIEdgeInsets(top: -30, left: 0, bottom: 0, right: 0)
        titleLabel?.font = UIFont(name: kFontType, size: 15)
        setBackgroundImage(UIImage(named: "game_betbutton_big_n"), for: .normal)
        setBackgroundImage(UIImage(named: "game_betbutton_big_h"), for: .highlighted)
    }
    
    func blueNormal(){
        titleEdgeInsets = UIEdgeInsets(top: -15, left: 0, bottom: 0, right: 0)
        titleLabel?.font = UIFont(name: kFontType, size: 12)
        setBackgroundImage(UIImage(named: "game_betbutton_s_n"), for: .normal)
        setBackgroundImage(UIImage(named: "game_betbutton_s_h"), for: .highlighted)
    }
}

// MARK:- 配置模型
extension GameBetBar{
    /////
    func setModels(models:[AppFootballTypeM],betGameType:Int){
        for button in buttons{ button.blueNormal() }
        if models[betGameType].投注项列表.count == 2{
            buttons[1].isHidden = true
            titleLs[1].isHidden = true
            buttons[0].setTitle(models[betGameType].投注项列表[0].投注项名称, for: .normal)
            buttons[2].setTitle(models[betGameType].投注项列表[1].投注项名称, for: .normal)
            
            let odd_0 = models[betGameType].投注项列表[0].投注项赔率
            let odd_1 = models[betGameType].投注项列表[1].投注项赔率
            titleLs[0].text = "赔率 ".local + "\(odd_0)"
            titleLs[2].text = "赔率 ".local + "\(odd_1)"
            if odd_0 > odd_1{
                buttons[0].redBig()
            }else if odd_0 < odd_1{
                buttons[2].redBig()
            }
            
        }else{
            buttons[1].isHidden = false
            titleLs[1].isHidden = false
            buttons[0].setTitle(models[betGameType].投注项列表[0].投注项名称, for: .normal)
            buttons[1].setTitle(models[betGameType].投注项列表[1].投注项名称, for: .normal)
            buttons[2].setTitle(models[betGameType].投注项列表[2].投注项名称, for: .normal)
            titleLs[0].text = "赔率 ".local + "\(models[betGameType].投注项列表[0].投注项赔率)"
            titleLs[1].text = "赔率 ".local + "\(models[betGameType].投注项列表[1].投注项赔率)"
            titleLs[2].text = "赔率 ".local + "\(models[betGameType].投注项列表[2].投注项赔率)"
            
            let odd_0 = models[betGameType].投注项列表[0].投注项赔率
            let odd_1 = models[betGameType].投注项列表[1].投注项赔率
            let odd_2 = models[betGameType].投注项列表[2].投注项赔率
            if odd_0 > odd_1 && odd_0 > odd_2 {
                buttons[0].redBig()
            }else if odd_1 > odd_0 && odd_1 > odd_2 {
                buttons[1].redBig()
            }else if odd_2 > odd_0 && odd_2 > odd_1 {
                buttons[2].redBig()
            }
        }
    }
    
    func setModels(models:[AppBasketballTypeM],betGameType:Int){
        for button in buttons{ button.blueNormal() }
        buttons[1].isHidden = true
        titleLs[1].isHidden = true
        buttons[0].setTitle(models[betGameType].投注项列表[0].投注项名称, for: .normal)
        buttons[2].setTitle(models[betGameType].投注项列表[1].投注项名称, for: .normal)
        
        let odd_0 = models[betGameType].投注项列表[0].投注项赔率
        let odd_1 = models[betGameType].投注项列表[1].投注项赔率
        titleLs[0].text = "赔率 ".local + "\(odd_0)"
        titleLs[2].text = "赔率 ".local + "\(odd_1)"
        if odd_0 > odd_1{
            buttons[0].redBig()
        }else if odd_0 < odd_1{
            buttons[2].redBig()
        }
    }
    
    func setModels(models:[AppEventTypeM],betGameType:Int){
        for button in buttons{ button.blueNormal()}
        buttons[1].isHidden = true
        titleLs[1].isHidden = true
        buttons[0].setTitle(models[betGameType].投注项列表[0].投注项名称, for: .normal)
        buttons[2].setTitle(models[betGameType].投注项列表[1].投注项名称, for: .normal)
        
        let odd_0 = models[betGameType].投注项列表[0].投注项赔率
        let odd_1 = models[betGameType].投注项列表[1].投注项赔率
        titleLs[0].text = "赔率 ".local + "\(odd_0)"
        titleLs[2].text = "赔率 ".local + "\(odd_1)"
        if odd_0 > odd_1{
            buttons[0].redBig()
        }else if odd_0 < odd_1{
            buttons[2].redBig()
        }
    }
}

// MARK:- 事件响应
extension GameBetBar{
    
    @objc private func action(button:UIButton){
        self.handleAction(tag: button.tag)
    }
    
    func handleAction(tag:Int){
        if buttons[1].isHidden {
            if betAction != nil {
                if tag == 0{ betAction!(0) }else{ betAction!(1)}
            }
        }else{
            if betAction != nil {
                betAction!(tag)
            }
        }
    }
    
    //放大缩小动画
    func btnBSAnimation(){
        for btn in buttons {
            btn.animationBigSmall()
        }
    }
}
