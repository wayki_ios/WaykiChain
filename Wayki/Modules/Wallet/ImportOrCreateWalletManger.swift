

import UIKit
//MARK: - import
class ImportOrCreateWalletManger: NSObject {
    //点击开始导入的逻辑判断
    class func startImportCheck(_ isAgreen:Bool = true, helpStr:String, password:String, makesurePWD:String, complete:@escaping ((_ isSuccess:Bool,_ alertString:String,_ address:String,_ helpStr:String,_ password:String) -> Void)) -> Void {
        var alertMessage:String

        if isAgreen == false{
            alertMessage = NSLocalizedString("请阅读并同意 服务及隐私条款", comment: "")
            complete(false,alertMessage,"","","")
            return
        }
        
        if helpStr.count>0 {
            if (makesurePWD.count>7) || (password.count>7){
                if makesurePWD == password{
                    let helpArr = Bridge.getWalletHelpCodes(from: helpStr)
                    let helpString = Bridge.getWaletHelpString(withCodes: helpArr) as String
                    if Bridge.checkMnemonicCode(Bridge.getWalletHelpCodes(from: helpString)) == true {
                        let arr = Bridge.getAddressAndPrivateKey(withHelp: helpString, password: password)
                        let address = arr![0] as! String
                        //let privateKey = arr![1]
                        if Bridge.addressIsAble(address ){
                            complete(true,"",address,helpString,password)
                            return
                        }else{
                            alertMessage = NSLocalizedString("请输入正确的助记词", comment: "")
                            complete(false,alertMessage,"","","")
                            return
                        }
                    }else{
                        alertMessage = NSLocalizedString("请输入正确的助记词", comment: "")
                        complete(false,alertMessage,"","","")
                        return
                    }
                }else{
                    alertMessage = NSLocalizedString("两次密码输入不一致", comment: "")
                    complete(false,alertMessage,"","","")
                    return
                }
                
            }else{
                alertMessage = NSLocalizedString("请输入8~16位密码", comment: "")
                complete(false,alertMessage,"","","")
                return
            }
        }else{
            alertMessage = NSLocalizedString("请输入助记词", comment: "")
            complete(false,alertMessage,"","","")
            return
        }
    }
    
    //cjlabel 同意服务及点击条款设计
    class func addAttStylesWithLabel(label:CJLabel) ->(secondStr:String,attStyles:[AnyHashable:Any]){
        label.isUserInteractionEnabled = true
        label.isUserInteractionEnabled = true
        label.font = UIFont(name: kFontType, size: 12)

        let firstStr = NSLocalizedString("我已认真阅读并同意", comment: "")
        let secondStr = NSLocalizedString(" 服务及隐私条款", comment: "")
        let pStr = firstStr.appendingFormat(secondStr)
        let muAttstr = NSMutableAttributedString(string: pStr)
        muAttstr.addAttribute(.foregroundColor, value: UIColor.RGB(r: 150, g: 150, b: 150), range: NSRange(location: 0, length: firstStr.count))
        muAttstr.addAttribute(.font, value: UIFont.systemFont(ofSize: 12), range: NSRange(location: 0, length: firstStr.count))
        muAttstr.addAttribute(.font, value: UIFont.systemFont(ofSize: 12) as Any, range: NSRange(location: firstStr.count, length: pStr.count - firstStr.count))

        let fontColor = UIColor.RGB(r: 220, g: 220, b: 220) as Any
        muAttstr.addAttribute(.foregroundColor, value: fontColor, range: NSRange(location: firstStr.count, length: pStr.count - firstStr.count))
        label.attributedText = muAttstr
        let attStyles = [AnyHashable(NSAttributedStringKey.foregroundColor): fontColor]
        
        let truple:(secondStr:String,attStyles:[AnyHashable:Any]) = (secondStr:secondStr,attStyles:attStyles)

        return truple
    }
    

    
}

//MARK: - create
extension ImportOrCreateWalletManger {
    //点击开始创建钱包的判断逻辑
    class func startCreateCheck(_ isAgreen:Bool = true, password:String, makesurePWD:String, complete:@escaping ((_ isSuccess:Bool,_ alertString:String,_ address:String,_ helpStr:String,_ password:String) -> Void)) -> Void {
        var alertMessage:String
        
        if isAgreen == false{
            alertMessage = NSLocalizedString("请阅读并同意 服务及隐私条款", comment: "")
            complete(false,alertMessage,"","","")
            return
        }
        
        if (makesurePWD.count>7&&makesurePWD.count<17) || (password.count>7&&password.count<17){
            if makesurePWD == password{
                let helpcodes = Bridge.getWalletHelpCodes()
                let helpstring = Bridge.getWaletHelpString(withCodes: helpcodes)
                let addressAndPrivateKeyArr = Bridge.getAddressAndPrivateKey(withHelp: helpstring, password: password) as! [String]
                complete(true,"",addressAndPrivateKeyArr[0],helpstring!,password)
                return
            }else{
                alertMessage = NSLocalizedString("两次密码输入不一致", comment: "")
                complete(false,alertMessage,"","","")
                return
            }
        }else{
            alertMessage = NSLocalizedString("请输入8~16位密码", comment: "")
            complete(false,alertMessage,"","","")
            return
        }
    }
    
    //生成提示Label
    class func addRemarkLabel(superview:UIView){
        let xSpace = ScreenWidth*24.0/375.0
        let lWidth = ScreenWidth - 2*xSpace
        var firstTop = ScreenWidth*7.0/375.0 + naviHeight
        if ScreenWidth == 320 {
            firstTop = ScreenWidth*1.0/375.0 + naviHeight
        }
        var firstTopLabel:UILabel?
        var secondTopLabel:UILabel?
        
        let bcView = UIView()
        bcView.frame = CGRect(x: xSpace, y: firstTop, width: lWidth, height: ScreenWidth*100.0/375.0)
        bcView.backgroundColor = UIColor.clear
        superview.addSubview(bcView)
        

        
        firstTopLabel = UILabel(frame: CGRect(x: 0, y: 0, width: lWidth, height: ScreenWidth*18.0/375.0))
        firstTopLabel?.text = NSLocalizedString("重要提示", comment: "")
        firstTopLabel?.textColor = UIColor.white
        firstTopLabel?.backgroundColor = UIColor.clear
        firstTopLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        firstTopLabel?.textAlignment = .left
        bcView.addSubview(firstTopLabel!)
        
        let pStr1 = "●  " + "密码用于保护私钥和交易授权，强度非常重要".local
        let pStr2 = "●  " + "WaykiChain不存储用户密码，无法提供找回或者重置密码功能，请务必牢记".local
        let showStr = pStr1 + "\n" + pStr2
        let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = ScreenWidth*4.0/375.0
        secondTopLabel = UILabel(frame: CGRect(x:0, y: (firstTopLabel?.bottom())! + ScreenWidth*8.0/375.0 , width: lWidth, height: ScreenWidth*75.0/375.0))
        secondTopLabel?.font = UIFont.systemFont(ofSize: 11)
        let secAttStr:NSMutableAttributedString = NSMutableAttributedString(string: showStr)
        secAttStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, showStr.count))
        secAttStr.addAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 9)], range: NSMakeRange(0, 1))
        secAttStr.addAttributes([NSAttributedStringKey.font:UIFont.systemFont(ofSize: 9)], range: NSMakeRange(pStr1.count+1, 1))
        var attSize = secAttStr.boundingRect(with: CGSize(width: lWidth, height: 1000), options: NSStringDrawingOptions(rawValue: NSStringDrawingOptions.RawValue(UInt8(NSStringDrawingOptions.usesFontLeading.rawValue)|UInt8(NSStringDrawingOptions.usesLineFragmentOrigin.rawValue))), context: nil)
        if attSize.height >  ScreenWidth*80.0/375.0{
            attSize.size.height = ScreenWidth*80.0/375
            firstTopLabel?.frame.origin.y = (firstTopLabel?.frame.origin.y)! - 10
            secondTopLabel?.frame.origin.y =  (secondTopLabel?.frame.origin.y)! - 20
        }
        
        secondTopLabel?.frame.size.height = attSize.size.height
        secondTopLabel?.attributedText = secAttStr
        secondTopLabel?.textColor = UIColor.white
        secondTopLabel?.numberOfLines = 0
        secondTopLabel?.backgroundColor = UIColor.clear
        secondTopLabel?.alpha = 0.8
        secondTopLabel?.textAlignment = .left
        bcView.addSubview(secondTopLabel!)
    }
}
