

import UIKit

class MyWallectC: NavBaseVC {
    private let buttonH:CGFloat = ScreenHeight*(80/755)
    
    fileprivate var contentV : InputPwdView? = nil
    
    fileprivate var index:Int = -1
    ///显示那把锁
    fileprivate var img:UIImageView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        配置界面()
        配置按钮()
    }


}

extension MyWallectC{
    func 配置界面(){
        titleLabel?.text = "钱包管理".local
        showImageView?.isHidden = true
        view.clipsToBounds = true
    }
    
    func 配置按钮(){
        let titles:[String] = ["备份钱包".local,"导入钱包".local,"创建钱包".local,"导出私钥".local,"修改密码".local,"获取Wicc".local]
        let images:[String] = ["wallet_restore","wallet_import","wallet_create","wallet_privatekey","wallet_password","wallet_wicc"]
        
        for i in 0..<titles.count{
            创建按钮(y:ScreenHeight*(100/755)+buttonH*CGFloat(i),title: titles[i], image: images[i], tag: i)
        }
    }
    
    func 创建按钮(y:CGFloat,title:String,image:String,tag:Int){
        
        let btnH:CGFloat = 44
        
        let button = UIButton(frame: CGRect(x: 0, y: y, width: ScreenWidth, height: buttonH))
        button.tag = tag
        button.backgroundColor = UIColor.clear
        button.addTarget(self, action: #selector(界面事件(btn:)), for: .touchDown)
        view.addSubview(button)
        
        let imageV = UIImageView(frame: CGRect(x: ScreenWidth*(17/427), y: (buttonH-btnH)/2, width: btnH, height:btnH))
        imageV.image = UIImage(named: image)
        button.addSubview(imageV)
        
        let contentL = UILabel(frame: CGRect(x: buttonH*(5/8)+ScreenWidth*(17/427)+20, y: 0, width: 200, height: buttonH))
        contentL.text = title
        contentL.textColor = UIColor.white
        contentL.alpha = 0.8
        contentL.font = UIFont(name: kFontType, size: 16)
        button.addSubview(contentL)
        
        let back = UIImageView(frame: CGRect(x: ScreenWidth-30, y: (buttonH-16)/2, width: 8, height: 16 ))
        back.image = UIImage(named: "wallet_back")
        back.alpha = 0.6
        button.addSubview(back)
        
        let line = CGView(frame: CGRect(x:0, y: buttonH-0.5, width: ScreenWidth, height: 0.5))
        line.backgroundColor = UIColor.white
        line.alpha = 0.4
        button.addSubview(line)
        
    }
    
}

// MARK:- 界面跳转事件

extension MyWallectC{
    @objc func 界面事件(btn:UIButton){
        if btn.tag == 0{
            index = 0
            alertView(title: "备份钱包".local)
            
        }else if btn.tag == 1{
            index = 1
            alertView(title: "导入钱包".local)
            
        }else if btn.tag == 2{
            index = 2
            alertView(title: "创建钱包".local)
            
        }else if btn.tag == 3{
            index = 3
            alertView(title: "导出私钥".local)
        }else if btn.tag == 4{
            index = 4
            let c = WalletPasswordVC()
            navigationController?.pushViewController(c, animated: true)
        }
    }
}

extension MyWallectC{
    
    func alertView(title:String){
        
        let widthScale = ScreenWidth/375
        let x = 24 * widthScale
        
        let contentWidth = widthScale * 327
        let contentheight = contentWidth * 0.73
        
        contentV = InputPwdView.init(frame: CGRect(x: x , y: ScreenHeight/2-contentheight/2, width: contentWidth , height: contentheight), title: title)
        contentV?.backgroundColor = UIColor.white
        contentV?.layer.cornerRadius = 10
        contentV?.alertDelegate = self
        self.contentV?.transform = CGAffineTransform.init(scaleX: 0, y: 0)
        
        img = UIImageView.init(frame: CGRect(x:self.view.bounds.width/2-50, y:ScreenHeight/2-contentheight/2-45, width: 100, height: 100))
        img?.image = UIImage.init(named: "alert_lock")
       
        AlertManager.shared.showAlert(contenView: self.contentV!, onView: UIApplication.shared.keyWindow!,headImg: img!)
        contentV?.scrollRectToVisible(CGRect(x: 0, y: 0, width: contentV!.width(), height: contentV!.height()), animated: true)
        
    }
    

}


extension MyWallectC:InputPwdViewDelegate{
    func closeView() {
        img?.removeFromSuperview()
        self.contentV?.removeFromSuperview()
        self.contentV?.removeAllSubviews()
    }
    
    func sureBtn(obj: InputPwdView, pwdTf: UITextField) {
        if index == 0{
            obj.exitAlert()
            
            let c = WalletBackupVC()
            c.password = pwdTf.text!
            navigationController?.pushViewController(c, animated: true)
        }else if index == 1{
            obj.exitAlert()
            UmengEvent.eventWithDic(name: "import_wallet_side")
            let c = WalletImportVC()
            navigationController?.pushViewController(c, animated: true)
        }else if index == 2{
            obj.exitAlert()
            UmengEvent.eventWithDic(name: "create_wallet_side")
            let c = WalletCreateC()
            navigationController?.pushViewController(c, animated: true)
            
        }else if index == 3{
            pwdTf.resignFirstResponder()
            img!.removeFromSuperview()
            obj.scrollRectToVisible(CGRect(x: 0, y: obj.height(), width: obj.width(), height: obj.height()), animated: true)
        }
    }
    
    func keyboradShow() {
        
        UIView.animate(withDuration: 0.3) {
            if ScreenHeight == 812{
                self.contentV?.frame.origin.y = ScreenHeight-330-self.contentV!.height()
                self.img!.frame.origin.y = ScreenHeight-330-self.contentV!.height()-45
            }else{
                self.contentV?.frame.origin.y = ScreenHeight-271-self.contentV!.height()
                self.img!.frame.origin.y = ScreenHeight-271-self.contentV!.height()-45
            }
            
        }

    }
    
    func keyboradHide() {
        UIView.animate(withDuration: 0.3) {
            self.contentV?.frame.origin.y = ScreenHeight/2-self.contentV!.height()/2
            self.img!.frame.origin.y = ScreenHeight/2-self.contentV!.height()/2-45
        }

    }
    
    
}
