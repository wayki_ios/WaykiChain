

import UIKit

class WalletBackupVC: NavBaseVC {
    var isFromCreate:Bool = false
    var password:String = ""
    var firstTopLabel:UILabel?
    var secondTopLabel:UILabel?
    
    var mnBackView:UIView?
    var showMnemonicsLabel:UILabel?
    
    var remarkLabel1:UILabel?
    var remarkLabel2:UILabel?
    
    var startBtn:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: - UI
extension WalletBackupVC{
    func layoutUI(){
        titleLabel?.text = NSLocalizedString("备份助记词", comment: "")
        addUpperBackShowImageView(imageName: "wallet_header_bgd_short")
        addTopSHowView()
        addShowLabel()
        addRemarkView()
        addStartBtn()
        
        showMnemonicsLabel?.text = AccountManager.getAccount().getHelpString(password: password)
        print(showMnemonicsLabel?.text)
        showMnemonicsLabel?.sizeToFit()
        remarkLabel2?.sizeToFit()

    }
    
    func addTopSHowView(){
        let xSpace = ScreenWidth*24.0/375.0
        let lWidth = ScreenWidth - 2*xSpace
        let firstTop = ScreenWidth*80.0/375.0
        
        firstTopLabel = UILabel(frame: CGRect(x: xSpace, y: firstTop, width: lWidth, height: ScreenWidth*18.0/375.0))
        firstTopLabel?.text = NSLocalizedString("请抄下助记词", comment: "")
        firstTopLabel?.textColor = UIColor.white
        firstTopLabel?.backgroundColor = UIColor.clear
        firstTopLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        firstTopLabel?.textAlignment = .left
        self.view.addSubview(firstTopLabel!)
        
        
        secondTopLabel = UILabel(frame: CGRect(x: xSpace, y: ScreenWidth*108.0/375.0 , width: lWidth, height: ScreenWidth*36.0/375.0))
        secondTopLabel?.text = NSLocalizedString("助记词用于恢复钱包或重置钱包密码，请将它准确的抄写到纸上，并存放在只有您自己知道的安全地方。", comment: "")
        secondTopLabel?.textColor = UIColor.white
        secondTopLabel?.numberOfLines = 0
        secondTopLabel?.backgroundColor = UIColor.clear
        secondTopLabel?.alpha = 0.8
        secondTopLabel?.font = UIFont.systemFont(ofSize: 13)
        secondTopLabel?.textAlignment = .left
        self.view.addSubview(secondTopLabel!)
    }
    
    
    func addShowLabel(){
        let xSpace = ScreenWidth*24.0/375.0
        let mnWidth = ScreenWidth - 2*xSpace
        let mnHeight = mnWidth*110.0/327.0
        mnBackView = UIView(frame: CGRect(x: xSpace, y: ScreenWidth*164.0/375.0, width: mnWidth, height: mnHeight))
        mnBackView?.backgroundColor = UIColor.white
        mnBackView?.layer.cornerRadius = 6
        self.view.addSubview(mnBackView!)
        
        let spaceToLeft = ScreenWidth*16.0/375.0
        showMnemonicsLabel = UILabel(frame: CGRect(x: spaceToLeft, y: spaceToLeft, width: mnWidth - 2*spaceToLeft, height: mnHeight - 2*spaceToLeft))
        showMnemonicsLabel?.textAlignment = .left
        showMnemonicsLabel?.numberOfLines = 0
        showMnemonicsLabel?.font = UIFont.systemFont(ofSize: 16)
        mnBackView?.addSubview(showMnemonicsLabel!)
    }
    
    func addRemarkView(){
        let top = ScreenWidth*20.0/375.0
        let xSpace = ScreenWidth*24.0/375.0
        let rWidth = ScreenWidth - 2*xSpace
        remarkLabel1 = UILabel(frame: CGRect(x: xSpace, y: (mnBackView?.bottom())!+top, width: rWidth, height: ScreenWidth*17.0/375.0))
        remarkLabel1?.textAlignment = .left
        remarkLabel1?.text = "特别提醒：".local
        remarkLabel1?.textColor = UIColor.white
        remarkLabel1?.font = UIFont.systemFont(ofSize: 12)
        remarkLabel1?.backgroundColor = UIColor.clear
        self.view.addSubview(remarkLabel1!)
        
        remarkLabel2 = UILabel(frame: CGRect(x: xSpace, y: (remarkLabel1?.bottom())! + ScreenWidth*8.0/375.0, width: rWidth, height: ScreenWidth*32.0/375.0))
        remarkLabel2?.textAlignment = .left
        remarkLabel2?.text = "助记词非常重要！不要丢失或泄漏，如果有人获取您的助记词将直接获取您的资产！".local
        remarkLabel2?.numberOfLines = 0
        remarkLabel2?.alpha = 0.5
        remarkLabel2?.backgroundColor = UIColor.clear
        remarkLabel2?.textColor = UIColor.white
        remarkLabel2?.font = UIFont.systemFont(ofSize: 11)
        self.view.addSubview(remarkLabel2!)

    }
    
    func addStartBtn(){
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = btnWidth*44.0/327.0
        let btnTop = (remarkLabel2?.bottom())! + ScreenWidth*30.0/375.0
        startBtn = UIButton(type: .custom)
        startBtn?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: btnTop, width: btnWidth, height: btnHeight)
        startBtn?.setTitle(NSLocalizedString("开始备份", comment: ""), for: .normal)
        startBtn?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        startBtn?.addTarget(self, action: #selector(startBackup), for: .touchUpInside)
        
        self.view.addSubview(startBtn!)
    }
}

extension WalletBackupVC {
    @objc func startBackup(){
        let c =  WalletMarkSureBackupVC()
        c.helpStr = showMnemonicsLabel?.text
        c.isFromCreate = isFromCreate
        self.navigationController?.pushViewController(c, animated: true)
    }
}
