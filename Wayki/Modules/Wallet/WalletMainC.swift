

import UIKit

class WalletMainC: FoundationC {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        buildV()
    }
}

// MARK:- 创建界面
extension WalletMainC{
    func buildV() {
        navigationController?.navigationBar.isHidden = true
        var bgdImage:UIImage = UIImage(named: "walletMain_6")!
        if UIDevice.isX() {
            bgdImage = UIImage(named: "walletMain_x")!
        }else if UIDevice.model() == "5"{
            bgdImage = UIImage(named: "walletMain_5")!
        }else if UIDevice.model() == "6"{
            bgdImage = UIImage(named: "walletMain_6")!
        }else if UIDevice.model() == "p"{
            bgdImage = UIImage(named: "walletMain_p")!
        }
        
        
        let bgdV = UIImageView(frame: CGRect(x: -1, y: -1, width: ScreenWidth+2, height: ScreenHeight+2))
        bgdV.image = bgdImage
        view.addSubview(bgdV)
        
        let x:CGFloat = 24
        let y:CGFloat = (478/667)*ScreenHeight
        let w:CGFloat = (327/375)*ScreenWidth
        let h:CGFloat = (44/667)*ScreenHeight
        let createBtn = UIButton(frame: CGRect(x: x, y: y, width: w, height: h))
        createBtn.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        createBtn.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .highlighted)
        createBtn.setTitle("创建钱包".local, for: .normal)
        createBtn.tag = 1
        createBtn.setTitleColor(UIColor.white, for: .normal)
        createBtn.addTarget(self, action: #selector(viewAction(button:)), for: .touchUpInside)
        view.addSubview(createBtn)
        
        let detaY:CGFloat = (60/667)*ScreenHeight
        let installBtn = UIButton(frame: CGRect(x: x, y: y+detaY, width: w, height: h))
        installBtn.tag = 2
        installBtn.layer.cornerRadius = 5
        installBtn.layer.borderColor = UIColor.white.cgColor
        installBtn.layer.borderWidth = 0.5
        installBtn.setTitle("导入钱包".local, for: .normal)
        installBtn.setTitleColor(UIColor.white, for: .normal)
        installBtn.addTarget(self, action: #selector(viewAction(button:)), for: .touchUpInside)
        view.addSubview(installBtn)
    }
    
    
}

// MARK:- 事件处理
extension WalletMainC{
    @objc func viewAction(button:UIButton){
        if button.tag == 1 { // 创建钱包
            UmengEvent.eventWithDic(name: "create_wallet_init")
            let c = WalletCreateC()
            c.fromTheInitial = true
            self.navigationController?.pushViewController(c, animated: true)
        }else if button.tag == 2{ // 导入钱包
            UmengEvent.eventWithDic(name: "import_wallet_init")
            let c = WalletImportVC()
            c.fromTheInitial = true
            navigationController?.pushViewController(c, animated: true)
        }
    }
}

