

import UIKit

class WalletCreateSuccessVC: NavBaseVC {
    var password:String = ""
    
    var successImageView:UIImageView?
    var proLabel:UILabel?
    var startBtn:UIButton?
    
    var rmBackView:UIView?
    var remarkLabel:UILabel?
    var fromTheInitial:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

       layoutUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension WalletCreateSuccessVC{
    func layoutUI(){
        titleLabel?.text = "创建完成".local
        leftItem?.isHidden = true
        addShowImageAndProLabel()
        addRemarkLabel()
        addStartUseBtn()
    }
    
    func addShowImageAndProLabel(){
        
        let imageWidth = ScreenWidth*52.0/375.0
        let imageTop = naviHeight + ScreenWidth*36.0/375.0
        successImageView = UIImageView(image: UIImage(named: "wallet_succeed"))
        successImageView?.frame = CGRect(x: (ScreenWidth - imageWidth)/2.0, y: imageTop, width: imageWidth, height: imageWidth)
        self.view.addSubview(successImageView!)
        
        proLabel = UILabel(frame: CGRect(x: 0, y: naviHeight + ScreenWidth*100.0/375.0, width: ScreenWidth, height: 20))
        proLabel?.text = "恭喜您，新钱包创建成功".local
        proLabel?.textAlignment = .center
        proLabel?.textColor = UIColor.white
        proLabel?.font = UIFont(name: kFontType, size: 14)
        self.view.addSubview(proLabel!)
    }
    
    
    func addRemarkLabel(){
        let vWidth = ScreenWidth*327.0/375.0
        let vHeight = ScreenWidth*110.0/375.0
        let vTop = (showImageView?.bottom())! - ScreenWidth*24.0/375.0
        rmBackView = UIView(frame: CGRect(x: (ScreenWidth - vWidth)/2.0, y: vTop, width: vWidth, height: vHeight))
        rmBackView?.layer.cornerRadius = 6
        rmBackView?.clipsToBounds = true
        rmBackView?.backgroundColor = UIColor.white
        self.view.addSubview(rmBackView!)
        
        let xSpace = ScreenWidth*14.0/375.0
        let ySpace = ScreenWidth*2.0/375.0
        remarkLabel = UILabel(frame: CGRect(x: xSpace, y: ySpace, width: vWidth-2*xSpace, height: vHeight-2*ySpace))
        remarkLabel?.textAlignment = .left
        remarkLabel?.numberOfLines = 0
        remarkLabel?.font = UIFont.systemFont(ofSize: 13)
        rmBackView?.addSubview(remarkLabel!)
        
        let firstStr = "重要提示：".local
        let secondStr = "\n\n" + "拥有钱包助记词就能完全控制钱包资产。因此强烈建议您在使用钱包前请做好助记词备份，并将助记词保存在安全的地方。".local
        let pStr = firstStr + secondStr
        let muAttStr = NSMutableAttributedString(string: pStr)
        muAttStr.addAttributes([NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 13)], range: NSMakeRange(0, firstStr.count))
        remarkLabel?.attributedText = muAttStr
        

    }
    
    func addStartUseBtn(){
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = btnWidth*44.0/327.0
        let btnTop =  ScreenWidth*30.0/375.0  +  (rmBackView?.bottom())!
        startBtn = UIButton(type: .custom)
        startBtn?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: btnTop, width: btnWidth, height: btnHeight)
        startBtn?.setTitle("备份助记词".local, for: .normal)
        startBtn?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        startBtn?.addTarget(self, action: #selector(startBackup), for: .touchUpInside)
        
        self.view.addSubview(startBtn!)
    }
}


extension WalletCreateSuccessVC{
    @objc func startBackup(){
        let c = WalletBackupVC()
        c.isFromCreate = true
        c.password = password
        navigationController?.pushViewController(c, animated: true)
    }
}
