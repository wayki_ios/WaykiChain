//
//  NoDataView.swift
//  WaykiChain
//
//  Created by lcl on 2018/6/5.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class NoDataView: UIView {

    static let shared = NoDataView.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight))
    
    let activity = UIActivityIndicatorView.init()
    let msgLabel = UILabel.init()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView(f: frame)
        self.isUserInteractionEnabled = false
    }
    
    fileprivate func setupView(f:CGRect){
        self.backgroundColor = UIColor.clear
        activity.frame = CGRect(x:0, y: 0, width: 0, height: 0)
        activity.center = self.center
        activity.color = UIColor.white
        activity.hidesWhenStopped = true
//        activity.startAnimating()
        self.addSubview(activity)
        
        msgLabel.frame = CGRect(x: 0, y: f.height/2, width: f.width, height: 30)
        msgLabel.textColor = UIColor.init(white: 1, alpha: 0.5)
        msgLabel.font = UIFont.systemFont(ofSize: 15)
        
        msgLabel.textAlignment = .center
        msgLabel.isHidden = true
        self.addSubview(msgLabel)
        
    }
   
    func stop(haveDate:Bool = false,msg:String = "什么记录也没有".local){
        activity.isHidden = true
        if haveDate{
            msgLabel.isHidden = true
            self.removeFromSuperview()
        }else{
            msgLabel.isHidden = false
            msgLabel.text = msg
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
