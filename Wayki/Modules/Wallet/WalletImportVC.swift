

import UIKit

class WalletImportVC: NavBaseVC {
    var textBackView:UIView?
    var textView:UITextView?
    var placeholderLabel:UILabel?
    
    var pwdBackView:UIView?
    var passwordTF:InputPasswordTextFiled?
    var makesureTF:InputPasswordTextFiled?
    
    var agreenBtn:UIButton?
    var protocolLabel:CJLabel?
    
    var startImportButton:UIButton?
    
    var fromTheInitial:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutView()
        NotificationCenter.default.addObserver(self, selector: #selector(listenInBtnStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }


}

//MARK:- UI
extension WalletImportVC{
    func layoutView(){
        titleLabel?.text = NSLocalizedString("导入钱包", comment: "")
        createTextView()
        createPasswordView()
        createAgreenBtnAndProtocol()
        createStartBtn()
    }
    
    func createTextView(){
     
        let textWidth = ScreenWidth*327.0/375.0
        let tfSpace = ScreenWidth*20.0/375.0
        let bView = UIView(frame: CGRect(x: (ScreenWidth - textWidth)/2.0, y: (header?.bottom())! + ScreenWidth*21.0/375.0, width: textWidth, height: textWidth*110.0/327.0))
        bView.backgroundColor = UIColor.white
        bView.alpha = 0.1
        bView.layer.cornerRadius = 6
        textBackView = bView
        self.view.addSubview(bView)
        
        let tWidth = textWidth - 2*tfSpace
        let tHeight = textWidth*110.0/327.0 - 2*tfSpace
        textView = UITextView(frame: CGRect(x: (ScreenWidth - tWidth)/2.0, y: (header?.bottom())! + ScreenWidth*21.0/375.0 + tfSpace, width: tWidth, height: tHeight))
        textView?.layer.cornerRadius = 6
        textView?.layer.masksToBounds = true
        textView?.backgroundColor = UIColor.clear
        textView?.font = UIFont(name: kFontType, size: 14)
        textView?.textColor = UIColor.black
        textView?.delegate = self
        self.view.addSubview(textView!)
        
        placeholderLabel = UILabel(frame: CGRect(x: 0, y:0, width: tWidth, height:20))
        placeholderLabel?.textColor = UIColor.white
        placeholderLabel?.font = UIFont(name: kFontType, size: 14)
        placeholderLabel?.text = NSLocalizedString("助记词 按空格分隔", comment: "")
        textView?.addSubview(placeholderLabel!)
    }
    
    func createPasswordView(){
        let bvWidth = ScreenWidth*327.0/375.0
        let bvHeight = bvWidth*135.0/327.0
        let backView = UIView(frame: CGRect(x: (ScreenWidth - bvWidth)/2.0, y: (textBackView?.bottom())! + ScreenWidth*24.0/375.0, width: bvWidth, height: bvHeight))
        backView.backgroundColor = UIColor.white
        backView.layer.cornerRadius = 6
        pwdBackView = backView
        self.view.addSubview(backView)
        
        let tfSpace = ScreenWidth*20.0/375.0
        let tftopSpace = ScreenWidth*24.0/375.0
        let tfWidth = bvWidth - tfSpace*2
        let tfHieght = (bvHeight - 2*tftopSpace - 1)/2.0
        passwordTF = InputPasswordTextFiled(frame: CGRect(x: tfSpace, y: tftopSpace, width: tfWidth, height: tfHieght))
        passwordTF?.font = UIFont(name: kFontType, size: 18)
        passwordTF?.isSecureTextEntry = true
        passwordTF?.delegate = self
        passwordTF?.placeholder = NSLocalizedString("设置钱包密码（8～16位）", comment: "")
        backView.addSubview(passwordTF!)
        
        let lineView = UIView(frame: CGRect(x: tfSpace, y: (passwordTF?.bottom())!, width: tfWidth, height: 0.5))
        lineView.backgroundColor = UIColor.gray
        backView.addSubview(lineView)
        
        makesureTF = InputPasswordTextFiled(frame: CGRect(x: tfSpace, y: lineView.bottom(), width: tfWidth, height: tfHieght))
        makesureTF?.font = UIFont(name: kFontType, size: 18)
        makesureTF?.isSecureTextEntry = true
        makesureTF?.delegate = self
        makesureTF?.placeholder = NSLocalizedString("重复密码", comment: "")
        backView.addSubview(makesureTF!)
    }
    
    func createAgreenBtnAndProtocol(){
        let space = ScreenWidth*24.0/375.0
        let topSpace = (pwdBackView?.bottom())! + ScreenWidth*20.0/375.0
        agreenBtn = UIButton(type: .custom)
        agreenBtn?.frame = CGRect(x: space, y: topSpace, width: ScreenWidth*20.0/375.0, height: ScreenWidth*20.0/375.0)
        agreenBtn?.setImage(UIImage(named: "wallet_item_normal"), for: .normal)
        agreenBtn?.setImage(UIImage(named: "wallet_item_select"), for: .selected)
        agreenBtn?.addTarget(self, action: #selector(agreedProtocol(_:)), for: .touchUpInside)
        self.view.addSubview(agreenBtn!)
        
        let protocolWidth = ScreenWidth - (agreenBtn?.right())! - ScreenWidth*8.0/375.0
        protocolLabel = CJLabel(frame: CGRect(x: (agreenBtn?.right())! + ScreenWidth*8.0/375.0, y: topSpace, width: protocolWidth, height: 20))
        let truple = ImportOrCreateWalletManger.addAttStylesWithLabel(label: protocolLabel!)
        protocolLabel?.addLinkString(truple.secondStr, linkAddAttribute: truple.attStyles, block: { [weak self](linkModel) in
            self?.onLookUserProtol()
        })
        self.view.addSubview(protocolLabel!)
    }
    
    func createStartBtn(){
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = btnWidth*44.0/327.0
        startImportButton = UIButton(type: .custom)
        startImportButton?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: (protocolLabel?.bottom())! + ScreenWidth*30.0/375.0, width: btnWidth, height: btnHeight)
        startImportButton?.setTitle(NSLocalizedString("开始导入", comment: ""), for: .normal)
        startImportButton?.setTitleColor(UIColor.white, for: .normal)
        startImportButton?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        startImportButton?.setBackgroundImage(UIImage(named: "wallet_button_unable"), for: .disabled)
        startImportButton?.isEnabled = false
        startImportButton?.layer.cornerRadius = 6
        startImportButton?.addTarget(self, action: #selector(startImport), for: .touchUpInside)
        self.view.addSubview(startImportButton! )
    }
    
}

//MARK: - UITextViewDelegate
extension WalletImportVC:UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
//        if textView.text.count == 0 {
//            //placeholderLabel?.isHidden = false
//        }else{
//            placeholderLabel?.isHidden = true
//        }
        listenInBtnStatus()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        if textView.text.count>0 {
            
        }else{
            placeholderLabel?.isHidden = false
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let toBeString = textView.text
        if (toBeString?.count)!>200 {
            textView.text = toBeString?.subString(to: 200)
            return false
        }else{
            return true
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        placeholderLabel?.isHidden = true
    }
    
}

//MARK: - UITextFieldDelegate
extension WalletImportVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }else if (textField.text?.count)!>=16{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
}

//MARK: - event
extension WalletImportVC {
    @objc func agreedProtocol(_ btn:UIButton){
        btn.isSelected = !btn.isSelected
        listenInBtnStatus()
    }
    
    @objc func onLookUserProtol(){
        //print("onLookUserProtol")
        let c = ShowProtocolVC()
        self.navigationController?.pushViewController(c, animated: true)
    }
    //判断按钮是否可点击
    @objc func listenInBtnStatus(){
        let isAgreen = agreenBtn?.isSelected
        var isEnable = false
        if isAgreen==true {
            if (textView?.text.count)!>10{
                if ((passwordTF?.text?.count)!>0) && ((makesureTF?.text?.count)!>0){
                    isEnable = true
                }else{
                    isEnable = false
                }
            }else{
                isEnable = false
            }
        }else{
            isEnable = false
        }
        
        startImportButton?.isEnabled = isEnable

    }
    
    //开始导入
    @objc func startImport(){
        if agreenBtn?.isSelected == false {
            return
        }
        let helpstr = textView?.text
        let password = passwordTF?.text
        let makesurePWD = makesureTF?.text
        ImportOrCreateWalletManger.startImportCheck((agreenBtn?.isSelected)!, helpStr: helpstr!, password: password!, makesurePWD: makesurePWD!) { (isSuccess, alertStr, address, helpStr,password) in
            if isSuccess==true{
                self.importWallet(helpStr: helpStr, address: address, password: password)
            }else{
                self.alertShow(str: alertStr)
            }
        }

    }
    
    func alertShow(str:String){
        self.addTextHUD(text: str)
    }
    
    //导入钱包，助记词存储加密,密码生成加密key
    func importWallet(helpStr:String,address:String,password:String){
        
        UmengEvent.eventWithDic(name: "import_success")
        let account = NewAccount()
        account.address = address
        account.mHash = Bridge.getWalletHash(from: helpStr)
        account.setEncyptHelpStringAndPassword(helpStr: helpStr, password: password)
        AccountManager.saveAccount(account: account)
        
        let c = WalletImportSuccessVC()
        self.navigationController?.pushViewController(c, animated: true)
        var vcArr = self.navigationController?.viewControllers
        vcArr = [c]
        self.navigationController?.setViewControllers(vcArr!, animated: false)
        return
    }
    
}
