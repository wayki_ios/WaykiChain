
import UIKit

class WalletRecordDetaiInfolVC: NavBaseVC {
    var listModel:MyAssertCellModel?
    var detailModel:WalletRecordDetailModel?
    
    var backSView:UIView?
    var statusImageVIew:UIImageView?
    var mainScrollView:UIScrollView?
    
    var topBackView:UIView?
    var statusLabel:UILabel?
    var countLabel:UILabel?
    var fromAddressLabel:UILabel?
    var toAddressLabel:UILabel?
    
    var middleBackView:UIView?
    var typeLabel:UILabel?
    var feesLabel:UILabel?
    var remarkLabel:UILabel?
    var timeLabel:UILabel?

    var bottomBackView:UIView?
    var heightLabel:UILabel?
    var hashBlockLabel:UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
        getRecordDetail()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK: - UI
extension WalletRecordDetaiInfolVC {
    func layoutUI(){
        showImageView?.isHidden = true
        titleLabel?.text = "交易详情".local
        addBView()
        
    }
    
    //白色背景框
    func addBView(){
        let xSpace = ScreenWidth*16.0/375.0
        let vTop = naviHeight + ScreenWidth*51.0/375.0
        let vBottom = ScreenHeight - ScreenWidth*24.0/375.0
        backSView = UIView(frame: CGRect(x: xSpace, y: vTop, width: ScreenWidth - 2*xSpace, height: vBottom-vTop))
        backSView?.backgroundColor = UIColor.white
        backSView?.layer.cornerRadius = 6
        backSView?.layer.shadowColor = UIColor.grayColor(v: 50).cgColor
        backSView?.layer.shadowRadius = 3
        backSView?.layer.shadowOffset = CGSize(width: 2, height: 3)
        backSView?.layer.shadowOpacity = 0.5
        self.view.addSubview(backSView!)
        
        let imageWidth = ScreenWidth*60.0/375.0
        let imageTop = vTop - imageWidth/2.0
        let imageX = (ScreenWidth - imageWidth)/2.0
        let iconImageView = UIImageView(frame: CGRect(x: imageX, y: imageTop, width: imageWidth, height: imageWidth))
        iconImageView.image = UIImage(named: "bet_record_wait")
        statusImageVIew = iconImageView
        self.view.addSubview(iconImageView)
        addSCView()
    }
    
    //scrollview及其子控件
    func addSCView(){
        let scX = ScreenWidth*14.0/375.0
        let scWidth = (backSView?.width())! - 2*scX
        mainScrollView = UIScrollView(frame:CGRect(x: scX, y: 0, width: scWidth, height: (backSView?.height())!))
        mainScrollView?.showsHorizontalScrollIndicator = false
        mainScrollView?.showsVerticalScrollIndicator = false

        backSView?.addSubview(mainScrollView!)
        addTopView()
        addMiddleView()
        addBottomView()
        
        let allHeight = (topBackView?.height())! + (middleBackView?.height())! + (bottomBackView?.height())!
        mainScrollView?.contentSize = CGSize(width:scWidth , height: allHeight)

    }
    
    //交易状态、交易金额、发款、收款方
    func addTopView(){
        let vWidth = (mainScrollView?.width())!
        topBackView = UIView(frame: CGRect(x: 0, y: 0, width: vWidth, height: ScreenWidth*270.0/375.0))
        mainScrollView?.addSubview(topBackView!)
        
        statusLabel = UILabel(frame: CGRect(x: 0, y: ScreenWidth*42.0/375.0, width: vWidth, height: ScreenWidth*22.0/375.0))
        statusLabel?.font = UIFont(name: kFontType, size: 16)
        statusLabel?.textColor = UIColor.RGBHex(0xef3f50)
        statusLabel?.textAlignment = .center
        topBackView?.addSubview(statusLabel!)
        
        countLabel = UILabel(frame: CGRect(x: 0, y: (statusLabel?.bottom())!+ScreenWidth*19.0/375.0, width: vWidth, height: ScreenWidth*33.0/375.0))
        countLabel?.font = UIFont(name: kFontType, size: 22)
        countLabel?.textColor = UIColor.black
        countLabel?.textAlignment = .center
        topBackView?.addSubview(countLabel!)
        
        let fromTabLabel = UILabel(frame: CGRect(x: 0, y: (countLabel?.bottom())!+ScreenWidth*30.0/375.0, width: vWidth, height: ScreenWidth*20.0/375.0))
        fromTabLabel.font = UIFont.systemFont(ofSize: 14)
        fromTabLabel.textColor = UIColor.black
        fromTabLabel.textAlignment = .left
        fromTabLabel.text = "发款方：".local
        topBackView?.addSubview(fromTabLabel)

        fromAddressLabel = UILabel(frame: CGRect(x: 0, y: fromTabLabel.bottom()+ScreenWidth*8.0/375.0, width: vWidth, height: ScreenWidth*17.0/375.0))
        fromAddressLabel?.textColor = UIColor.RGBHex(0x848484)
        fromAddressLabel?.textAlignment = .left
        fromAddressLabel?.font = UIFont.systemFont(ofSize: 12)
        topBackView?.addSubview(fromAddressLabel!)
        
        let toTabLabel = UILabel(frame: CGRect(x: 0, y: (fromAddressLabel?.bottom())!+ScreenWidth*12.0/375.0, width: vWidth, height: ScreenWidth*20.0/375.0))
        toTabLabel.font = UIFont.systemFont(ofSize: 14)
        toTabLabel.textColor = UIColor.black
        toTabLabel.textAlignment = .left
        toTabLabel.text = "收款方：".local
        topBackView?.addSubview(toTabLabel)
        
        toAddressLabel = UILabel(frame: CGRect(x: 0, y: toTabLabel.bottom()+ScreenWidth*8.0/375.0, width: vWidth, height: ScreenWidth*17.0/375.0))
        toAddressLabel?.textColor = UIColor.RGBHex(0x848484)
        toAddressLabel?.textAlignment = .left
        toAddressLabel?.font = UIFont.systemFont(ofSize: 12)
        topBackView?.addSubview(toAddressLabel!)
        
        let lineVIew = UIView(frame: CGRect(x: 0, y: (topBackView?.height())! - 1.5, width: vWidth, height: 0.5))
        lineVIew.backgroundColor = UIColor.grayColor(v: 180)
        topBackView?.addSubview(lineVIew)

    }
    
    //交易类型 消费 交易备注、交易时间
    func addMiddleView(){
        let vWidth = (mainScrollView?.width())!
        middleBackView = UIView(frame: CGRect(x: 0, y: (topBackView?.bottom())!, width: vWidth, height: ScreenWidth*144.0/375.0))
        mainScrollView?.addSubview(middleBackView!)
        //交易类型
        let typeTagLabel = UILabel(frame: CGRect(x: 0, y: ScreenWidth*24.0/375.0, width: 130, height: ScreenWidth*20.0/375.0))
        typeTagLabel.text = "交易类型：".local
        typeTagLabel.font = UIFont.systemFont(ofSize: 14)
        typeTagLabel.textColor = UIColor.black
        typeTagLabel.textAlignment = .left
        middleBackView?.addSubview(typeTagLabel)
    
        typeLabel = UILabel(frame: CGRect(x: 0, y: ScreenWidth*24.0/375.0, width: vWidth, height: ScreenWidth*20.0/375.0))
        typeLabel?.font = UIFont.systemFont(ofSize: 13)
        typeLabel?.textColor = UIColor.RGBHex(0x848484)
        typeLabel?.textAlignment = .right
        middleBackView?.addSubview(typeLabel!)
        //小费
        let feesTagLabel = UILabel(frame: CGRect(x: 0, y: typeTagLabel.bottom() + ScreenWidth*7.0/375.0, width: 100, height: ScreenWidth*20.0/375.0))
        feesTagLabel.text = "小费：".local
        feesTagLabel.font = UIFont.systemFont(ofSize: 14)
        feesTagLabel.textColor = UIColor.black
        feesTagLabel.textAlignment = .left
        middleBackView?.addSubview(feesTagLabel)
        
        feesLabel = UILabel(frame: CGRect(x: 0, y: feesTagLabel.top(), width: vWidth, height: ScreenWidth*20.0/375.0))
        feesLabel?.font = UIFont.systemFont(ofSize: 13)
        feesLabel?.textColor = UIColor.RGBHex(0x848484)
        feesLabel?.textAlignment = .right
        middleBackView?.addSubview(feesLabel!)
        //备注
        let remarkTagLabel = UILabel(frame: CGRect(x: 0, y: feesTagLabel.bottom() + ScreenWidth*7.0/375.0, width: 130, height: ScreenWidth*20.0/375.0))
        remarkTagLabel.text = "交易备注：".local
        remarkTagLabel.font = UIFont.systemFont(ofSize: 14)
        remarkTagLabel.textColor = UIColor.black
        remarkTagLabel.textAlignment = .left
        middleBackView?.addSubview(remarkTagLabel)
        
        remarkLabel = UILabel(frame: CGRect(x: 100, y: remarkTagLabel.top()-7, width: vWidth - 100, height: ScreenWidth*20.0/375.0+14))
        remarkLabel?.font = UIFont.systemFont(ofSize: 13)
        remarkLabel?.textColor = UIColor.RGBHex(0x848484)
        remarkLabel?.textAlignment = .right
        remarkLabel?.numberOfLines = 0
        middleBackView?.addSubview(remarkLabel!)
        //确认时间
        let timeTagLabel = UILabel(frame: CGRect(x: 0, y: remarkTagLabel.bottom() + ScreenWidth*7.0/375.0, width: 100, height: ScreenWidth*20.0/375.0))
        timeTagLabel.text = "确认时间：".local
        timeTagLabel.font = UIFont.systemFont(ofSize: 14)
        timeTagLabel.textColor = UIColor.black
        timeTagLabel.textAlignment = .left
        middleBackView?.addSubview(timeTagLabel)
        
        timeLabel = UILabel(frame: CGRect(x: 0, y: timeTagLabel.top(), width: vWidth, height: ScreenWidth*20.0/375.0))
        timeLabel?.font = UIFont.systemFont(ofSize: 13)
        timeLabel?.textColor = UIColor.RGBHex(0x848484)
        timeLabel?.textAlignment = .right
        middleBackView?.addSubview(timeLabel!)
        
        let lineVIew = UIView(frame: CGRect(x: 0, y: (middleBackView?.height())! - 1.5, width: vWidth, height: 0.5))
        lineVIew.backgroundColor = UIColor.grayColor(v: 180)
        middleBackView?.addSubview(lineVIew)

    }
    
    //确认时间和blockmash
    func addBottomView(){
        let vWidth = (mainScrollView?.width())!
        bottomBackView = UIView(frame: CGRect(x: 0, y: (middleBackView?.bottom())!, width: vWidth, height: ScreenWidth*144.0/375.0))
        mainScrollView?.addSubview(bottomBackView!)
        
        let heightTabLabel = UILabel(frame: CGRect(x: 0, y: ScreenWidth*24.0/375.0, width: 100, height: ScreenWidth*20.0/375.0))
        heightTabLabel.text = "确认时间：".local
        heightTabLabel.font = UIFont.systemFont(ofSize: 14)
        heightTabLabel.textColor = UIColor.black
        heightTabLabel.textAlignment = .left
        bottomBackView?.addSubview(heightTabLabel)
        
        heightLabel = UILabel(frame: CGRect(x: 0, y: heightTabLabel.top(), width: vWidth, height: ScreenWidth*20.0/375.0))
        heightLabel?.font = UIFont.systemFont(ofSize: 13)
        heightLabel?.textColor = UIColor.RGBHex(0x848484)
        heightLabel?.textAlignment = .right
        bottomBackView?.addSubview(heightLabel!)
        
        let toTabLabel = UILabel(frame: CGRect(x: 0, y: heightTabLabel.bottom()+ScreenWidth*7.0/375.0, width: vWidth, height: ScreenWidth*20.0/375.0))
        toTabLabel.font = UIFont.systemFont(ofSize: 14)
        toTabLabel.textColor = UIColor.black
        toTabLabel.textAlignment = .left
        toTabLabel.text = "确认blockHash：".local
        bottomBackView?.addSubview(toTabLabel)
        
        hashBlockLabel = UILabel(frame: CGRect(x: 0, y: toTabLabel.bottom()+ScreenWidth*8.0/375.0, width: vWidth, height: ScreenWidth*38.0/375.0))
        hashBlockLabel?.textColor = UIColor.RGBHex(0x848484)
        hashBlockLabel?.textAlignment = .left
        hashBlockLabel?.numberOfLines = 0
        hashBlockLabel?.font = UIFont.systemFont(ofSize: 12)
        bottomBackView?.addSubview(hashBlockLabel!)
    }
}

//MARK: - data
extension WalletRecordDetaiInfolVC{
    func getRecordDetail(){

        let path:String = httpPath(path: HTTPPath.交易详情_G)
        let address = AccountManager.getAccount().address

        let requestPath = path + address + "/" + (listModel?.txid)!

        LHRequest.get(url: requestPath,parameters: [:],runHUD: .loading, success: {[weak self] (json) in

            self?.detailModel = WalletRecordDetailModel.getModels(json: json)
            self?.refreshData(model: (self?.detailModel)!)

        }) { (error) in }
    }
    
    func refreshData(model:WalletRecordDetailModel){
        if listModel?.txstatus == 1{
            statusLabel?.text = NSLocalizedString("已确认", comment: "")
            statusImageVIew?.image = UIImage(named: "bet_record_success")

        }else if listModel?.txstatus == 2{
            statusLabel?.text = NSLocalizedString("待确认", comment: "")
            statusImageVIew?.image = UIImage(named: "bet_record_wait")

        }else if listModel?.txstatus == 3 {
            //确认失败
            statusLabel?.text = NSLocalizedString("交易失败", comment: "")
            statusImageVIew?.image = UIImage(named: "bet_record_failure")
        }
        
        var countS:String = "\(listModel?.txmoney ?? 0)".removeLost0()
        let tagS:String = " "+(listModel?.symbol)!
        
        if (listModel?.txmoney)! > Double(0) {
            countS = "+" + countS
        }else if listModel?.txmoney == Double(0){
            countS = "-0.0 "
        }
        let countAS = NSMutableAttributedString(string: countS, attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 24),NSAttributedStringKey.foregroundColor:UIColor.black])
        let tagAS = NSMutableAttributedString(string: tagS,attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12),
                                                                        NSAttributedStringKey.foregroundColor:UIColor.lightGray()])
        countAS.append(tagAS)
        countLabel?.attributedText = countAS
        
        fromAddressLabel?.text = model.faddress
        toAddressLabel?.text = model.taddress
        
        let chineseType = MyAssertCellModel.getChineseTypeWithType(model.txtype)
        typeLabel?.text = chineseType
        feesLabel?.text = "\(model.txfees)"
        remarkLabel?.text = model.remark
        timeLabel?.text = model.confirmedtime
        
        heightLabel?.text = "\(model.confirmedtime)"
        hashBlockLabel?.text = model.blockhash
        hashBlockLabel?.sizeToFit()
    }
}
