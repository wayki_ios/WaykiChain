

import UIKit

class WalletImportSuccessVC: NavBaseVC {
    var backView:UIView?
    var successImageView:UIImageView?
    var proLabel:UILabel?
    var startBtn:UIButton?
    var fromTheInitial:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

//MARK: - UI
extension WalletImportSuccessVC{
    func layoutUI(){
        titleLabel?.text = NSLocalizedString("导入成功", comment: "")
        leftItem?.isHidden = true
        showImageView?.isHidden = true
        addShowImageAndProLabel()
        addStartUseBtn()
    }
    
    func addShowImageAndProLabel(){
        let viewWidth  = ScreenWidth*200.0/375.0
        let imageWidth = ScreenWidth
        successImageView = UIImageView(image: UIImage(named: "wallet_succeed_black"))
        successImageView?.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageWidth)
        self.view.addSubview(successImageView!)
        
        proLabel = UILabel(frame: CGRect(x: 0, y: naviHeight + viewWidth - 25, width: ScreenWidth, height: 25))
        proLabel?.text = NSLocalizedString("钱包导入成功", comment: "")
        proLabel?.textAlignment = .center
        proLabel?.textColor = UIColor.white
        proLabel?.font = UIFont(name: kFontType, size: 20)
        self.view.addSubview(proLabel!)
    }
    
    func addStartUseBtn(){
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = btnWidth*44.0/327.0
        let btnTop = (proLabel?.bottom())! + ScreenWidth*65.0/375.0
        startBtn = UIButton(type: .custom)
        startBtn?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: btnTop, width: btnWidth, height: btnHeight)
        startBtn?.setTitle(NSLocalizedString("开始使用", comment: ""), for: .normal)
        startBtn?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        startBtn?.addTarget(self, action: #selector(startUse), for: .touchUpInside)
        
        self.view.addSubview(startBtn!)
    }
}


//MARK: - event
extension WalletImportSuccessVC{
    @objc func startUse(){
        let keyWindow = UIApplication.shared.keyWindow
        keyWindow?.rootViewController = NavigationManager.shared.configureControlers()
    }
}
