

import UIKit

class WalletCreateC: NavBaseVC {
    

    
    var pwdBackView:UIView?
    var passwordTF:InputPasswordTextFiled?
    var makesureTF:InputPasswordTextFiled?
    
    var agreenBtn:UIButton?
    var protocolLabel:CJLabel?
    
    var startCreateButton:UIButton?
    
    var fromTheInitial:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutView()
        checkAddAlert()
        NotificationCenter.default.addObserver(self, selector: #selector(listenInBtnStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


//MARK:- UI
extension WalletCreateC{
    func layoutView(){
        titleLabel?.text = "创建钱包".local
        addUpperBackShowImageView(imageName: "wallet_header_bgd_short")

        addTopSHowView()
        addPasswordView()
        addAgreenBtnAndProtocol()
        addStartBtn()
    }

    
    
    func addTopSHowView(){
       ImportOrCreateWalletManger.addRemarkLabel(superview: self.view)
    }
    
    
    
    func addPasswordView(){
        let bvWidth = ScreenWidth*327.0/375.0
        let bvHeight = bvWidth*135.0/327.0
        let bvTop = (showImageView?.height())! - ScreenWidth*17.0/375.0
        let backView = UIView(frame: CGRect(x: (ScreenWidth - bvWidth)/2.0, y: bvTop, width: bvWidth, height: bvHeight))
        backView.backgroundColor = UIColor.white
        backView.layer.cornerRadius = 6
        pwdBackView = backView
        self.view.addSubview(backView)
        
        let tfSpace = ScreenWidth*20.0/375.0
        let tftopSpace = ScreenWidth*24.0/375.0
        let tfWidth = bvWidth - tfSpace*2
        let tfHieght = (bvHeight - 2*tftopSpace - 1)/2.0
        passwordTF = InputPasswordTextFiled(frame: CGRect(x: tfSpace, y: tftopSpace, width: tfWidth, height: tfHieght))
        passwordTF?.font = UIFont(name: kFontType, size: 18)
        passwordTF?.isSecureTextEntry = true
        passwordTF?.delegate = self
        passwordTF?.placeholder = NSLocalizedString("请设置钱包密码（8～16位）", comment: "")
        backView.addSubview(passwordTF!)
        
        let lineView = UIView(frame: CGRect(x: tfSpace, y: (passwordTF?.bottom())!, width: tfWidth, height: 0.5))
        lineView.backgroundColor = UIColor.gray
        backView.addSubview(lineView)
        
        makesureTF = InputPasswordTextFiled(frame: CGRect(x: tfSpace, y: lineView.bottom(), width: tfWidth, height: tfHieght))
        makesureTF?.font = UIFont(name: kFontType, size: 18)
        makesureTF?.isSecureTextEntry = true
        makesureTF?.delegate = self
        makesureTF?.placeholder = NSLocalizedString("重复密码", comment: "")
        backView.addSubview(makesureTF!)
    }
    
    func addAgreenBtnAndProtocol(){
        let space = ScreenWidth*24.0/375.0
        let topSpace = (pwdBackView?.bottom())! + ScreenWidth*20.0/375.0
        agreenBtn = UIButton(type: .custom)
        agreenBtn?.frame = CGRect(x: space, y: topSpace, width: ScreenWidth*20.0/375.0, height: ScreenWidth*20.0/375.0)
        agreenBtn?.setImage(UIImage(named: "wallet_item_normal"), for: .normal)
        agreenBtn?.setImage(UIImage(named: "wallet_item_select"), for: .selected)
        agreenBtn?.addTarget(self, action: #selector(agreedProtocol(_:)), for: .touchUpInside)
        self.view.addSubview(agreenBtn!)
        
        let protocolWidth = ScreenWidth - (agreenBtn?.right())! - ScreenWidth*8.0/375.0
        protocolLabel = CJLabel(frame: CGRect(x: (agreenBtn?.right())! + ScreenWidth*8.0/375.0, y: topSpace, width: protocolWidth, height: 20))
        let truple = ImportOrCreateWalletManger.addAttStylesWithLabel(label: protocolLabel!)
        protocolLabel?.addLinkString(truple.secondStr, linkAddAttribute: truple.attStyles, block: { [weak self](linkModel) in
            self?.onLookUserProtol()
        })
        self.view.addSubview(protocolLabel!)
    }
    
    func addStartBtn(){
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = btnWidth*44.0/327.0
        startCreateButton = UIButton(type: .custom)
        startCreateButton?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: (protocolLabel?.bottom())! + ScreenWidth*30.0/375.0, width: btnWidth, height: btnHeight)
        startCreateButton?.setTitle("创建钱包".local, for: .normal)
        startCreateButton?.setTitleColor(UIColor.white, for: .normal)
        startCreateButton?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        startCreateButton?.setBackgroundImage(UIImage(named: "wallet_button_unable"), for: .disabled)
        startCreateButton?.isEnabled = false
        startCreateButton?.layer.cornerRadius = 6
        startCreateButton?.addTarget(self, action: #selector(startCreate), for: .touchUpInside)
        self.view.addSubview(startCreateButton! )
        
    }
    
}

//MARK: - UITextFieldDelegate
extension WalletCreateC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }else if (textField.text?.count)!>=16{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: - event
extension WalletCreateC{
    @objc func agreedProtocol(_ btn:UIButton){
        btn.isSelected = !btn.isSelected
        listenInBtnStatus()
    }
    
    @objc func onLookUserProtol(){
        let c = ShowProtocolVC()
        self.navigationController?.pushViewController(c, animated: true)
    }
    
    //开始点击创建钱包
    @objc func startCreate(){
        view.endEditing(true)
        if agreenBtn?.isSelected == false {
            return
        }
        let password = passwordTF?.text
        let makesurePWD = makesureTF?.text
        ImportOrCreateWalletManger.startCreateCheck((agreenBtn?.isSelected)!, password: password!, makesurePWD: makesurePWD!) { (isSuccess, alertStr, address, helpStr,password) in
            if isSuccess==true{
                LHRequest.dismissHUD()
                self.createWalletRequest(password: password, address: address, helpString: helpStr)
            }else{
                UILabel.showFalureHUD(text: alertStr)
            }
        }
        
    }
    
    //判断按钮是否可点击
    @objc func listenInBtnStatus(){
        let isAgreen = agreenBtn?.isSelected
        var isEnable = false
        if isAgreen==true {
            if ((passwordTF?.text?.count)!>0) && ((makesureTF?.text?.count)!>0){
                isEnable = true
            }else{
                isEnable = false
            }
        }else{
            isEnable = false
        }
        startCreateButton?.isEnabled = isEnable
    }
    
    //判断是否需要添加弹框（从app内创建）
    func checkAddAlert(){
        if fromTheInitial {
            return
        }
        
        let alertView = UIAlertController.init(title: "提示".local, message: "完成新建后原有钱包可通过助记词找回，请确认已备份".local, preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction.init(title: "确定".local, style: .default, handler: { (action) in
            
        })
        alertView.addAction(ok)
        self.present(alertView, animated: true, completion: nil)
    }
    
}

//MARK: - request
extension WalletCreateC{
    func createWalletRequest(password:String,address:String ,helpString:String){
        let mhash = Bridge.getWalletHash(from: helpString)
        var dic =  ["address":address,"mhash":mhash] as? [String:String]
        LHRequest.showHUD()
        LHRequest.post(url: httpPath(path: .创建钱包_P),
                       parameters:dic!,
                       runHUD: .none,
                       success: { (json) in
                        print("创建钱包_P:\(json)")
                        LHRequest.dismissHUD()
                        UmengEvent.eventWithDic(name: "create_success")
                        let account = NewAccount()
                        account.address = address
                        account.mHash = mhash!
                        account.setEncyptHelpStringAndPassword(helpStr: helpString, password: password)
                        AccountManager.saveAccount(account: account)
                        
                        let c = WalletCreateSuccessVC()
                        c.password = password
                        self.navigationController?.pushViewController(c, animated: true)
                        var vcArr = self.navigationController?.viewControllers
                        vcArr = [c]
                        self.navigationController?.setViewControllers(vcArr!, animated: false)
                   
        }) { (str) in
            UILabel.showFalureHUD(text: str)
        }}
    
}
