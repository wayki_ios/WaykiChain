

import UIKit

class WealthSumModel: NSObject {
    
    var amount:Double = 0  //持有数量
    var icon:String =  ""//币图标
    var symbol:String = ""//币符号 例如：WICC
    var unitPrice:Double = 0//单价

    
    class func getModels(json:JSON)->[WealthSumModel]{
        var models:[WealthSumModel] = []
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{if status != 1 {return models }}
            if let result = dic["result"]?.array{
                for lo in result {
                    let model = WealthSumModel()
                    model.analysisData(json:lo)
                    models.append(model)
                }
            }
        }
        return models
    }
    
    func analysisData(json:JSON){
        if let val = json["amount"].double{ amount = val }
        if let val = json["icon"].string{ icon = val }
        if let val = json["symbol"].string{ symbol = val }
        if let val = json["price"].double{ unitPrice = val }

    }
    
}
