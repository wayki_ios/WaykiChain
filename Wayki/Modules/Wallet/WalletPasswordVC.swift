

import UIKit

class WalletPasswordVC: NavBaseVC {
    
    var inputBackView:UIView?
    
    var oldPwdTF:InputPasswordTextFiled?
    var nPwdTF:InputPasswordTextFiled?
    var makeSurePWDTF:InputPasswordTextFiled?
    
    var confirmBtn:UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
        NotificationCenter.default.addObserver(self, selector: #selector(listenInBtnStatus), name: NSNotification.Name.UITextFieldTextDidChange, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

//MARK: - UI
extension WalletPasswordVC{
    func layoutUI(){
        titleLabel?.text = "修改密码".local
        addUpperBackShowImageView(imageName: "wallet_header_bgd_shortest")
        addInputBV()
        addConfirmBtn()
    }
    
    func addInputBV(){
        let xSpace = ScreenWidth*24.0/375.0
        let vTop = (showImageView?.bottom())! - ScreenWidth*20.0/375.0
        let vWidth = ScreenWidth - 2*xSpace
        let VHeight = vWidth*188.0/327.0
        inputBackView = UIView(frame: CGRect(x: xSpace, y: vTop, width: vWidth, height: VHeight))
        inputBackView?.backgroundColor = UIColor.white
        inputBackView?.layer.cornerRadius = 6
        self.view.addSubview(inputBackView!)
        
        let bSpace = ScreenWidth*20.0/375.0
        let tfHeight = ScreenWidth*55.0/375.0
        let tfWidth = vWidth - 2*bSpace
        let bTop = (VHeight - 3*tfHeight)/2.0
        let backView = UIView(frame: CGRect(x: bSpace, y: bTop, width: tfWidth, height: tfHeight*3))
        inputBackView?.addSubview(backView)
        
        oldPwdTF = InputPasswordTextFiled(frame: CGRect(x: 0, y: 0, width: tfWidth, height: tfHeight))
        oldPwdTF?.font = UIFont.systemFont(ofSize: 16)
        oldPwdTF?.delegate = self
        oldPwdTF?.isSecureTextEntry = true
        oldPwdTF?.placeholder = "请输入原密码".local
        backView.addSubview(oldPwdTF!)
    
        let line1 = UIView(frame: CGRect(x: 0, y: (oldPwdTF?.height())! - 0.5, width: (oldPwdTF?.width())!, height: 0.5))
        line1.backgroundColor = UIColor.grayColor(v: 150)
        oldPwdTF?.addSubview(line1)
        
        nPwdTF = InputPasswordTextFiled(frame: CGRect(x: 0, y: (oldPwdTF?.bottom())!, width: tfWidth, height: tfHeight))
        nPwdTF?.font = UIFont.systemFont(ofSize: 16)
        nPwdTF?.delegate = self
        nPwdTF?.isSecureTextEntry = true
        nPwdTF?.placeholder = "请输入新密码（8～16位）".local
        backView.addSubview(nPwdTF!)
        
        let line2 = UIView(frame: CGRect(x: 0, y: (nPwdTF?.height())! - 0.5, width: (nPwdTF?.width())!, height: 0.5))
        line2.backgroundColor = UIColor.grayColor(v: 150)
        nPwdTF?.addSubview(line2)
        
        makeSurePWDTF = InputPasswordTextFiled(frame: CGRect(x: 0, y: (nPwdTF?.bottom())!, width: tfWidth, height: tfHeight))
        makeSurePWDTF?.font = UIFont.systemFont(ofSize: 16)
        makeSurePWDTF?.delegate = self
        makeSurePWDTF?.isSecureTextEntry = true
        makeSurePWDTF?.placeholder = "请重复输入新密码".local
        backView.addSubview(makeSurePWDTF!)
    }
    
    func addConfirmBtn(){
        
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = ScreenWidth*44.0/375.0
        confirmBtn = UIButton(type: .custom)
        confirmBtn?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: (inputBackView?.bottom())! + ScreenWidth*30.0/375.0, width: btnWidth, height: btnHeight)
        confirmBtn?.setTitle("确定".local, for: .normal)
        confirmBtn?.setTitleColor(UIColor.white, for: .normal)
        confirmBtn?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        confirmBtn?.setBackgroundImage(UIImage(named: "wallet_button_unable"), for: .disabled)
        confirmBtn?.isEnabled = false
        confirmBtn?.layer.cornerRadius = 6
        confirmBtn?.addTarget(self, action: #selector(clickConfirm), for: .touchUpInside)
        self.view.addSubview(confirmBtn! )
    }
    

}

extension WalletPasswordVC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" {
            return true
        }else if (textField.text?.count)!>=16{
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK: - event
extension WalletPasswordVC{
    @objc func clickConfirm(){
        let oldPassword = oldPwdTF?.text
        let surePassword = nPwdTF?.text
        let makeSurePassword = makeSurePWDTF?.text

        
        if ((oldPassword?.count)! < 1) {
            addTextHUD(text: NSLocalizedString("请输入原密码",comment: ""), type: 2)
            
            return
        } else if (surePassword?.count)! < 1{
            addTextHUD(text: NSLocalizedString("请输入新密码",comment: ""), type: 2)
            return
        } else if (makeSurePassword?.count)! < 1{
            addTextHUD(text: NSLocalizedString("请重复输入新密码",comment: ""), type: 2)
            return
        }
        
        if checkPassword(oldPWD: oldPassword!) {
            //原密码相等于输入的原密码
            if surePassword == makeSurePassword {
                //新密码两次输入是否一致
                if ((surePassword?.count)! >= 8)&&((surePassword?.count)! <= 16){
                    //新密码长度是否符合要求
                    //更新新密码
                    updateNewPassword(oldPassword: oldPassword!, newPassword: surePassword!)
                    UmengEvent.eventWithDic(name: "resetPwd_success")
                    addTextHUD(text: NSLocalizedString("修改成功",comment: ""), type: 2)
                    perform(#selector(popVC), with: nil, afterDelay: 1.2)
                }else{
                    addTextHUD(text: NSLocalizedString("请输入8~16位新密码",comment: ""), type: 2)
                }
            }else{
                
                addTextHUD(text: NSLocalizedString("两次密码输入不一致",comment: ""), type: 2)
            }
        }else{
            
            addTextHUD(text: NSLocalizedString("原密码输入错误",comment: ""), type: 2)
        }
    }
    
    //判断按钮是否可点击
    @objc func listenInBtnStatus(){
     
        var isEnable = false
        if ((nPwdTF?.text?.count)!>0) && ((oldPwdTF?.text?.count)!>0) && ((makeSurePWDTF?.text?.count)!>0){
                isEnable = true
        }else{
                isEnable = false
        }

        confirmBtn?.isEnabled = isEnable
        
    }
    
    @objc func popVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //判断和原密码是否相等
    private func checkPassword(oldPWD:String) ->Bool{
        var isEqual = false
        let account = AccountManager.getAccount()
        isEqual = account.checkPassword(inputPassword: oldPWD)
        
        return isEqual
    }
    //更新新密码
    private func updateNewPassword(oldPassword:String,newPassword:String){
        let account = AccountManager.getAccount()
        let isEqual = account.checkAndUpdatePassword(oldPassword: oldPassword, nPassword: newPassword)
        if isEqual  ==  false {
            addTextHUD(text: NSLocalizedString("原密码输入错误",comment: ""), type: 2)
        }
    }
}
