

import UIKit

class ShowProtocolVC: NavBaseVC {
    var webView:UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        layoutUI()
        loadPDF()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//MARK: - UI
extension ShowProtocolVC{
    func layoutUI(){
        titleLabel?.text = "服务及隐私条款".local;
        
        webView = UIWebView(frame: CGRect(x: 0, y: naviHeight, width: ScreenWidth, height: ScreenHeight - naviHeight))
        
        self.view.addSubview(webView)
    }
    func loadPDF() {
        let documentsPath = Bundle.main.path(forResource: "WaykichainServiceAgreement", ofType: "pdf")
        
        let urlStr = URL.init(fileURLWithPath: documentsPath!)
        webView.loadRequest(URLRequest(url:urlStr))
        
    }
}
