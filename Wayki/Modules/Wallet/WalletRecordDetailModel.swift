

import UIKit

class WalletRecordDetailModel: NSObject {
    var blockhash:String = "" //确认blockhash
    var confirmedtime:String = "" //确认时间
    var confirmheight:Int64 = 0 //确认高度 ,
    var faddress:String = "" //转出地址 ,
    var overheight:Int64 = 0 //超时高度 ,
    var symbol:String = ""//币符号，例如：WICC
    var taddress:String = ""//交易地址
    var txfees:Double = 0 //小费 ,
    var txid:String = ""// 交易hash
    var txtype:Int = 0//txtype
    var ver:Int = 0//ver
    var remark:String = ""//ver
    
    class func getModels(json:JSON)->WalletRecordDetailModel{
        let model = WalletRecordDetailModel()
        
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{
                if status != 1 {
                    return model
                }else{
                    let result = dic["result"]?.dictionaryValue
                    if result != nil{
                        model.analysisData(json:dic["result"]!)
                    }
                }
                
            }
        }
        return model
    }
    
    func analysisData(json:JSON){
        if let val = json["blockhash"].string{ blockhash = val }
        if let val = json["confirmedtime"].int64{ confirmedtime = Date(timeIntervalSince1970: TimeInterval(val)).dateString() }
        if let val = json["confirmheight"].int64{ confirmheight = val }
        if let val = json["faddress"].string{ faddress = val }
        if let val = json["overheight"].int64{ overheight = val }
        
        if let val = json["symbol"].string{ symbol = val }
        if let val = json["taddress"].string{ taddress = val }
        if let val = json["txfees"].double{ txfees = val }
        if let val = json["txid"].string{ txid = val }
        if let val = json["txtype"].int{ txtype = val }
        if let val = json["ver"].int{ ver = val }
        if let val = json["txremark"].string{ remark = val }
    }
    
}
