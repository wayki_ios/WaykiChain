//
//  EX_AlertImageView.swift
//  Wayki
//
//  Created by sorath on 2018/7/3.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class EX_AlertImageView: UIView {
    
    var showImageView:UIImageView?
    var tTop:CGFloat = 0
    var imageNameStr:String = ""
    
    init(frame: CGRect = CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight),triangleTop:CGFloat,imageName:String) {
        super.init(frame: frame)
        tTop = triangleTop
        imageNameStr = imageName
        layoutUI()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        self.addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK:-UI
extension EX_AlertImageView{
    func layoutUI(){
        self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)
        let image = UIImage(named: imageNameStr)
        var imageSize = CGSize(width: ScreenWidth, height: ScreenHeight)
        if (image != nil){
            let iSize = image?.size
            imageSize = CGSize(width: ScreenWidth, height: ScreenWidth*(iSize?.height)!/(iSize?.width)!)
        }
        showImageView = UIImageView(frame: CGRect(x: 0, y: tTop, width: ScreenWidth, height: imageSize.height))
        showImageView?.image = image
        self.addSubview(showImageView!)
        
    }
}


//MARK: - event
extension EX_AlertImageView{
    func show(){
        let keyWindow = UIApplication.shared.keyWindow
        
        for vi in (keyWindow?.subviews)!{
            if vi.isKind(of: EX_AlertImageView.self){
                vi.removeFromSuperview()
            }
        }
        
        showImageView?.alpha = 0
        keyWindow?.addSubview(self)

        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0.1)

            self.showImageView?.alpha = 1

        }, completion: nil)
        
    }
    
    @objc func dismiss(){
        self.layer.opacity = 1.0
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.showImageView?.alpha = 0
            self.backgroundColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0)

        }) { (isSuccess) in
            for vi in self.subviews{
                vi.removeFromSuperview()
            }
            self.removeFromSuperview()
        }
        
    }
}
