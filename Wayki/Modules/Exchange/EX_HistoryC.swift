//
//  EX_HistoryC.swift
//  Wayki
//
//  Created by louis on 2018/7/2.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class EX_HistoryC: FoundationC {
    
    var cellAction:((Int)->Void)?
    /// 上拉加载，下拉刷新
    var action:((Int)->Void)?
    
    private var table:UITableView!
    private var allDatas:[EX_HistoryM] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildV()
        配置导航条()
        table.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        TableViewAnimationKit.show(with: .fall, tableView: table)
    }
}

// MARK:- 创建界面
extension EX_HistoryC{
    
    func buildV(){
        view.backgroundColor = UIColor.clear
        let bgdV = UIImageView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight))
        bgdV.alpha = 0.7
        bgdV.image = UIImage(named: "wallet_bgd")
        view.addSubview(bgdV)
        view.backgroundColor = UIColor.black
        table = UITableView(frame: CGRect(x: 0, y: naviHeight, width: ScreenWidth, height: ScreenHeight-naviHeight))
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.backgroundColor = UIColor.clear
        view.addSubview(table)
    }
    
    func setDatas(allData:[EX_HistoryM]){
        allDatas = allData
    }
    
    private func 配置导航条(){
        
        let titleL = UILabel(frame: CGRect(x: 0, y: naviHeight-50, width: ScreenWidth, height: 50))
        titleL.textColor = UIColor.white
        titleL.text = "兑换记录".local
        titleL.textAlignment = .center
        view.addSubview(titleL)
        
        let rightBtn = UIButton(frame: CGRect(x: 0, y: naviHeight-50, width: 50, height: 50))
        rightBtn.layer.cornerRadius = 15
        rightBtn.setImage(UIImage(named: "main_back"), for: .normal)
        rightBtn.setImage(UIImage(named: "main_back"), for: .highlighted)
        rightBtn.addTarget(self, action: #selector(EX_HistoryC.back), for: .touchUpInside)
        view.addSubview(rightBtn)
    }
}


// MARK:- 列表代理
extension EX_HistoryC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160*scale
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cellAction != nil{
            cellAction!(indexPath.row)
            navigationController?.popViewController(animated: true)
        }
    }
}

// MARK:- 列表数据源代理
extension EX_HistoryC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return allDatas.count
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let model:EX_HistoryM =  allDatas[indexPath.row]
//        return EX_HistoryCell.getCell(table: tableView).configureFootball(model: model)
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model:EX_HistoryM = EX_HistoryM() //allDatas[indexPath.row]
        return EX_HistoryCell.getCell(table: tableView).configureFootball(model: model)
    }
}

// MARK:- 界面事件
extension EX_HistoryC{
    
    @objc func back(){
        navigationController?.popViewController(animated: true) // dismiss(animated: false, completion: nil)
    }
    
    @objc func loadMoreData(){ if action != nil{ action!(1)} }
    
    @objc func refreshData(){ if action != nil{action!(0)} }
    
}
