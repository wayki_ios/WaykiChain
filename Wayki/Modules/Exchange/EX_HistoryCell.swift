//
//  EX_HistoryCell.swift
//  Wayki
//
//  Created by louis on 2018/7/2.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class EX_HistoryCell: FoundationCell {
    
    var wiccTitleL:UILabel!
    var spcTitleL:UILabel!
    var spcCountL:UILabel!
    var wiccCountL:UILabel!
    var exchangeRateL:UILabel!
    var exchangeTimeL:UILabel!
    var statusL:UILabel!
    
    class func getCell(table:UITableView,id:String="EX_HistoryCell")->EX_HistoryCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = EX_HistoryCell() }
        return cell as! EX_HistoryCell
    }
    
    override func buildCell(frame: CGRect){
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        wiccTitleL = UILabel()
        wiccTitleL.backgroundColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.2)
        wiccTitleL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        wiccTitleL.layer.cornerRadius = 7
        wiccTitleL.textAlignment = .center
        wiccTitleL.font = UIFont(name: kFontTypeThin, size: 12)
        addSubview(wiccTitleL)
        
        spcTitleL = UILabel()
        spcTitleL.backgroundColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.2)
        spcTitleL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        spcTitleL.layer.cornerRadius = 4
        spcTitleL.textAlignment = .center
        spcTitleL.font = UIFont(name: kFontTypeThin, size: 12)
        addSubview(spcTitleL)
        
        wiccCountL = UILabel()
        wiccCountL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.9)
        addSubview(wiccCountL)
        
        spcCountL = UILabel()
        spcCountL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.9)
        addSubview(spcCountL)
        
        exchangeRateL = UILabel()
        exchangeRateL.textColor =  UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.8)
        exchangeRateL.font = UIFont(name: kFontTypeThin, size: 14)
        addSubview(exchangeRateL)

        exchangeTimeL = UILabel()
        exchangeTimeL.textColor =  UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.8)
        exchangeTimeL.font = UIFont(name: kFontTypeThin, size: 14)
        addSubview(exchangeTimeL)
        
        statusL = UILabel()
        statusL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        statusL.textAlignment = .right
        statusL.font = UIFont(name: kFontTypeThin, size: 14)
        addSubview(statusL)
        
        configureFrame()
        
        let line = UIView(frame: CGRect(x: 27*scale, y:150*scale , width: ScreenWidth-54*scale, height: 0.5))
        line.backgroundColor = UIColor.RGB(r: 255, g: 255, b: 255,alpha: 0.4)
        addSubview(line)
        
        
    }
    
    func configureFrame(){
        let x_1:CGFloat = 27*scale
        let x_2:CGFloat = ScreenWidth/2+27*scale
        
        let y_1:CGFloat = 20*scale
        let h_1:CGFloat = 16*scale
        
        let y_2:CGFloat = y_1 + h_1 + 5*scale
        let h_2:CGFloat = 22*scale
        
        let y_3:CGFloat = y_2 + h_2 + 25*scale
        let h_3:CGFloat = 22*scale
        
        let y_4:CGFloat = y_3 + h_3 + 5*scale
        let h_4:CGFloat = 22*scale
        
        wiccTitleL.frame = CGRect(x:x_1 , y: y_1, width: 50*scale, height: h_1)
        spcTitleL.frame = CGRect(x: x_2, y: y_1, width: 50*scale, height: h_1)
        wiccCountL.frame = CGRect(x: x_1, y: y_2, width: ScreenWidth/2-26*scale, height: h_2)
        spcCountL.frame = CGRect(x: x_2, y: y_2, width: ScreenWidth/2-26*scale, height: h_2)
        exchangeRateL.frame = CGRect(x: x_1, y: y_3, width: ScreenWidth-26*scale, height: h_3)
        exchangeTimeL.frame = CGRect(x: x_1, y: y_4, width: ScreenWidth/2-26*scale, height: h_4)
        statusL.frame = CGRect(x:x_2, y: y_4, width: ScreenWidth/2-52*scale, height: h_4)
        
        wiccTitleL.text = "WICC"
        spcTitleL.text = coinName
    }
    
}

//MARK: - 配置数据
extension EX_HistoryCell{
    
    func configureFootball(model:EX_HistoryM) -> Self{

        wiccCountL.text = "100000000000.0"
        spcCountL.text = "1000000.0"
        exchangeRateL.attributedText = exchangeAttString(title: "兑换汇率: ", content: "1WICC=20"+coinName)
        exchangeTimeL.attributedText =  exchangeAttString(title:  "兑换时间: ", content: "2018-06-01")
        statusL.text = "确认中.."
        
        return self
    }
    
    func exchangeAttString(title:String,content:String)-> NSAttributedString {
        let poolTitle = title
        let poolCount = content
        let titleAs = NSMutableAttributedString(string: poolTitle,attributes: [NSAttributedStringKey.font: UIFont(name: kFontTypeMedium, size: 12*scale) as Any,NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0xffffff,alpha: 0.5) as Any  ])
        let poolCountAS = NSMutableAttributedString(string: poolCount,attributes: [NSAttributedStringKey.font: UIFont(name: kFontBold, size: 12*scale) as Any,NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0xffffff,alpha: 0.8) as Any])
        titleAs.append(poolCountAS)
        return titleAs
    }
}

