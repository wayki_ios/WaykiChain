//
//  EX_MainC.swift
//  Wayki
//
//  Created by louis on 2018/7/2.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit
import AVFoundation

class EX_MainC: NavBaseVC {
    
    fileprivate var topView:UIView? //上部视图
    fileprivate var rateLabel:UILabel? //显示汇率label
    fileprivate var limitLabel:UILabel? //显示限额label
    
    fileprivate var middleView:UIView?//中部视图
    fileprivate var wiccInputTF:UITextField?//显示输入wicc的tf

    fileprivate var spcInputTF:UITextField?//显示输入spc的tf

    ///小费
    fileprivate var feeLabel:UILabel?
    fileprivate let slider = UISlider.init()
    
    fileprivate var rateModel:EX_MainRateModel = EX_MainRateModel()//目标地址、汇率、最大兑换金额，最小兑换金额
    
    var xSpace = scale*24
    
    var randomNum:Double = 0.000016 //wicc可用预先扣除值
    var wiccSum:Double = 0  //扣除预留小费随机值的wicc数量
    
    var couldEx_SPC:Double = 0//可兑换的SPC总数
    var couldEx_WICC:Double = 0 ///可兑换/输入wicc的最大数量
    var fees:Double = 0.0001 //手续费
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LHRequest.getSumWealth()
        layoutUI()
        getRateInfo()
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}



//MARK: - UITextFieldDelegate
extension EX_MainC:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let flag =  string.formartInputMoney(inputMoney: textField.text!, inputCharacter: string)
        if flag == false{
            return false
        }
        let str:NSString = textField.text! as NSString
        
        let showStr = str.replacingCharacters(in: range, with: string)
        let strArr = showStr.components(separatedBy: ".")
        
        if textField == wiccInputTF{
            if((strArr.last?.count)! > 8  ){
                //最大输入8位数
            }else{
                textField.text = showStr
                if showStr != "" && showStr != "." {
                    if let a = Double(showStr){
                        //如果大于能兑换的值，则修改
                        if a > self.wiccSum{
                            var amount = self.wiccSum
                            if amount < AccountManager.getAccount().wiccSumAmount - fees - randomNum{
                                amount = self.wiccSum
                            }else{
                                amount = AccountManager.getAccount().wiccSumAmount - fees - randomNum
                            }
                            if amount < 0 {
                                amount = 0
                            }
                            self.wiccInputTF?.text = String(format: "%.8f", amount).removeLost0()
                            //此时修改输入的可兑换wicc数量
                            couldEx_WICC = amount
                            
                        }else{
                            couldEx_WICC = a
                        }
                       
                        
                    }
                }else{
                    couldEx_WICC = 0
                }

                exchangeSPCNum()
            }
        }else{

            if(strArr.count>1 && (strArr.last?.count)! > 8  ){
                //最大输入8位数
            }else{
                textField.text = showStr
                if showStr != "" && showStr != "." {
                    if let b:Double = Double(showStr){
                        let a:Double = b/rateModel.exchangeRate
                        //如果大于能兑换的值，则修改
                        if a > self.wiccSum{
                            var amount = self.wiccSum
                            if amount < AccountManager.getAccount().wiccSumAmount - fees - randomNum{
                                amount = self.wiccSum
                            }else{
                                amount = AccountManager.getAccount().wiccSumAmount - fees - randomNum
                            }
                            if amount < 0 {
                                amount = 0
                            }
                            self.wiccInputTF?.text = String(format: "%.8f", amount).removeLost0()
                            self.spcInputTF?.text  = String(format: "%.8f", amount*rateModel.exchangeRate).removeLost0()
                            //UILabel.showFalureHUD(text: "您最多能兑换".local + (self.wiccInputTF?.text)! + "wicc")
                            //此时修改输入的可兑换wicc数量
                            couldEx_WICC = amount
                            
                        }else{
                            couldEx_WICC = a
                        }
                        
                    }
                }else{
                    couldEx_WICC = 0
                }
                exchangeWICCNum()
            }
        }
        return false

    }
}

//MARK: - event
extension EX_MainC{
    //确认兑换
    @objc func confirm(){
        //先判断是否激活
        if ActiviteManager.checkIsActivityAndActivity(vc: self) == false {
            return
        }
        
        if wiccInputTF?.text != nil && (wiccInputTF?.text?.count)! > 0 {
            let inputStr:String = (wiccInputTF?.text)!
            if let a = Double(inputStr){
                if a > self.wiccSum{
                    UILabel.showFalureHUD(text: "您输入的金额大于最大金额".local)
                    return
                }else if a<rateModel.exchangeMinLimit {
                    UILabel.showFalureHUD(text: "您输入的金额小于最小金额".local)
                    return
                }else{
                    //在限额之中
                    //调起输入密码框
                    //输入密码，然后调用转账等接口
                    AlertInputPwdView.shared.show().sureBlock = { pwd in
                        self.startExchange(transferM: a, pwd: pwd)
                    }
                    
                }
            }else{
                UILabel.showFalureHUD(text: "请输入数字".local)
                return
            }

            
        }else{
            UILabel.showFalureHUD(text: "请输入兑换金额".local)
            return
        }
        
        
    }
    
    //1000spc赠送100spc
    @objc func giving(){
        
    }
    
    //点击疑问
    @objc func clickQuestion(btn:UIButton){
        
        let alert = EX_AlertImageView(triangleTop: (btn.superview?.top())! + btn.bottom(), imageName: "LP_alert2_c".local)
        alert.show()
    }
    
    @objc func allin(){
        let showWicc = String(format: "%.8f", wiccSum).removeLost0()
        wiccInputTF?.text = showWicc
        couldEx_WICC = wiccSum

        exchangeMAXWiccToSPC()


    }
    
    ///slider滑块相应事件
    @objc func silderEvent(obj:UISlider){
        var  cf:Double! = 0
        cf = Double(Double(obj.value) * 0.0499+0.0001)
        let feeStr = String(format: "%.8f", cf)
        fees = Double(feeStr)!
        exchangeMAXWiccToSPC()
        // 小费更新
        let attrStr = NSMutableAttributedString.init(string: String(format: "小费:".local + "  %.8f %@", fees,"WICC"))
        attrStr.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], range: NSRange.init(location: 0, length: 3))
        feeLabel?.attributedText = attrStr
    }
    
}

//MARK: - logic

extension EX_MainC{
    //获取wicc最大可兑换数量，并且如果此时最大可兑换wicc的数量小于输入数量，则改变 输入数量
    //同时调用 置换 spc数量 方法
    private func exchangeMAXWiccToSPC(){
        let amount = AccountManager.getAccount().wiccSumAmount - randomNum - fees
        if amount>rateModel.exchangeMaxLimit {
            wiccSum = rateModel.exchangeMaxLimit
        }else if amount<rateModel.exchangeMinLimit{
            wiccSum = amount
        }else{
            wiccSum = amount
        }
        if wiccSum < 0{
            wiccSum = 0
        }
        if couldEx_WICC>wiccSum {
            couldEx_WICC = wiccSum
        }
        
        setInputWiccPlaceHloder()
        
        if wiccInputTF?.text != nil && (wiccInputTF?.text?.count)! > 0{
            let oldInputStr:String = (wiccInputTF?.text)!
            let oldInputValue:Double = Double(oldInputStr)!
            //如果此时可兑换wicc的数量小于输入数量，则改变 输入数量
            if wiccSum < oldInputValue{
                wiccInputTF?.text = String(format: "%.8f", wiccSum).removeLost0()
            }
        }
        //通过wicc值改变spc值
        exchangeSPCNum()
        
    }
    
    //改变wicc数量时调用，修改可兑换SPC数量
    private func exchangeSPCNum(){
        //wicc输入框调用，修改spc输入框
        if wiccInputTF?.text != nil && (wiccInputTF?.text?.count)! > 0 {
            couldEx_SPC = couldEx_WICC*rateModel.exchangeRate
            if couldEx_SPC < 0{
                couldEx_SPC = 0
            }
            spcInputTF?.text = String(format: "%.8f", couldEx_WICC*rateModel.exchangeRate).removeLost0()
        }else{
            //wicc 没有输入
            spcInputTF?.text = ""
            setInputSPCPlaceHloder()
            couldEx_SPC = 0
        }

    }
    
    //改变SPC数量时调用，修改可兑换WICC数量
    private func exchangeWICCNum(){
        //spc输入框调用，修改wicc输入框
        if spcInputTF?.text != nil && (spcInputTF?.text?.count)! > 0 {
            couldEx_SPC = couldEx_WICC*rateModel.exchangeRate
            if couldEx_SPC < 0{
                couldEx_SPC = 0
            }
            wiccInputTF?.text = String(format: "%.8f", couldEx_WICC).removeLost0()
            
        }else{
            //spc 没有输入
            wiccInputTF?.text = ""
            setInputWiccPlaceHloder()
            couldEx_WICC = 0
        }
    }
    
    
    //数据刷新页面
    private func refreshData(model:EX_MainRateModel){
        rateModel = model
        
        //计算wicc最大和最小限额
        exchangeMAXWiccToSPC()
        
        rateLabel?.text = "  " + "实时汇率".local + "  1 WICC = \(model.exchangeRate )" + coinName
        limitLabel?.text = "  " + "交易限额".local + "  \(model.exchangeMinLimit) ~ \(model.exchangeMaxLimit)  WICC"
        setInputWiccPlaceHloder()
    }
    
    //设置输入wicc 的 attributedPlaceholder
    private func setInputWiccPlaceHloder(){
        wiccInputTF?.attributedPlaceholder = NSMutableAttributedString(string: "最大可发".local + String(format: "%.8f", wiccSum).removeLost0(),
            attributes: [NSAttributedStringKey.font: UIFont(name: "PingFangSC-Regular", size: 16) as Any,
                         NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0xd8d8d8) as Any  ])
    }
    
    //设置输入spc 的 attributedPlaceholder
    private func setInputSPCPlaceHloder(){
        let spcShow = String(format: "%.8f", wiccSum*rateModel.exchangeRate).removeLost0()
        spcInputTF?.attributedPlaceholder = NSMutableAttributedString(string: "最大可收".local + spcShow,
                                                                      attributes: [NSAttributedStringKey.font: UIFont(name: "PingFangSC-Regular", size: 16) as Any,
                                                                                   NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0xd8d8d8) as Any  ])
    }
    
    
    //使页面恢复初始状态
    @objc private func restoreInitial(){
        LHRequest.getSumWealth()
        getRateInfo()
        fees = 0.0001
        couldEx_WICC = 0
        couldEx_SPC = 0
        
        setInputWiccPlaceHloder()
        setInputSPCPlaceHloder()
        wiccInputTF?.text = ""
        spcInputTF?.text = ""
        slider.setValue(0, animated: false)
        // 小费更新
        let attrStr = NSMutableAttributedString.init(string: String(format: "小费:".local + "  %.8f %@", fees,"WICC"))
        attrStr.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], range: NSRange.init(location: 0, length: 3))
        feeLabel?.attributedText = attrStr
        
    }
}

//MARK: - request
extension EX_MainC{
    func getRateInfo(){
        let typepath = HTTPPath.兑换信息_G
        let requestpath = httpPath(path: typepath)
        
        LHRequest.get(url: requestpath,parameters: [:],runHUD: .loading, success: {[weak self] (json) in
            print(json)
            let model = EX_MainRateModel.getModel(json: json)
            self?.refreshData(model: model)
        }) { (error) in
     
            UILabel.showFalureHUD(text: "网络异常，请重试")
        }
    }
    
    //发起wicc兑换spc接口（wicc转账）
    func startExchange(transferM:Double,pwd:String){

        LHRequest.getVaildHeight(symbol: "WICC") { (vaildHeight, _, appid) in
            //签名，从api中获取
            let toAddress:String = self.rateModel.recvAddress
            let helpStr = AccountManager.getAccount().getHelpString(password: pwd)
            let regid = AccountManager.getAccount().regId
            let fees = self.fees
            var hex:String! = ""
            hex = Bridge.getTransfetWICCHex(withHelpStr: helpStr, withPassword:pwd, fees:fees , validHeight: vaildHeight, srcRegId: regid, destAddr: toAddress, transferValue: transferM)
            self.initiateTransfer(hex: hex, remark: "")
        }
    }
    
    //发起转账
    func initiateTransfer(hex:String,remark:String){
        
        let path:String = httpPath(path: HTTPPath.发起转账交易_P)
        let mhash = AccountManager.getAccount().mHash
        
        let requestPath = path + mhash + "/" + "WICC"

        var pa:[String:Any] = [:]
        pa["signHex"] = hex
        pa["txRemark"] = remark
        LHRequest.post(url: requestPath, parameters: pa,runHUD: .loading, success: {(json) in
            UILabel.showSucceedHUD(text: "兑换申请成功，请等待区块确认".local)
            self.perform(#selector(self.restoreInitial), with: nil, afterDelay: 2)
            //页面状态归0
            
        }) { () in
            UILabel.showFalureHUD(text: "兑换失败，请稍后再试".local)
        }
    }
}


//MARK: - UI
extension EX_MainC{
    fileprivate func layoutUI(){
        titleLabel?.text = "WICC > \(coinName)"
        wiccSum = AccountManager.getAccount().wiccSumAmount - 0.000016  //扣除预留小费随机值的wicc数量
        addUpperBackShowImageView(imageName: "")
        //addRightBtn()
        addTopView()
        addMiddleView()
        addBottomView()
    }
    
    fileprivate func addRightBtn(){
        addRightBtn(title: "兑换记录".local)
    }
    
    //添加上方视图（汇率、限额、问号）
    fileprivate func addTopView(){
        let topSpace = naviHeight + scale*16
        topView = UIView(frame: CGRect(x: 0, y: topSpace, width: ScreenWidth, height: scale*92))
        self.view.addSubview(topView!)
        
        let tLabel = UILabel(frame: CGRect(x: xSpace, y: 0, width: (topView?.width())! - 2*scale, height: scale*18))
        tLabel.font = UIFont(name: "PingFangSC-Semibold", size: 13)
        tLabel.textColor = UIColor.init(white: 1, alpha: 0.8)
        tLabel.text = "兑换".local + coinName;
        tLabel.textAlignment = .left
        topView?.addSubview(tLabel)
        
        
        limitLabel = UILabel(frame: CGRect(x: xSpace + 6, y: tLabel.bottom() + scale*8, width: 200, height: 16))
        limitLabel?.font = UIFont(name: "PingFangSC-Medium", size: 11)
        limitLabel?.textColor = UIColor.init(white: 1, alpha: 0.8)
        limitLabel?.text = "  " + "交易限额".local;
        limitLabel?.textAlignment = .left
        topView?.addSubview(limitLabel!)
        
        let pointHeight:CGFloat = 5
        let firstPointViewTop = (limitLabel?.top())! + (limitLabel?.height())!/2.0 - pointHeight/2.0
        let firstPointView = UIView(frame: CGRect(x: xSpace, y: firstPointViewTop, width: pointHeight, height: pointHeight))
        firstPointView.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        firstPointView.layer.cornerRadius = pointHeight/2.0
        topView?.addSubview(firstPointView)
        
        rateLabel = UILabel(frame: CGRect(x: xSpace + 6, y: (limitLabel?.bottom())! + scale*4, width: 200, height: 16))
        rateLabel?.font = UIFont(name: "PingFangSC-Medium", size: 11)
        rateLabel?.textColor = UIColor.init(white: 1, alpha: 0.8)
        rateLabel?.text = "  " + "实时汇率".local;
        rateLabel?.textAlignment = .left
        topView?.addSubview(rateLabel!)
        
        let secondPointViewTop = (rateLabel?.top())! + (rateLabel?.height())!/2.0 - pointHeight/2.0
        let secondPointView = UIView(frame: CGRect(x: xSpace, y: secondPointViewTop, width: pointHeight, height: pointHeight))
        secondPointView.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        secondPointView.layer.cornerRadius = pointHeight/2.0
        topView?.addSubview(secondPointView)

        let questionBtn = UIButton(type: .custom)
        let btnWidth:CGFloat = 16
        let btnRightTpR:CGFloat = xSpace
        let btnX:CGFloat = (topView?.width())! - btnWidth - btnRightTpR
        questionBtn.frame = CGRect(x: btnX, y: (rateLabel?.top())!, width: btnWidth, height: btnWidth)
        questionBtn.addTarget(self, action: #selector(clickQuestion(btn:)), for: .touchUpInside)
        questionBtn.setBackgroundImage(UIImage(named: "LP_question"), for: .normal)
        topView?.addSubview(questionBtn)

        
    }
    
    //添加中部视图（输入wicc数量 可兑spc数量）
    fileprivate func addMiddleView(){
        let mXSPace = scale*20
        
        middleView  =  UIView(frame: CGRect(x: xSpace, y: (topView?.bottom())!, width: ScreenWidth-2*xSpace, height: scale*135))
        middleView?.backgroundColor = UIColor.white
        middleView?.layer.cornerRadius = 6
        self.view.addSubview(middleView!)
        
        let lineHeight:CGFloat = 0.5
        let lineVIew = UIView(frame: CGRect(x: mXSPace, y: (middleView?.height())!/2.0, width: (middleView?.width())! - 2*mXSPace, height: lineHeight))
        lineVIew.backgroundColor = UIColor.RGBHex(0xd8d8d8)
        middleView?.addSubview(lineVIew)
        
        
        spcInputTF  = UITextField(frame: CGRect(x: mXSPace, y: lineVIew.bottom(), width: lineVIew.width() - 60, height: scale*50))
        spcInputTF?.delegate = self
        spcInputTF?.keyboardType = .decimalPad
        spcInputTF?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        spcInputTF?.textColor = UIColor.RGBHex(0x333333)
        setInputSPCPlaceHloder()
        middleView?.addSubview(spcInputTF!)
        
        let spcRightLabel = UILabel(frame: CGRect(x: (middleView?.width())!-mXSPace-60, y: (spcInputTF?.top())!, width: 60, height: (spcInputTF?.height())!))
        spcRightLabel.font = UIFont(name: "Helvetica-BoldOblique", size: 14)
        spcRightLabel.textColor = UIColor.RGBHex(0x333333, alpha: 0.3)
        spcRightLabel.text = coinName
        spcRightLabel.textAlignment = .right
        middleView?.addSubview(spcRightLabel)
        
        wiccInputTF  = UITextField(frame: CGRect(x: mXSPace, y: lineVIew.top()-scale*50, width: (spcInputTF?.width())!, height: scale*50))
        wiccInputTF?.delegate = self
        wiccInputTF?.keyboardType = .decimalPad
        wiccInputTF?.font = UIFont(name: "PingFangSC-Regular", size: 16)
        wiccInputTF?.textColor = UIColor.RGBHex(0x333333)
        setInputWiccPlaceHloder()
        middleView?.addSubview(wiccInputTF!)
        
        let wiccRightLabel = UILabel(frame: CGRect(x: (wiccInputTF?.right())!, y: (wiccInputTF?.top())!, width: 60, height: (wiccInputTF?.height())!))
        wiccRightLabel.font = UIFont(name: "Helvetica-BoldOblique", size: 14)
        wiccRightLabel.textColor = UIColor.RGBHex(0x333333, alpha: 0.3)
        wiccRightLabel.text = "WICC"
        wiccRightLabel.textAlignment = .right
        middleView?.addSubview(wiccRightLabel)
        
//        let vLine = UIView(frame: CGRect(x: wiccRightLabel.right(), y: wiccRightLabel.top()+wiccRightLabel.height()/2.0-9, width: 0.5, height: 18))
//        vLine.backgroundColor = UIColor.RGBHex(0xd8d8d8)
//        middleView?.addSubview(vLine)
        
        
//        let allINputBtn = UIButton(type: .custom)
//        allINputBtn.frame = CGRect(x: vLine.right(), y: wiccRightLabel.top(), width: (middleView?.width())! - vLine.right() , height: wiccRightLabel.height())
//        allINputBtn.setTitle("全部兑出".local, for: .normal)
//        allINputBtn.setTitleColor(UIColor.RGBHex(0x333333), for: .normal)
//        allINputBtn.titleLabel?.font = UIFont(name: "PingFangSC-Medium", size: 13)
//        allINputBtn.addTarget(self, action: #selector(allin), for: .touchUpInside)
//        middleView?.addSubview(allINputBtn)
        
    }
    
    fileprivate func addBottomView(){
        feeLabel = UILabel(frame: CGRect(x: xSpace, y: (middleView?.bottom())! + 24*scale, width: ScreenWidth - 2*xSpace, height: 18*scale))
        feeLabel?.textColor = UIColor.RGBHex(0xA69A9B)
        let attrStr = NSMutableAttributedString.init(string: "小费:".local+" 0.000100 WICC")
        attrStr.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], range: NSRange.init(location: 0, length: 3))
        feeLabel?.attributedText = attrStr
        feeLabel?.font = UIFont.init(name: "Helvetica", size: 12)
        view.addSubview(feeLabel!)
        
        
        slider.frame = CGRect(x: xSpace-2, y: feeLabel!.bottom()+12*scale, width: feeLabel!.width()+4, height: 20*scale)
        slider.value = 0
        slider.maximumTrackTintColor = UIColor.init(white: 0.5, alpha: 0.5)
        slider.minimumTrackTintColor = UIColor.RGBHex(0xD65355)
        slider.setThumbImage(UIImage(named: "slider2"), for: .normal)
        slider.addTarget(self, action: #selector(silderEvent(obj: )), for: .valueChanged)
        view.addSubview(slider)
        
        let slow = UILabel(frame: CGRect(x: xSpace, y: slider.bottom() + 24*scale, width:40, height: 18))
        slow.textColor = UIColor.RGBHex(0xA69A9B)
        slow.text = "慢".local
        slow.font = UIFont.init(name: "PingFangSC-Medium", size: 12)
        view.addSubview(slow)
        
        let fast = UILabel.init(frame: CGRect(x: feeLabel!.right() - 40, y: slider.bottom() + 24*scale, width:40, height: 18))
        fast.textColor = UIColor.RGBHex(0xA69A9B)
        fast.text = "快".local
        fast.textAlignment = .right
        fast.font = UIFont.init(name: "PingFangSC-Medium", size: 12)
        view.addSubview(fast)
        
        
        let sureBtn = UIButton(type: .custom)
        sureBtn.frame = CGRect(x: xSpace, y: slow.bottom()+scale*30, width: ScreenWidth - 2*xSpace, height: 44*scale)
        sureBtn.layer.cornerRadius = 6
        sureBtn.setBackgroundImage(UIImage.init(named: "wallet_button_normal"), for: .normal)
        sureBtn.setTitle("确认兑换".local, for: .normal)
        sureBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        sureBtn.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        view.addSubview(sureBtn)
        //1000加赠100spc
//        let givingBtn = UIButton(type: .custom)
//        givingBtn.frame = CGRect(x: xSpace, y: ScreenHeight - scale*75 - xSpace, width: ScreenWidth-2*xSpace, height: scale*75)
//        givingBtn.setBackgroundImage(UIImage(named: "ex_add_btn"), for: .normal)
//        givingBtn.addTarget(self, action: #selector(giving), for: .touchUpInside)
//        view.addSubview(givingBtn)
    }
}

class EX_MainRateModel:NSObject{
    var recvAddress:String = ""//目标地址
    var exchangeRate:Double = 0.0001 //兑换倍率
    var exchangeMaxLimit:Double = 0
    var exchangeMinLimit:Double = 0
    
    class func getModel(json:JSON) ->EX_MainRateModel{
        let model = EX_MainRateModel()
        if let sep = json["recvAddress"].string{ model.recvAddress =  sep }
        if let sep = json["exchangeRate"].double{ model.exchangeRate =  sep }
        if let sep = json["exchangeMaxLimit"].double{ model.exchangeMaxLimit =  sep }
        if let sep = json["exchangeMinLimit"].double{ model.exchangeMinLimit =  sep }

        return model
    }
}
