//
//  MyBetView.swift
//  WaykiChain
//
//  Created by 1 on 2018/5/25.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class MyBetView: UITableView {
    ///0，1是足球篮球，2是事件
    var viewTag:Int = 0
    lazy var ball_dataArr : [MyBetListModel] = []
    lazy var event_dataArr : [MyBetListModel] = []
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
        self.separatorStyle = .none
        configDatas(datas: [GameListM]())
    }
    func configDatas(datas:[GameListM]){
        self.left()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
extension MyBetView : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewTag == 0 || viewTag == 1{
            return ball_dataArr.count
        }
        return event_dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if viewTag == 0 || viewTag == 1{
            let cell = MyBetBallCellTableViewCell.getCell(table: tableView)
            return cell.configureData(model: ball_dataArr[indexPath.row],viewTag: viewTag)
        }else{
            let cell = MyBetEventCell.getCell(table: tableView)
            return cell.configureData(model: event_dataArr[indexPath.row])
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let bgdW = ScreenWidth-40*scale
        let bgdH = bgdW*(110/335)
        return bgdH + 45*scale
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewTag == 2{
            requestData(orderID: event_dataArr[indexPath.row].orderid)
        }else if viewTag == 1{
            requestData(orderID: ball_dataArr[indexPath.row].orderid)
        }else{
            requestData(orderID: ball_dataArr[indexPath.row].orderid)
        }

    }
}

extension MyBetView{
    func requestData(orderID:Int){
        let address = AccountManager.getAccount().address
        let gamePath = httpPath(path: viewTag == 0 ? HTTPPath.足球投注详情_G : HTTPPath.篮球投注详情_G )
        let requestPath = gamePath+"\(address)/%7Borderid%7D?orderid=\(orderID)"
        
        LHRequest.get(url: requestPath,
                      parameters: [:],runHUD:  .loading,
                      success: { [weak self] (json) in
                        self?.handleDetailData(json: json)
                        
        }) { (error) in }
    }
    
    func handleDetailData(json:JSON){

        let data = GameResultModel.getModel(json: json)
        let c = GameEventResultVC()

        if data.isEvent == true {
            c.refreshUI(data: data, resultType: .event)
        }else{
            c.refreshUI(data: data, resultType: .other)
        }
        self.viewController().navigationController?.pushViewController(c, animated: true)
        
    }
}
