//
//  MyBetC.swift
//  WaykiChain
//
//  Created by louis on 2018/5/18.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class MyBetC: NavBaseVC {

    fileprivate var line : UIView? = nil
    fileprivate lazy var titleBtnArr : [UIButton] = []
    fileprivate lazy var scrollViewArr : [UIView] = []
    fileprivate var scrollView:UIScrollView?
    fileprivate var tropMenu:ZHDropDownMenu = ZHDropDownMenu()
    ///0是全部，1是待开奖，2是中奖
    var curTag:Int = 0
    ///0，1是足球篮球，2是事件
    var viewTag : Int  = 0
    
    var all_data_arr : [[String:Any]] = []
    ///缓存一下滚动的小标
    fileprivate var oldTag : CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        addUpperBackShowImageView(imageName: "wallet_header_bgd_shortest")
        setupView()
        
        self.requestData()
    }

   
    
    
    fileprivate func setupView(){
        var headerHeight : CGFloat = ScreenWidth * 0.293

        if UIDevice.isX(){
            headerHeight += 24
        }
        
        
        scrollView = UIScrollView.init(frame: CGRect(x: 0, y: headerHeight, width: ScreenWidth, height: ScreenHeight-headerHeight))
        scrollView?.contentSize = CGSize(width: ScreenWidth * 3, height: ScreenHeight - headerHeight)
        self.view.addSubview(scrollView!)
        line = UIView.init(frame: CGRect(x: ScreenWidth/6-8, y: headerHeight-2, width: 16, height: 2))
        line?.backgroundColor = .white
        self.view.addSubview(line!)
        scrollView?.delegate = self
        scrollView?.isPagingEnabled = true
        scrollView?.bounces = false
        
        let titleArr = ["全部".local,"待开奖".local,"中奖".local]
        for i in 0...2{
            let button = UIButton.init(frame: CGRect(x: CGFloat(i) * ScreenWidth/3, y: headerHeight - 21 - 12, width: ScreenWidth/3, height: 21))
            button.setTitle(titleArr[i], for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            button.setTitleColor(UIColor.white, for: .selected)
            button.setTitleColor(UIColor.init(white: 1, alpha: 0.5), for: .normal)
            self.view.addSubview(button)
            button.tag = i
            if i == 0{
                button.isSelected = true
            }
            button.addTarget(self, action: #selector(clickTitleBtn(btn:)), for: .touchUpInside)
            titleBtnArr.append(button)
            
            let view = MyBetView.init(frame: CGRect(x: CGFloat(i) * ScreenWidth, y:0, width: ScreenWidth, height: scrollView!.height()), style: .plain)
            view.backgroundColor = .clear
            let header_refresh = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refreshData))
            header_refresh?.stateLabel.isHidden = true
            header_refresh?.lastUpdatedTimeLabel.isHidden = true
            view.mj_header = header_refresh
            
            let footer = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMoreData))
            view.mj_footer = footer
//            footer?.setTitle("我是有底线的".local, for: .noMoreData)
            footer?.isHidden = true
            footer?.stateLabel.textColor = UIColor.init(white: 1, alpha: 0.5)
            scrollView?.addSubview(view)
            view.tag = i
            scrollViewArr.append(view)
        }
        
        tropMenu.frame = CGRect(x: ScreenWidth/2-60, y: naviHeight-45, width: 120, height: 40)
        var wordsArr:[String] = []
        wordsArr = ["足球竞猜".local,"篮球竞猜".local,"事件竞猜".local]
        tropMenu.options = wordsArr
        tropMenu.defaultValue = wordsArr.first
        tropMenu.textColor = UIColor.white
        tropMenu.backgroundColor = UIColor.clear
        tropMenu.showBorder = false
        tropMenu.delegate = self
        tropMenu.font = UIFont(name: kFontType, size: 14)
        view.addSubview(tropMenu)
        
        
    }
    @objc func clickTitleBtn(btn:UIButton){
        btn.isSelected = true
        curTag = btn.tag
        for b in titleBtnArr{
            if b.tag == btn.tag{
                b.isSelected = true
            }else{
                b.isSelected = false
            }
        }
        scrollLine(tag:btn.tag)
        UIView.animate(withDuration: 0.3) {
            self.scrollView?.contentOffset.x = ScreenWidth * CGFloat(btn.tag)
        }
        requestData()
    }
    fileprivate func scrollLine(tag:Int){
        var index:CGFloat = 0
        if tag == 0{
            index = 1
        }else if tag == 1{
            index = 3
        }else if tag == 2{
            index = 5
        }
        UIView.animate(withDuration: 0.3) {
            self.line?.frame.origin.x = ScreenWidth*index/6-8
        }
        
    }

    
    
    @objc func refreshData(){
        if #available(iOS 10.0, *) {
            let imp = UIImpactFeedbackGenerator.init(style: .light)
            imp.impactOccurred()
        }
        self.loadWay = .refresh
        requestData(showHUD: false,isDropView: true)
    }
    @objc func loadMoreData(){
        self.loadWay = .loadmore
        self.curPage += 1
        requestData(showHUD: false,isDropView:  true)
    }
}
extension MyBetC:UIScrollViewDelegate,ZHDropDownMenuDelegate{
    func openOrClose(_ isopen: Bool) {
        if isopen{
            UIView.animate(withDuration: 0.3, animations: {
                self.tropMenu.cover.alpha = 1
            }) { (finish) in
                self.view.insertSubview(self.tropMenu.cover, belowSubview: self.tropMenu)
            }
            
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.tropMenu.cover.alpha = 0
            }) { (finish) in
                self.tropMenu.cover.removeFromSuperview()
            }
            
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let a = scrollView.contentOffset.x/ScreenWidth
        self.line?.frame.origin.x = ScreenWidth/3 * a + ScreenWidth/6 - 8
        for b in titleBtnArr{
            if b.tag == Int(a+0.5){
                b.isSelected = true
            }else{
                b.isSelected = false
            }
        }
        curTag = Int(a)
        if a > 0 && a < 1 || a > 1 && a < 2{
            return
        }
        if oldTag == a{
            return
        }
        self.loadWay = .refresh
        requestData()
        
        oldTag = a
    }
    
    func dropDownMenu(_ menu: ZHDropDownMenu, didEdit text: String) {
        
    }
    
    func dropDownMenu(_ menu: ZHDropDownMenu, didSelect index: Int) {
       
        let curView = scrollViewArr[curTag] as! MyBetView
        for v in scrollViewArr{
            let oldView = v as! MyBetView
            if oldView != curView{
                
                oldView.ball_dataArr.removeAll()
                oldView.event_dataArr.removeAll()
                oldView.reloadData()
            }
            
        }
        self.viewTag = index
        //refreshData()
        curView.mj_header.beginRefreshing()
    }
    
    
    

}
///获取数据
extension MyBetC{

    
    func requestData(showHUD:Bool=false,isDropView:Bool = false){
        
        if loadWay == .refresh{
            self.curPage = 1
        }
        if !isDropView{
            let curView = scrollViewArr[curTag] as! MyBetView
            
            if curView.ball_dataArr.count != 0 {
                NoDataView.shared.stop(haveDate: true)
                return
            }
            if viewTag == 2{
                if curView.event_dataArr.count != 0{
                    NoDataView.shared.stop(haveDate: true)
                    return
                }
            }
        }
        
        let address = AccountManager.getAccount().address
        let gamePath = httpPath(path: viewTag > 0 ? .篮球投注记录_G : .足球投注记录_G)
        let typePath = gamePath + "\(address)?type=\(curTag)&page=\(curPage)&rows=25"
        self.view.addSubview(NoDataView.shared)
       
        LHRequest.get(url: typePath,
                      parameters: ["":""],
                      runHUD:  showHUD ? HUDStyle.loading:HUDStyle.none,success: { [weak self] (json) in
                        self?.handleData(json: json)
        }) { (error) in
            let curView = self.scrollViewArr[self.curTag] as! MyBetView
            curView.mj_header.endRefreshing()
            NoDataView.shared.stop(msg: "网络异常，请重试".local)
        }
    }
    
    func handleData(json:JSON){

        let curView = scrollViewArr[curTag] as! MyBetView
        curView.mj_header.endRefreshing()
        if let dic = json.dictionaryObject{
            
            if let arr = dic["result"] as? [[String:Any]]{
        
                if arr.count < 25{
                    if arr.count == 0{
                        curView.mj_footer.isHidden = true
                    }else{
                        
                        curView.mj_footer.isHidden = false
                    }
                    curView.mj_footer.state = .noMoreData

                }else{
                    curView.mj_footer.isHidden = false
                    curView.mj_footer.state = .idle
                }
                if loadWay == .refresh{
                    all_data_arr.removeAll()
                    all_data_arr = arr
                }else{
                    all_data_arr += arr
                }
                
                var ball_data:[MyBetListModel] = []
                var event_data:[MyBetListModel] = []
                for dic in all_data_arr{
                    let m = MyBetListModel.init(dic)
                    
                    if m.isEvent(){
                        event_data.append(m)
                    }else{
                        ball_data.append(m)
                    }
                    
                }
                if viewTag == 2{
                    if event_data.count == 0{
                        curView.mj_footer.isHidden = true
                        NoDataView.shared.stop(msg: "没有竞猜记录".local)
                    }else{
                        NoDataView.shared.stop(haveDate: true)
                    }
                }else{
                    if ball_data.count == 0{
                        curView.mj_footer.isHidden = true
                        NoDataView.shared.stop(msg: "没有竞猜记录".local)
                    }else{
                        curView.mj_footer.isHidden = false
                        NoDataView.shared.stop(haveDate: true)
                    }
                }
                curView.ball_dataArr = ball_data
                curView.event_dataArr = event_data
                curView.viewTag = self.viewTag
                curView.reloadData()
                TableViewAnimationKit.show(with: .fall, tableView: curView)
            }
            
        }
    }
}



