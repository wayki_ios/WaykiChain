//
//  MyBetEventCell.swift
//  WaykiChain
//
//  Created by lcl on 2018/5/26.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class MyBetEventCell: FoundationCell {

    var timeL:UILabel!
    var priceL:UILabel!
    
    var leftTeamL:UILabel!
   
    var stateLabel:UILabel!
    
    class func getCell(table:UITableView,id:String="eventCell")->MyBetEventCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = MyBetEventCell() }
        return cell as! MyBetEventCell
    }
    
    override func buildCell(frame: CGRect){
        backgroundColor = UIColor.clear
        
        timeL = UILabel(frame: CGRect(x: 20*scale, y: 18*scale, width: 200, height: 18))
        timeL.textColor = UIColor.RGBHex(0xa199a2)
        timeL.font = UIFont(name: kFontTypeMedium, size: 14)
        addSubview(timeL)
        
        let bgdW = ScreenWidth-40*scale
        let bgdH = bgdW*(110/335)
        let bgdY = 45*scale
        let bgdV = UIImageView(frame: CGRect(x: 20*scale, y:bgdY , width: bgdW, height: bgdH))
        bgdV.image = UIImage(named: "bet_game_list_cell_bgd")
        bgdV.isUserInteractionEnabled = true
        addSubview(bgdV)
        
        
        let titley:CGFloat = 12/110 * bgdH
        let titleW:CGFloat = bgdW - 20 //110*scale-5
        
        leftTeamL = UILabel(frame: CGRect(x: 10, y: titley, width: titleW, height: 63 ))
        if UIDevice.model() == "5"{
            leftTeamL.font = UIFont.init(name: kFontBold, size: 12)
        }else{
            leftTeamL.font = UIFont.init(name: kFontBold, size: 13)
        }
        leftTeamL.numberOfLines = 3
        leftTeamL.textColor = UIColor.RGBHex(0x312B2C)
        leftTeamL.textAlignment = .left
        bgdV.addSubview(leftTeamL)
        
        let footH:CGFloat = bgdH * 0.321
        
        
        priceL = UILabel(frame: CGRect(x: 12 , y: bgdH - footH, width: bgdW*2/3-25, height: footH))
        priceL.font = UIFont.systemFont(ofSize: 11)
        priceL.textColor = UIColor.RGBHex(0xEF3F50)
        bgdV.addSubview(priceL)
        
        stateLabel = UILabel(frame: CGRect(x: bgdW*2/3 - 20 , y: priceL.top(), width: bgdW*1/3 + 10, height: footH))
        if UIDevice.model() == "5"{
            stateLabel.font = UIFont.init(name: kFontBold, size: 12)
        }else{
            stateLabel.font = UIFont.init(name: kFontBold, size: 13)
        }
        stateLabel.textAlignment = .right
        bgdV.addSubview(stateLabel)
        
    }
    
}

//MARK: - 配置数据
extension MyBetEventCell{
    
    func configureData(model:MyBetListModel) -> Self {
        timeL.text = Date(timeIntervalSince1970: TimeInterval(model.bettime/1000)).dateString()
        leftTeamL.text = model.league
        
        let touzhu = NSMutableAttributedString(string: "投注金额 \(model.betmoney) \(model.csymbol)")
        touzhu.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0x8E8A8C),NSAttributedStringKey.font : UIFont.systemFont(ofSize: 10)], range: NSRange.init(location: 0, length: 4))
        priceL.attributedText = touzhu
       
        //(1.待确认,2.未成功,3.未开奖,4.中奖,5.未中奖,6.已关闭)
        switch model.betstatus {
        
        case 1:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "待确认".local + " >"
            break
        case 2:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "未成功".local + " >"
            break
        case 3:
            stateLabel.textColor = UIColor.RGBHex(0xEF3F50)
            stateLabel.text = "待开奖".local + " >"
            break
        case 4:
            stateLabel.text = "中奖".local + " +\(model.bonusmoney) >"
            stateLabel.textColor = UIColor.RGBHex(0x379062)
            break
        case 5:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "未中奖".local + " >"
            break
        case 6:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "已关闭".local + " >"
            break
            
        default:
            break
        }
        
        
       
        return self
    }
}

