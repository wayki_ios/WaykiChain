//
//  MyBetBallCellTableViewCell.swift
//  WaykiChain
//
//  Created by 1 on 2018/5/25.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class MyBetBallCellTableViewCell: FoundationCell {

    var timeL:UILabel!
    var priceL:UILabel!
    var rightTeamL:UILabel!
    var leftTeamL:UILabel!
    var rightTeamIV:UIImageView?
    var leftTeamIV:UIImageView?
    var playStyle:UILabel!
    var stateLabel:UILabel!
    
    class func getCell(table:UITableView,id:String="ballCell")->MyBetBallCellTableViewCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = MyBetBallCellTableViewCell() }
        return cell as! MyBetBallCellTableViewCell
    }
    
    override func buildCell(frame: CGRect){
        backgroundColor = UIColor.clear
        
        timeL = UILabel(frame: CGRect(x: 20*scale, y: 18*scale, width: 200, height: 18))
        timeL.textColor = UIColor.white
        timeL.alpha = 0.5
        
        timeL.font = UIFont(name: kFontType, size: 12)
        addSubview(timeL)
        
        let bgdW = ScreenWidth-40*scale
        let bgdH = bgdW*(110/335)
        let bgdY = 45*scale
        let bgdV = UIImageView(frame: CGRect(x: 20*scale, y:bgdY , width: bgdW, height: bgdH))
        bgdV.image = UIImage(named: "bet_record_cell_bgd")
        bgdV.isUserInteractionEnabled = true
        addSubview(bgdV)
        
        
        let titley:CGFloat = 20/110 * bgdH
        let titleW:CGFloat = bgdW/3 //110*scale-5
        
        leftTeamL = UILabel(frame: CGRect(x: 0, y: titley, width: titleW, height: 20))
        leftTeamL.font = UIFont(name: kFontBold, size: 14)
        leftTeamL.textColor = UIColor.RGBHex(0x312B2C)
        leftTeamL.textAlignment = .center
        bgdV.addSubview(leftTeamL)
        
        rightTeamL = UILabel(frame: CGRect(x: bgdW-titleW , y: titley, width: titleW, height: 20))
        rightTeamL.font = UIFont(name: kFontTypeMedium, size: 14)
        rightTeamL.textAlignment = .center
        rightTeamL.textColor = UIColor.RGBHex(0x312B2C)
        bgdV.addSubview(rightTeamL)
        
        // 图片
        let imageY:CGFloat = 18/110 * bgdH
        leftTeamIV = UIImageView(frame: CGRect(x: leftTeamL.right(), y: imageY, width: 25/110 * bgdH, height: 25/110 * bgdH))
        bgdV.addSubview(leftTeamIV!)
        
        rightTeamIV = UIImageView(frame: CGRect(x: rightTeamL.left()-25/110 * bgdH, y: imageY, width: 25/110 * bgdH, height: 25/110 * bgdH))
        bgdV.addSubview(rightTeamIV!)
        
//        if UIDevice.model() == "5" {
//            let titleW:CGFloat = (bgdW-50)/2
//            rightTeamL.frame =  CGRect(x: 0, y: 0, width: titleW, height: 20)
//            leftTeamL.frame =  CGRect(x: bgdW-titleW , y: 0, width: titleW, height: 20)
//            leftTeamIV?.isHidden = true
//            rightTeamIV?.isHidden = true
//        }
        
        let centerH:CGFloat = 12*scale
        let centerW:CGFloat = 19*scale
        let centerY:CGFloat = 24/110 * bgdH
        let centerIV = UIImageView(frame: CGRect(x: bgdW/2-centerW/2, y:centerY , width: centerW, height: centerH))
        centerIV.image = UIImage(named: "game_list_vs")
        bgdV.addSubview(centerIV)
        
        let footY:CGFloat = bgdH/2 + 10
        let footH:CGFloat = 20
        
        playStyle = UILabel(frame: CGRect(x:12 , y: footY, width: bgdW/2, height: footH))
        playStyle.font = UIFont.systemFont(ofSize: 10)
        playStyle.textColor = UIColor.black
        bgdV.addSubview(playStyle)
        
        priceL = UILabel(frame: CGRect(x: 12 , y: playStyle.bottom(), width: bgdW*2/3-25, height: footH))
        priceL.font = UIFont.systemFont(ofSize: 10)
        priceL.textColor = UIColor.RGBHex(0xEF3F50)
        bgdV.addSubview(priceL)
        
        stateLabel = UILabel(frame: CGRect(x: bgdW*2/3 - 10 , y: playStyle.bottom()-3, width: bgdW*1/3 + 5, height: footH))
        if UIDevice.model() == "5"{
            stateLabel.font = UIFont.init(name: kFontBold, size: 12)
        }else{
            stateLabel.font = UIFont.init(name: kFontBold, size: 13)
        }
        stateLabel.textAlignment = .right
        stateLabel.textColor = UIColor.RGBHex(0x379062)
        bgdV.addSubview(stateLabel)
        
    }
    
}

//MARK: - 配置数据
extension MyBetBallCellTableViewCell{
    
    func configureData(model:MyBetListModel,viewTag:Int) -> Self {
        timeL.text = Date(timeIntervalSince1970: TimeInterval(model.bettime/1000)).dateString()
        let arr = model.lmatch.components(separatedBy: "VS")
        leftTeamL.text = arr.first
        rightTeamL.text = arr.last
        rightTeamIV?.image = UIImage(named: viewTag == 0 ? "game_type_foot_selected" : "game_type_basket_selected")
        leftTeamIV?.image = UIImage(named: viewTag == 0 ? "game_type_foot_selected" : "game_type_basket_selected")
        let str1 = "竞猜玩法".local
        let str2 = "投注金额".local

        let attrPalyWays = NSMutableAttributedString(string: str1 + " \(model.bettype)")
        attrPalyWays.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0x8E8A8C)], range: NSRange.init(location: 0, length: str1.count))
        playStyle.attributedText = attrPalyWays
        let touzhu = NSMutableAttributedString(string: str2+" \(model.betmoney) \(model.csymbol)")
        touzhu.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.RGBHex(0x8E8A8C)], range: NSRange.init(location: 0, length: str2.count))
        priceL.attributedText = touzhu
        //(1.待确认,2.未成功,3.未开奖,4.中奖,5.未中奖,6.已关闭)
        switch model.betstatus {
            
        case 1:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "待确认".local + " >"
            break
        case 2:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "未成功".local + " >"
            break
        case 3:
            stateLabel.textColor = UIColor.RGBHex(0xEF3F50)
            stateLabel.text = "待开奖".local + " >"
            break
        case 4:
            stateLabel.text = "中奖".local+" +\(model.bonusmoney) >"
            stateLabel.textColor = UIColor.RGBHex(0x379062)
            break
        case 5:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "未中奖".local + " >"
            break
        case 6:
            stateLabel.textColor = UIColor.RGBHex(0x908C8C)
            stateLabel.text = "已关闭".local + " >"
            break
            
        default:
            break
        }
        
        return self
    }
}
