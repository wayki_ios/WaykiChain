

import UIKit

fileprivate let headerCellHeight:CGFloat = (1-25/375)*(ScreenWidth)/2+50
fileprivate let contentCellheight:CGFloat = 92//88/668*(ScreenHeight)
class MyAssertC: NavBaseVC {

    
    ///WICC 还是 SPC
    var wicc_spc : String = "WICC"
    
    private var headerCell:MyAssertHeaderCell!
    private var tableV:UITableView!
    private var data:[MyAssertCellModel] = []
    var transferBtn:UIButton?
    var receiveBtn:UIButton?
    
    ///筛选index
    private var selectedTypeIndex : Int = 0
    
    fileprivate lazy var tropMenu = ZHDropDownMenu()
    fileprivate lazy var bgdV = UIView()
    fileprivate lazy var wordsArrWicc:[String] = []
    fileprivate lazy var wordsArrSpc:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        创建界面()
        getWealthSum()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if wicc_spc == "WICC"{
            refresh()
        }else{
            NotificationCenter.default.post(name:NSNotification.Name.init("spc"), object: nil)
            self.didScrolledWithIndex(index: 1)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NoDataView.shared.removeFromSuperview()
    }

}


// MARK:-  创建界面 -- 界面事件
extension MyAssertC{
    func 创建界面(){
        titleLabel?.text = "交易记录".local
        showImageView?.isHidden = true
        header?.alpha = 0.8
        
        tableV = UITableView(frame: CGRect(x: 0, y: naviHeight, width: ScreenWidth, height: ScreenHeight-naviHeight - 92 * scale + 10))
        tableV.delegate = self
        tableV.dataSource = self
        tableV.separatorStyle = .none
        tableV.backgroundColor = UIColor.clear
        tableV.showsVerticalScrollIndicator = false
        tableV.separatorColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.2)
        bgdView.addSubview(tableV)
        tableV.adaptiOS11(table: tableV)
        
        let refresh_header = MJRefreshNormalHeader.init(refreshingTarget: self, refreshingAction: #selector(refresh))
        refresh_header?.stateLabel.isHidden = true
        refresh_header?.lastUpdatedTimeLabel.isHidden = true
        tableV.mj_header = refresh_header
        headerCell = MyAssertHeaderCell()
        
        
        let footer = MJRefreshAutoNormalFooter.init(refreshingTarget: self, refreshingAction: #selector(loadMore))
        footer?.stateLabel.textColor = UIColor.init(white: 1, alpha: 0.5)
        tableV.mj_footer = footer
        
        let bgdH:CGFloat =  92*scale
        bgdV.frame = CGRect(x: 0, y: ScreenHeight-bgdH, width: ScreenWidth, height:bgdH)
        bgdV.backgroundColor = UIColor.clear
        view.addSubview(bgdV)
        
        let buttonY:CGFloat = 24*scale
        let buttonW:CGFloat = 157*scale
        let buttonH:CGFloat = 44*scale
        
        let receiveButton = UIButton(frame: CGRect(x: 24*scale, y: buttonY, width:buttonW , height:buttonH))
        receiveButton.backgroundColor = UIColor.white
        receiveButton.layer.cornerRadius = 5
        receiveButton.setTitleColor(UIColor.RGBHex(0xbc334d), for: .normal)
        receiveButton.setTitleColor(UIColor.RGBHex(0xbc334d), for: .selected)
        receiveButton.setTitle("收款".local, for: .normal)
        receiveButton.addTarget(self, action: #selector(跳转到二维码), for: .touchDown)
        bgdV.addSubview(receiveButton)
        receiveBtn = receiveButton
        
        let transferButton = UIButton(frame: CGRect(x: ScreenWidth-181*scale, y: buttonY, width: buttonW, height: buttonH))
        transferButton.layer.cornerRadius = 5
        transferButton.setTitleColor(UIColor.white, for: .normal)
        transferButton.setTitleColor(UIColor.white, for: .selected)
        transferButton.setBackgroundImage(UIImage(named: "assert_transfer_bgd"), for: .normal)
        transferButton.setBackgroundImage(UIImage(named: "assert_transfer_bgd"), for: .highlighted)
        transferButton.setTitle("转账".local, for: .normal)
        transferButton.addTarget(self, action: #selector(跳转到转账), for: .touchDown)
        bgdV.addSubview(transferButton)
        transferBtn = transferButton
        changeBButtonStatus()
        
        
        wordsArrWicc = ["全部".local,"转账".local,"收款".local,"激活".local,"锁仓".local]
        wordsArrSpc = ["全部".local,"派奖".local,"收款".local,"投注".local]
        tropMenu.frame = CGRect(x: ScreenWidth-120, y: naviHeight-45, width: 110, height: 40)
        tropMenu.options = wordsArrWicc
        tropMenu.defaultValue = wordsArrWicc.first
        tropMenu.textColor = UIColor.white
        tropMenu.backgroundColor = UIColor.clear
        tropMenu.showBorder = false
        tropMenu.delegate = self
        tropMenu.font = UIFont.systemFont(ofSize: 15)
        view.addSubview(tropMenu)
        
        
        
    }
    
    func 列表事件(int:Int){
        let c = WalletRecordDetaiInfolVC()
        c.listModel = self.data[int]
        navigationController?.pushViewController(c, animated: true)
    }
    
    @objc func 跳转到二维码(){
        if wicc_spc == "WICC"{
            let c = MyQRcodeC()
            navigationController?.pushViewController(c, animated: true)
        }else{
            //兑换wkd
            let c = EX_MainC()
            navigationController?.pushViewController(c, animated: true)
        }
     
    }
    
    @objc func 跳转到转账(){
        if wicc_spc == "WICC"{
            let c = MyTransferC()
            navigationController?.pushViewController(c, animated: true)
        }else{
            //抽奖
           NavigationManager.shared.handleLeftAction(type: .lucky)
        }
    }
}

extension MyAssertC{
    //切换改变按钮状态
    func changeBButtonStatus(){
        if wicc_spc != "WICC" {
            receiveBtn?.setTitle("兑换".local+coinName, for: .normal)
            transferBtn?.setTitle("幸运抽奖".local, for: .normal)
            
        }else{
            receiveBtn?.setTitle("收款".local, for: .normal)
            transferBtn?.setTitle("转账".local, for: .normal)

        }
    }
}


// MARK:- 列表代理
extension MyAssertC:UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,ZHDropDownMenuDelegate{
    func openOrClose(_ isopen: Bool) {
        if isopen{
            UIView.animate(withDuration: 0.3, animations: {
                self.tropMenu.cover.alpha = 1
            }) { (finish) in
                self.view.insertSubview(self.tropMenu.cover, belowSubview: self.tropMenu)
            }
            
        }else{
            UIView.animate(withDuration: 0.3, animations: {
                self.tropMenu.cover.alpha = 0
            }) { (finish) in
                self.tropMenu.cover.removeFromSuperview()
            }
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            headerCell.block = { index in
                self.didScrolledWithIndex(index: index)
            }
            return headerCell
        }else{
            return MyAssertContentCell.getCell(table: tableView).configureData(model: data[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return headerCellHeight
        }else{
            return contentCellheight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            列表事件(int: indexPath.row)
        }
        if self.selectedTypeIndex == 4{
            self.selectedTypeIndex = 1
        }
    }
    
    func dropDownMenu(_ menu: ZHDropDownMenu, didEdit text: String) {
        
    }
    
    ///下拉刷新
    @objc func refresh(){
        
        if #available(iOS 10.0, *) {
            let imp = UIImpactFeedbackGenerator.init(style: .light)
            imp.impactOccurred()
        } 
        
        
        self.loadWay = .refresh
        getRecordsList(type:self.selectedTypeIndex,symbol: self.wicc_spc)
        getWealthSum()
    }
    @objc func loadMore(){
        self.loadWay = .loadmore
        self.curPage += 1
        getRecordsList(type:self.selectedTypeIndex,symbol: self.wicc_spc)
    }
    ///点击筛选按钮
    func dropDownMenu(_ menu: ZHDropDownMenu, didSelect index: Int) {
        
        if self.wicc_spc == coinName{
            if index == 1{
                self.selectedTypeIndex = 4
            }else{
                self.selectedTypeIndex = index
            }
            
        }else{
            if index == 4{
                self.selectedTypeIndex = 5
            }else{
                self.selectedTypeIndex = index
            }
            
        }
        
        refresh()
        
    }
    ///滑动卡片
    func didScrolledWithIndex(index:Int){
        tableV.reloadSections([1], with: .fade)
        self.wicc_spc = index == 0 ? "WICC" : coinName
        tropMenu.options = index == 0 ? wordsArrWicc : wordsArrSpc
        
        if index == 1{
            if selectedTypeIndex == 5{
                tropMenu.contentTextField.text = wordsArrSpc[0]
                selectedTypeIndex = 0
                tableV.mj_header.beginRefreshing()
                return
            }
            tropMenu.contentTextField.text = wordsArrSpc[selectedTypeIndex]
            if self.selectedTypeIndex == 1{
                self.selectedTypeIndex = 4
            }
        }else{
            if self.selectedTypeIndex == 4{
                self.selectedTypeIndex = 1
            }
            tropMenu.contentTextField.text = wordsArrWicc[selectedTypeIndex]
        }
        changeBButtonStatus()
        tableV.mj_header.beginRefreshing()
    }
    
    // 资产汇总
    func getWealthSum(){
        
        let account = AccountManager.getAccount()
        let path:String = httpPath(path: HTTPPath.获取资产汇总_G)
        let address = account.address
        
        
        let mHash = account.mHash
        let requestPath = path + mHash + "/\(address)"
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        
        LHRequest.get(url: getPath,parameters: [:],runHUD: .none, success: {[weak self] (json) in
            self?.tableV.mj_header.endRefreshing()
            if let dic = json.dictionary{
                if let status = dic["status"]?.int{
                    if status == 1{
                        if let arr = dic["result"]?.array{
                            self?.headerCell.configureData(result: arr)
                        }
                    }
                }
            }
        }) { (error) in }
    }
    
    ///获取列表数据
    func getRecordsList(type:Int,symbol:String = "WICC"){
        

        
        
        
        if self.loadWay == .refresh{
            self.curPage = 1
        }
        
        let path:String = httpPath(path: HTTPPath.交易记录_P)
        
        let address = AccountManager.getAccount().address
        let requestPath = path + "\(address)"
 
        var pa:[String:Any] = [:]
        
        pa["symbol"] = symbol
        pa["page"] = curPage
        pa["rows"] = 15
        
        pa["type"] = type
        
        
        
        tableV.addSubview(NoDataView.shared)
        LHRequest.post(url: requestPath, parameters: pa,runHUD:.none , success: { [weak self](json) in
            
            let dataArr = MyAssertCellModel.getModels(json: json)
            
            if self?.loadWay == .refresh{
                self?.data.removeAll()
                self?.data = dataArr
            }else{
                self?.data += dataArr
            }
            
            if dataArr.count < 15 {
                if self?.data.count == 0{
                    self?.tableV.mj_footer.isHidden = true
                }else{
                    self?.tableV.mj_footer.isHidden = false
                }
                self?.tableV.mj_footer.state = .noMoreData
                
            }else{
                self?.tableV.mj_footer.isHidden = false
                self?.tableV.mj_footer.state = .idle
            }
            
            
            self?.tableV.mj_header.endRefreshing()
            
            
            if self?.data.count == 0{
                NoDataView.shared.stop()
            }else{
                NoDataView.shared.stop(haveDate: true)
            }
//            self?.tableV.reloadSections([1], with: .fade)
            self?.tableV.reloadData()
            
        }) { [weak self]() in
            self?.tableV.mj_footer.isHidden = true
            self?.tableV.mj_header.endRefreshing()
            NoDataView.shared.stop(msg: "网络异常，请重试".local)
        }
        
    }

}


// MARK:- 数据模型
class MyAssertCellModel: NSObject {
    var confirmedtime:Int = 1 //确认时间
    var symbol:String = ""//币符号，例如：WICC,SPC
    var txaddress:String = ""//交易地址（本地址）
    var endaddress:String = ""//交易对端地址
    var txstatus:Int = 1 //1.已确定 2.确认中 3.确认失败
    var txid:String = ""// 交易hash
    var txmoney:Double = 0//交易金额
    var txtime:String = ""//交易时间 ,
    var txtype:Int = 0//(11：转出,12：接收,13：激活,21：合约转出,22：合约转入,23：投注,24：派奖)
    
    class func getModels(json:JSON)->[MyAssertCellModel]{
        var models:[MyAssertCellModel] = []
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{if status != 1 {return models }}
            if let result = dic["result"]?.array{
                for lo in result {
                    let model = MyAssertCellModel()
                    model.analysisData(json:lo)
                    models.append(model)
                }
            }
        }
        return models
    }
    
    func analysisData(json:JSON){
        if let val = json["confirmedtime"].int{ confirmedtime = val }
        if let val = json["txstatus"].int{ txstatus = val }
        if let val = json["symbol"].string{ symbol = val }
        if let val = json["txaddress"].string{ txaddress = val }
        if let val = json["endaddress"].string{ endaddress = val }
        if let val = json["txid"].string{ txid = val }
        if let val = json["txmoney"].double{ txmoney = val }
        if let val = json["txtime"].string{
            txtime = val
        }else if (json["txtime"].double != nil){
            txtime = String(format: "%f", json["txtime"].double!)
        }
        if let val = json["txtype"].int{ txtype = val }
    }
    
    //获取中文类型
    class func  getChineseTypeWithType(_ type:Int) ->String{
        var chineseType:String!
        switch type {
        case 11:
            chineseType="转账".local
            break
        case 12:
            chineseType="收款".local
            break
        case 13:
            chineseType="激活".local
            break
        case 15:
             chineseType="锁仓".local
        case 16:
            chineseType="解仓".local
        case 21:
            chineseType="兑奖".local//"合约转出"
            break
        case 22:
            chineseType="收款".local//"合约转入"
            break
        case 23:
            chineseType="投注".local
            break
        case 24:
            chineseType="派奖".local
            break
        default:
            chineseType = ""
        }
        return chineseType
    }
}


// MARK:- 表头
class MyAssertHeaderCell:FoundationCell,UIScrollViewDelegate{
    
    private var scrollV:UIScrollView!
    private var leftIV:UIImageView!
    private var rightIV:UIImageView!
    private var cPage:Int = 0
    
    private var wiccCountL:UILabel!
    private var wiccPriceL:UILabel!
    private var wiccTotalL:UILabel!
    
    private var spcCountL:UILabel!
    private var spcPriceL:UILabel!
    private var spcTotalL:UILabel!
    
    var oldIndex:CGFloat = 0
    var block:((_ tag:Int)->Void)? = nil
    
    
    class func getCell(table:UITableView,id:String="MyAssertHeaderCell")->MyAssertHeaderCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = MyAssertHeaderCell() }
        return cell as! MyAssertHeaderCell
    }
    
    private func currentPage(page:Int){
        if let b = block{
            b(page)
        }
        if page == 0{
            UIView.animate(withDuration: 0.2) {
                self.leftIV.alpha = 1
                self.rightIV.alpha = 0
            }
        }
        if page == 1{
            UIView.animate(withDuration: 0.2) {
                self.leftIV.alpha = 0
                self.rightIV.alpha = 1
            }
        }
        cPage = page
    }
    
    override func buildCell(frame: CGRect) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoSpc), name: NSNotification.Name.init("spc"), object: nil)
        
        backgroundColor = UIColor.clear
        let cellH:CGFloat = headerCellHeight
        
        let scrollH:CGFloat = cellH-30
        scrollV = UIScrollView(frame: CGRect(x: 0, y: 15, width: ScreenWidth, height: scrollH))
        scrollV.delegate = self
        scrollV.isPagingEnabled = true
        scrollV.bounces = false
        scrollV.showsHorizontalScrollIndicator = false
        scrollV.contentSize = CGSize(width: ScreenWidth*2, height: scrollH)
        addSubview(scrollV)
        
        let buttonW:CGFloat = 325*scale
        let buttonH:CGFloat = buttonW/2
        
        // WICC
        let wiccV = UIView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: scrollH))
        wiccV.backgroundColor = UIColor.clear
        scrollV.addSubview(wiccV)
        
        let wiccBgdIV = UIImageView(frame: CGRect(x: 25*scale, y: 0, width: buttonW, height: buttonH))
        wiccBgdIV.image = UIImage(named: "assert_wicc")
        wiccV.addSubview(wiccBgdIV)
        
        wiccCountL = UILabel(frame: CGRect(x: 42*scale, y: 80*scale, width: 300*scale , height: 28*scale))
        wiccCountL.font = UIFont(name: "Helvetica-Bold", size: 26*scale)
        wiccCountL.textColor = UIColor.white
        wiccV.addSubview(wiccCountL)
        
        wiccPriceL = UILabel(frame: CGRect(x: 42*scale, y: 120*scale, width: 80*scale , height: 13*scale))
        wiccPriceL.font = UIFont(name: kFontTypeThin, size: 13*scale)
        wiccPriceL.textColor = UIColor.white
        wiccV.addSubview(wiccPriceL)
       
        wiccTotalL = UILabel(frame: CGRect(x: 160*scale, y: 120*scale, width: 170*scale , height: 13*scale))
        wiccTotalL.font = UIFont(name: kFontTypeThin, size: 13*scale)
        wiccTotalL.textColor = UIColor.white
        wiccTotalL.textAlignment = .right
        wiccV.addSubview(wiccTotalL)
        
        
        // SPC
        let spcV = UIView(frame: CGRect(x: ScreenWidth, y: 0, width: ScreenWidth, height: scrollH))
        spcV.backgroundColor = UIColor.clear
        scrollV.addSubview(spcV)
        
        let spcBgdIV = UIImageView(frame: CGRect(x: 25*scale, y: 0, width: buttonW, height: buttonH))
        spcBgdIV.image = UIImage(named: "assert_wkd")
        spcV.addSubview(spcBgdIV)
        
        spcCountL = UILabel(frame: CGRect(x: 42*scale, y: 80*scale, width: 300*scale , height: 28*scale))
        spcCountL.font = UIFont(name: "Helvetica-Bold", size: 26*scale)
        spcCountL.textColor = UIColor.white
        spcV.addSubview(spcCountL)
        
        spcPriceL = UILabel(frame: CGRect(x: 42*scale, y: 120*scale, width: 80*scale , height: 13*scale))
        spcPriceL.font = UIFont(name: kFontTypeThin, size: 13*scale)
        spcPriceL.textColor = UIColor.white
        spcV.addSubview(spcPriceL)
        
        spcTotalL = UILabel(frame: CGRect(x: 160*scale, y: 120*scale, width: 170*scale , height: 13*scale))
        spcTotalL.font = UIFont(name: kFontTypeThin, size: 13*scale)
        spcTotalL.textColor = UIColor.white
        spcTotalL.textAlignment = .right
        spcV.addSubview(spcTotalL)
        
        rightIV = UIImageView(frame: CGRect(x: (ScreenWidth-30)/2, y: scrollH+15, width: 30, height: 3))
        rightIV.image = UIImage(named: "assert_exchang_right")
        rightIV.alpha = 0
        addSubview(rightIV)
        
        leftIV = UIImageView(frame: CGRect(x: (ScreenWidth-30)/2, y: scrollH+15, width: 30, height: 3))
        leftIV.image = UIImage(named: "assert_exchang_left")
        addSubview(leftIV)
        
    }
    @objc func gotoSpc(){
        scrollV.scrollRectToVisible(CGRect(x: ScreenWidth, y: 0, width: ScreenWidth, height: headerCellHeight), animated: true)
    }
    func configureData(result:[JSON]) {
        if let firstDic = result.first?.dictionary{
            var num:Float = 0
            var price:Float = 0
            if let amount = firstDic["amount"]?.float{
                num = amount
                AccountManager.getAccount().wiccSumAmount = (firstDic["amount"]?.double)!
            }
            if let p = firstDic["price"]?.float{
                price = p
                
            }
            
            wiccTotalL.text = String(format: "≈￥ %.2f",num * price)
            wiccCountL.text = String(format: "%.8f",num)
            wiccPriceL.text = String(format: "￥ %.2f", price)
        }
        if let lastDic = result.last?.dictionary{
            if let amount = lastDic["amount"]?.float{
                AccountManager.getAccount().spcSumAmount = (lastDic["amount"]?.double)!
                spcCountL.text = String(format: "%.8f",amount)
            }
        }
        
        spcPriceL.text = ""
        spcTotalL.text = ""
        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let curPage = scrollView.contentOffset.x/ScreenWidth
        
        if curPage > 0 && curPage < 1{
            return
        }
        if curPage == oldIndex{
            return
        }
        currentPage(page: Int(curPage))
        oldIndex = curPage
        
    }
    
    
    

        
}

// MARK:- 交易记录
class MyAssertContentCell: FoundationCell {
    private let scale:CGFloat = ScreenWidth/335
    private let greenColor:UIColor = UIColor.RGBHex(0x379062)
    private let redColor:UIColor = UIColor.RGBHex(0xef3f50)
    private var addressL:UILabel!
    private var typeL:UILabel!
    private var timeL:UILabel!
    private var statusL:UILabel!
    private var countL:UILabel!
    
    
   
    
    class func getCell(table:UITableView,id:String="MyAssertContentCell")->MyAssertContentCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = MyAssertContentCell() }
        return cell as! MyAssertContentCell
    }
    
    override func buildCell(frame: CGRect) {
        backgroundColor = UIColor.clear
        
        addressL = UILabel(frame: CGRect(x: 20*scale, y: 20*scale, width: 214*scale , height: 17*scale))
        addressL.font = UIFont(name: "Helvetica", size: 16*scale)
        addressL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        addSubview(addressL)
        
        countL = UILabel(frame: CGRect(x: ScreenWidth-110*scale - 20*scale, y: 20*scale, width: 110*scale , height: 17*scale))
        countL.font = UIFont(name: kFontTypeThin, size: 13*scale)
        countL.textColor = UIColor.white
        countL.textAlignment = .right
        addSubview(countL)
        
        typeL = UILabel(frame: CGRect(x: 20*scale, y: addressL.y()+addressL.height()+10, width: 27*scale, height: 16*scale))
        typeL.font = UIFont(name: kFontTypeThin, size: 11*scale)
        typeL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        typeL.backgroundColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.1)
        typeL.layer.cornerRadius = 5
        typeL.clipsToBounds = true
        typeL.textAlignment = .center
        addSubview(typeL)

        timeL = UILabel(frame: CGRect(x: typeL.x()+typeL.width()+5, y: typeL.y(), width: 105*scale, height: 16*scale))
        timeL.font = UIFont(name: "Helvetica-Light", size: 11*scale)
        timeL.textColor = UIColor.RGB(r: 255, g: 255, b:255 , alpha: 0.5)
        timeL.backgroundColor = UIColor.clear
        addSubview(timeL)
        
        statusL = UILabel(frame: CGRect(x: 240*scale, y: typeL.y(), width: 80*scale , height: 17*scale))
        statusL.font = UIFont(name: kFontTypeThin, size: 11*scale)
        statusL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.7)
        statusL.textAlignment = .right
        addSubview(statusL)
        
        let line = UIView.init(frame: CGRect(x: addressL.left(), y: contentCellheight-1, width: ScreenWidth - addressL.left()*2, height: 0.5))
        line.backgroundColor = UIColor.init(white: 1, alpha: 0.5)
        line.alpha = 0.5
        addSubview(line)
    }
    
    func configureData(model:MyAssertCellModel)-> Self {
        
        var showaddress:String! = ""
        showaddress = model.endaddress
        if showaddress == "" {
            showaddress = model.txaddress
        }
        let firstFour = showaddress.subString(to: 5)
        let lastFour = showaddress.subString(from: 29)
        
        addressL.text = firstFour + "*****" + lastFour
       
        if model.txmoney>0{
            countL?.text  =  "+" + String.init(format: "%.8f",model.txmoney)
            countL?.textColor = UIColor.green_deep()
            if model.symbol == coinName{
                countL?.text = "+" + String.init(format: "%.2f",model.txmoney)
            }
        }else if model.txmoney == 0{
            countL?.text = "-0"
            countL?.textColor = UIColor.red
        }else{
            countL?.textColor = UIColor.red
            countL?.text = String.init(format: "%.8f",model.txmoney)
            if model.symbol == coinName {
                countL?.text = String.init(format: "%.2f",model.txmoney)
            }
        }

        
        if model.txstatus == 1 {
            statusL?.text = ""
        }else if model.txstatus == 2{
            statusL?.text = NSLocalizedString("确认中", comment: "")
        }else{
            statusL?.text = NSLocalizedString("交易失败", comment: "")
        }
        typeL.text = MyAssertCellModel.getChineseTypeWithType(model.txtype )
        timeL.text = Date(timeIntervalSince1970: TimeInterval(Double(model.txtime)!/1000)).dateString()
        return self
    }
    
}

