//
//  MyTransferTokenC.swift
//  Wayki
//
//  Created by louis on 2018/6/28.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit
import AVFoundation

class MyTransferTokenC: NavBaseVC {
    
    ///额度
    fileprivate var moneyLabel:UILabel?
    ///小费
    fileprivate var feeLabel:UILabel?
    ///钱包地址
    fileprivate var walletAddressTf:UITextField?
    ///转出金额
    fileprivate var moneyTf:UITextField?
    ///备注
    fileprivate var msgTf:CLMyTextView?
    
    ///输入密码
    fileprivate var contentV : InputPwdView? = nil
    
    ///临时金额
    fileprivate var tempMoney:Double = 0
    
    fileprivate let slider = UISlider.init()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel?.text = coinName+"转账".local
        addUpperBackShowImageView(imageName: "wallet_header_bgd_short")
        layoutUI()
        getWealthSum()
        
    }
    
    func checkIsActivity(){
        DispatchQueue.main.asyncAfter(deadline:DispatchTime.now() + 3) {
            let account = AccountManager.getAccount()
            if account.regId.count == 0{
                let alertView = UIAlertController.init(title: "提示", message: "系统检测到您尚未激活钱包，请先激活", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction.init(title: "激活", style: .default, handler: {[weak self] (action) in
                    AlertInputPwdView.shared.show().sureBlock = {[weak self] pwd in
                        //先获取高度，再激活钱包
                        LHRequest.getVaildHeight { (height, address, appid) in
                            self?.actrivateWallet(height,pwd: pwd)
                        }
                    }
                })
                alertView.addAction(ok)
                self.present(alertView, animated: true, completion: nil)
            }
        }
    }
    
    fileprivate func layoutUI(){
        let widthScale = ScreenWidth/375
        let heightScale = ScreenHeight/667
        var x = 24 * widthScale
        
        let eduLabel = UILabel.init(frame: CGRect(x: x, y: 79*heightScale, width: 70, height: 18*widthScale))
        eduLabel.text = "可用额度:".local
        eduLabel.textColor = .white
        eduLabel.font = UIFont.init(name: kFontTypeMedium, size: 13)
        view.addSubview(eduLabel)
        
        moneyLabel = UILabel.init(frame: CGRect(x: eduLabel.right(), y: 79*heightScale, width: ScreenWidth - 2*x - eduLabel.width(), height: 18*widthScale))
        moneyLabel?.text = "0"
        moneyLabel?.textColor = .white
        moneyLabel?.font = UIFont.init(name: kFontTypeMedium, size: 13)
        view.addSubview(moneyLabel!)
        
        let msgLabel = UILabel.init(frame: CGRect(x: x, y: moneyLabel!.bottom()+10*heightScale, width: ScreenWidth - x, height: 18*widthScale))
        msgLabel.text = "转账"+coinName+"时，需要消耗WICC".local
        msgLabel.textColor = UIColor.init(white: 1, alpha: 0.8)
        msgLabel.font = UIFont.init(name: kFontTypeThin, size: 13)
        view.addSubview(msgLabel)
        
        
        let contentView = setupContentView(frame: CGRect(x: x, y: msgLabel.bottom()+20*heightScale, width: ScreenWidth-48, height: 210/327 * (ScreenWidth-2*x)),heightScale: heightScale,widthScale: widthScale)
        view.addSubview(contentView)
        
        
        x = 40 * widthScale
        feeLabel = UILabel.init(frame: CGRect(x: x, y: contentView.bottom() + 24*heightScale, width: ScreenWidth - 2*x, height: 18*widthScale))
        feeLabel?.textColor = UIColor.RGBHex(0xA69A9B)
        let attrStr = NSMutableAttributedString.init(string: "小费:".local+" 0.01 WICC")
        attrStr.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], range: NSRange.init(location: 0, length: 3))
        feeLabel?.attributedText = attrStr
        feeLabel?.font = UIFont.init(name: "Helvetica", size: 12)
        view.addSubview(feeLabel!)
        
        slider.frame = CGRect(x: x-2, y: feeLabel!.bottom()+20*heightScale, width: ScreenWidth - 2*x, height: 20)
        slider.value = 0
        slider.maximumTrackTintColor = UIColor.init(white: 0.5, alpha: 0.5)
        slider.minimumTrackTintColor = UIColor.RGBHex(0xD65355)
        slider.setThumbImage(UIImage.init(named: "slider2"), for: .normal)
        slider.addTarget(self, action: #selector(silderEvent(obj: )), for: .valueChanged)
        view.addSubview(slider)
        
        let slow = UILabel.init(frame: CGRect(x: x, y: slider.bottom() + 24*heightScale, width:40, height: 18*widthScale))
        slow.textColor = UIColor.RGBHex(0xA69A9B)
        slow.text = "慢".local
        slow.font = UIFont.init(name: "Helvetica", size: 12)
        view.addSubview(slow)
        
        let fast = UILabel.init(frame: CGRect(x: feeLabel!.right() - 40, y: slider.bottom() + 24, width:40, height: 18))
        fast.textColor = UIColor.RGBHex(0xA69A9B)
        fast.text = "快".local
        fast.textAlignment = .right
        fast.font = UIFont.init(name: "Helvetica", size: 12)
        view.addSubview(fast)
        
        
        let sureBtn = UIButton.init(frame: CGRect(x: x, y: ScreenHeight - 44 - x, width: ScreenWidth - 2*x, height: 44))
        sureBtn.layer.cornerRadius = 6
        sureBtn.setBackgroundImage(UIImage.init(named: "wallet_button_normal"), for: .normal)
        sureBtn.setTitle("确认转账".local, for: .normal)
        sureBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        sureBtn.addTarget(self, action: #selector(sureTransfer), for: .touchUpInside)
        view.addSubview(sureBtn)
        
    }
    
    fileprivate func setupContentView(frame:CGRect,heightScale:CGFloat,widthScale:CGFloat)->UIView{
        let contentView = UIView.init(frame: frame)
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 6
        contentView.layer.shadowOpacity = 0.2
        contentView.layer.shadowRadius = 3
        view.addSubview(contentView)
        
        
        let x = 20 * widthScale
        let tfHeight1 = contentView.height()*0.315
        let tfHeight2 = contentView.height()*0.25
        let tfHeight3 = contentView.height()*0.435
        walletAddressTf = UITextField.init(frame: CGRect(x: x, y: 0, width: contentView.width()-2*x-5, height: tfHeight1))
        walletAddressTf?.placeholder = "收款人钱包地址".local
        walletAddressTf?.font = UIFont.systemFont(ofSize: 16)
        contentView.addSubview(walletAddressTf!)
        
        let line1 = UIView.init(frame: CGRect(x: x, y: walletAddressTf!.bottom(), width: walletAddressTf!.width(), height: 0.5))
        line1.backgroundColor = UIColor.init(white: 0, alpha: 0.15)
        contentView.addSubview(line1)
        
        moneyTf = UITextField.init(frame: CGRect(x: x, y: line1.bottom(), width: contentView.width()-2*x, height: tfHeight2))
        moneyTf?.placeholder = "转账余额".local
        moneyTf?.keyboardType = .decimalPad
        moneyTf?.font = UIFont.systemFont(ofSize: 16)
        contentView.addSubview(moneyTf!)
        
        let line2 = UIView.init(frame: CGRect(x: x, y: moneyTf!.bottom(), width: walletAddressTf!.width(), height: 0.5))
        line2.backgroundColor = UIColor.init(white: 0, alpha: 0.15)
        contentView.addSubview(line2)
        
        
        let v = UIView.init(frame: CGRect(x: x-3.5, y: line2.bottom()+4, width: contentView.width()-2*x+8, height: tfHeight3-12))
        v.clipsToBounds = true
        contentView.addSubview(v)
        
        msgTf = CLMyTextView.init(frame: CGRect(x:0, y: -1, width: contentView.width()-2*x+9, height: tfHeight3-10))
        msgTf?.placeHolder = "备注信息（非必填）".local as NSString
        msgTf?.font = UIFont.systemFont(ofSize: 16)
        v.addSubview(msgTf!)
        
        walletAddressTf?.delegate = self
        walletAddressTf?.returnKeyType = .done
        walletAddressTf?.tag = 0
        moneyTf?.tag = 1
        self.addTextFieldRightView(tf: walletAddressTf!, rightViewFrame: CGRect(x: 0, y: 0, width: 44, height: walletAddressTf!.height()))
        moneyTf?.delegate = self
        msgTf?.delegate = self
        msgTf?.returnKeyType = .done
        
        return contentView
    }
    
    fileprivate func addTextFieldRightView(tf:UITextField,rightViewFrame:CGRect,title:String? = nil){
        let btn = UIButton.init(frame:rightViewFrame)
        if title != nil{
            btn.setTitle(title, for: .normal)
            btn.setTitleColor(.black, for: .normal)
            btn.titleLabel?.font = UIFont.systemFont(ofSize: 13)
            btn.contentHorizontalAlignment = .center
        }else{
            btn.setImage(UIImage.init(named: "transfer_scanner"), for: .normal)
            btn.contentHorizontalAlignment = .right
        }
        tf.rightView = btn
        btn.tag = tf.tag
        tf.rightViewMode = .always
        btn.addTarget(self, action: #selector(clickRight(btn:)), for: .touchUpInside)
        
    }
    
    @objc fileprivate func sureTransfer(){
        
        let account = AccountManager.getAccount()
        if account.regId.count == 0{
            let alertView = UIAlertController.init(title: "提示", message: "系统检测到您尚未激活钱包，请先激活", preferredStyle: UIAlertControllerStyle.alert)
            let ok = UIAlertAction.init(title: "激活", style: .default, handler: {[weak self] (action) in
                AlertInputPwdView.shared.show().sureBlock = {[weak self] pwd in
                    //先获取高度，再激活钱包
                    LHRequest.getVaildHeight { (height, address, appid) in
                        self?.actrivateWallet(height,pwd: pwd)
                    }
                }
            })
            alertView.addAction(ok)
            self.present(alertView, animated: true, completion: nil)
            return
        }
        
        let tAddress = walletAddressTf?.text
        var amount = moneyTf?.text
        if tAddress == nil || tAddress == "" {
            self.addTextHUD(text: "请输入钱包地址".local)
            return
        }else if tAddress?.first != "W"{
            self.addTextHUD(text: "钱包地址不正确".local)
            
            return
        }
        
        if amount == nil || amount == "" {
            self.addTextHUD(text: "请输入转账金额".local, type: 2)
            return
        }
        
        if amount == "." {
            amount = "0.0"
        }
        
        if (amount?.toFloat())! < CGFloat(0.001){
            self.addTextHUD(text: "最低转账金额为0.001".local, type: 2)
            return
        }
        
        
        let amountF:Double = Double(amount!)!
        var valueAmount:Double = Double(moneyLabel!.text!)!
        if amountF == 0 {
            UILabel.showFalureHUD(text: "转账金额不能为0".local)
            
            return;
        }
        
        if tAddress == AccountManager.getAccount().address{
            UILabel.showFalureHUD(text: "转账地址不能与本地地址相同".local)
            
            return
        }
        let isValid:Bool = Bridge.addressIsAble(tAddress)
        if isValid {
            if amountF <= valueAmount {
                
                valueAmount -= amountF
                
                var fee : Double = 0
                if slider.value == 0{
                    fee = 0.01
                }else{
                    fee = Double(slider.value) * 0.04+0.01
                }
                if (valueAmount >= fee) {
                    alertView(title: "请输入密码".local)
                }else{
                    UILabel.showFalureHUD(text: "当前余额不足".local)
                }
            }else{
                UILabel.showFalureHUD(text: "当前余额不足".local)
            }
        }else{
            UILabel.showFalureHUD(text: "收款地址无效".local)
        }
    }
    
    
    
    func alertView(title:String){
        
        let widthScale = ScreenWidth/375
        let x = 24 * widthScale
        
        let contentWidth = widthScale * 327
        let contentheight = contentWidth * 0.73
        
        contentV = InputPwdView.init(frame: CGRect(x: x , y: ScreenHeight/2-contentheight/2, width: contentWidth , height: contentheight), title: title)
        contentV?.backgroundColor = UIColor.white
        contentV?.layer.cornerRadius = 10
        contentV?.alertDelegate = self
        self.contentV?.transform = CGAffineTransform.init(scaleX: 0, y: 0)
        
        
        AlertManager.shared.showAlert(contenView: self.contentV!, onView: UIApplication.shared.keyWindow!,headImg: UIImageView())
        contentV?.scrollRectToVisible(CGRect(x: 0, y: 0, width: contentV!.width(), height: contentV!.height()), animated: true)
        
    }
    
    
    func actrivateWallet(_ height:Double,pwd:String){
        
        let amount = moneyLabel?.text
        if amount?.toFloat() == 0 {
            let aler = UIAlertView.init(title: "提示", message: "账户余额为0，无法激活", delegate: nil, cancelButtonTitle: "我知道了")
            aler.show()
            return
        }
        
        if (amount?.toFloat())! < CGFloat(0.001){
            let aler = UIAlertView.init(title: "提示", message: "最低转账金额为0.001", delegate: nil, cancelButtonTitle: "我知道了")
            aler.show()
            return
        }
        
        let acccount = AccountManager.getAccount()
        let path:String = httpPath(path: HTTPPath.激活地址_P)
        let mHash = acccount.mHash
        let helpStr = acccount.getHelpString(password: pwd)
        let signHex = Bridge.getActrivateHex(withHelpStr: helpStr, withPassword: pwd, fees: 0.0001, validHeight: height)
        
        let requestPath = path + mHash
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        var pa:[String:Any] = [:]
        
        pa["signHex"] = signHex
        pa["txRemark"] = ""
        
        LHRequest.post(url: getPath, parameters: pa,runHUD:.loading, success: { [weak self](json) in
            if let dic = json.dictionary{
                if let status = dic["status"]?.intValue{
                    if status != 1 {
                        let aler = UIAlertView.init(title: "提示", message: "激活失败", delegate: nil, cancelButtonTitle: "我知道了")
                        aler.show()
                    }else{
                        
                        acccount.regId = " "
                        AccountManager.saveAccount(account: acccount)
                        
                        let aler = UIAlertView.init(title: "提示", message: "激活成功", delegate: nil, cancelButtonTitle: "我知道了")
                        aler.show()
                        //                        self?.perform(#selector(self?.delayGetRegId), with: nil, afterDelay: 30)
                    }
                }
            }
            
        }) {() in
            let aler = UIAlertView.init(title: "提示", message: "激活失败", delegate: nil, cancelButtonTitle: "我知道了")
            aler.show()
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        moneyTf?.resignFirstResponder()
        walletAddressTf?.resignFirstResponder()
        msgTf?.resignFirstResponder()
    }
}


extension MyTransferTokenC:InputPwdViewDelegate{
    func closeView() {
        self.contentV?.removeFromSuperview()
        self.contentV?.removeAllSubviews()
    }
    
    func sureBtn(obj: InputPwdView, pwdTf: UITextField) {
        ///开始转账
        let amount = moneyTf?.text
        let amountF:Double = Double(amount!)!
        let fees = Double(slider.value) * 0.04+0.01
        
        if fees > AccountManager.getAccount().wiccSumAmount{
            UILabel.showFalureHUD(text: "WICC余额不足".local)
            return
        }
        if amountF > AccountManager.getAccount().spcSumAmount {
            UILabel.showFalureHUD(text: coinName+"余额不足".local)
            return
        }
        
        self.startTransfer(fees,(walletAddressTf?.text)!,amountF,pwdTf.text!)
        obj.exitAlert()
    }
    
    func keyboradShow() {
        
        UIView.animate(withDuration: 0.3) {
            if ScreenHeight == 812{
                self.contentV?.frame.origin.y = ScreenHeight-330-self.contentV!.height()
                
            }else{
                self.contentV?.frame.origin.y = ScreenHeight-271-self.contentV!.height()
            }
        }
    }
    
    func keyboradHide() {
        UIView.animate(withDuration: 0.3) {
            self.contentV?.frame.origin.y = ScreenHeight/2-self.contentV!.height()/2
        }
    }
    
    //确定转账
    func startTransfer(_ fees:Double,_ destAddr:String,_ transferM:Double,_ pwd:String){
        LHRequest.getVaildHeight(symbol: coinName) { (vaildHeight, _, appid) in
            //签名，从api中获取
            let account = AccountManager.getAccount()
            let helpString = account.getHelpString(password: pwd)
            var hex:String! = ""
            hex = Bridge.getTransfetSPCHex(withHelpStr: helpString, withPassword: pwd, fees: fees, validHeight: vaildHeight, srcRegId: account.regId, appId: appid, destAddr: destAddr, transferValue: transferM)

            let remarkStr = self.msgTf?.text
            self.initiateTransfer(hex:hex,remark:remarkStr!)
        }
        
    }
    //发起转账
    func initiateTransfer(hex:String,remark:String){
        
        let path:String = httpPath(path: HTTPPath.发起转账交易_P)
        let mhash = AccountManager.getAccount().mHash
        let requestPath = path + mhash + "/" + coinName// coinName //"SPC"
                //utf8编码
        //let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        var pa:[String:Any] = [:]
        pa["signHex"] = hex
        pa["txRemark"] = remark
        LHRequest.post(url: requestPath, parameters: pa,runHUD: .loading, success: { [weak self](json) in
            //UmengEvent.eventWithDic(name: "zhuanzhang_success")
            self?.addTextHUD(text: "转账成功".local, type: 2)
            self?.perform(#selector(self?.popViewController), with: nil, afterDelay: 1.0)
        }) {[weak self] () in
            self?.addTextHUD(text: "转账失败，请重试".local, type: 2)
        }
        
    }
    @objc func popViewController(){
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension MyTransferTokenC:UITextFieldDelegate,UITextViewDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == moneyTf {
            let flag =  string.formartInputMoney(inputMoney: textField.text!, inputCharacter: string)
            
            if flag == false{
                return false
            }
            let str:NSString = textField.text! as NSString
            
            let showStr = str.replacingCharacters(in: range, with: string)
            let strArr = showStr.components(separatedBy: ".")
            
            if((strArr.last?.count)! > 8  ){
                //最大输入8位数
            }else{
                textField.text = showStr
                
                if let a = Double(showStr){
                    if a > self.tempMoney{
                        if self.tempMoney - 0.000016 < 0{
                            self.tempMoney = 0.00
                        }else{
                            self.moneyTf?.text = String.init(format: "%.8f",self.tempMoney - 0.000016 )
                            UILabel.showFalureHUD(text: "您只能转出" + (self.moneyTf?.text)!)
                        }
                        
                    }
                }
                
            }
            
            
            return false
        }else if textField == msgTf{
            if ((textField.text?.count)! + string.count) > 20{
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            textView.resignFirstResponder()
            return false
        }
        let str:NSString = textView.text! as NSString
        let showStr = str.replacingCharacters(in: range, with: text)
        if showStr.lengthOfBytes(using: .utf8) > 60{
            UILabel.showFalureHUD(text: "输入长度超过限制".local)
            return false
        }
        
        return true
    }
    
    ///slider滑块相应事件
    @objc func silderEvent(obj:UISlider){
        var  cf:Double! = 0
        cf = Double(Double(obj.value) * 0.04+0.01)
        tempMoney = AccountManager.getAccount().wiccSumAmount - cf
        // 小费更新
        let attrStr = NSMutableAttributedString.init(string: String(format: "小费:".local + "  %.8f %@", cf,"WICC"))
        attrStr.addAttributes([NSAttributedStringKey.foregroundColor:UIColor.white], range: NSRange.init(location: 0, length: 3))
        feeLabel?.attributedText = attrStr
    }
}
///点击方法
extension MyTransferTokenC{
    
    @objc fileprivate func clickRight(btn:UIButton){
        if btn.tag == 0{
            ///扫一扫
            scanQRCode()
        }else if btn.tag == 1{
            ///全部转出
            
            let money = Double(self.moneyLabel!.text!)
            let fee = Double(slider.value) * 0.0499+0.0001
            let finalM = money! - fee
            self.moneyTf?.text = String.init(format: "%.8lf", finalM)
            if finalM < 0 {
                UILabel.showFalureHUD(text: "额度小于最低手续费")
                return
            }
        }
        
    }
    
    @objc fileprivate func scanQRCode(){
        if self.isRightCamera() {
            let scanner =  HMScannerController.scanner(withCardName: "", avatar: UIImage.init(named: "avatar"), completion: {
                [weak self] (stringValue:String?) in
                self?.walletAddressTf?.text = stringValue
            })
            scanner?.setTitleColor(UIColor.white, tintColor:UIColor.red)
            present(scanner!, animated: true, completion: nil)
        }else{
            //没有相机权限
            let alertC = UIAlertController(title: "提示".local, message: "尚未开启相机权限,您可以去设置中开启".local, preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "取消".local, style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "确定".local, style: .default) { (action) in
                let url =  URL(string: UIApplicationOpenSettingsURLString)
                UIApplication.shared.openURL(url!)
            }
            alertC.addAction(cancelAction)
            alertC.addAction(okAction)
            self.present(alertC, animated: true, completion: nil)
        }
    }
    
    // 相机权限
    fileprivate func isRightCamera() -> Bool {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        return authStatus != .restricted && authStatus != .denied
    }
    
}
extension MyTransferTokenC{
    // 资产汇总
    func getWealthSum(){
        
        let account = AccountManager.getAccount()
        let path:String = httpPath(path: HTTPPath.获取资产汇总_G)
        let address = account.address
        
        
        let mHash = account.mHash
        let requestPath = path + mHash + "/\(address)"
        //utf8编码
        let  getPath = requestPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        
        
        LHRequest.get(url: getPath,parameters: [:],runHUD: .none, success: {[weak self] (json) in
            print(json)
            if let dic = json.dictionary{
                if let status = dic["status"]?.int{
                    if status == 1{
                        if let arr = dic["result"]?.array{
                            if let wicc = arr.last?.dictionary{
                                if let amount = wicc["amount"]?.float{
                                    account.wiccSumAmount = (wicc["amount"]?.double)!
                                    self?.moneyLabel?.text = String(format: "%.8f",amount).removeLost0()
                                    self?.tempMoney = Double(amount - 0.0001)
                                    
                                }
                            }
                        }
                    }
                }
            }
            
        }) { (error) in }
    }
    
}
