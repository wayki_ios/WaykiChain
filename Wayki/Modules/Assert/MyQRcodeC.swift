
import UIKit


class MyQRcodeC: NavBaseVC {
    
    fileprivate var qrcodeImg : UIImageView?
    fileprivate var addressLabel : UILabel?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel?.text = "收款码".local
        addUpperBackShowImageView(imageName: "")
        configView()
        

        
    }
    fileprivate func configView(){
        
       let address = AccountManager.getAccount().address
        
        let widthScale = ScreenWidth/375
        let heightScale = ScreenHeight/667
        let imgWidth = widthScale * 180
        qrcodeImg = UIImageView.init(frame: CGRect(x: self.view.centerX()-imgWidth/2, y: 130*heightScale, width:imgWidth , height: imgWidth))
        qrcodeImg?.backgroundColor = UIColor.white
        qrcodeImg?.layer.cornerRadius = 11
        qrcodeImg?.clipsToBounds = true
        qrcodeImg?.image = UIImage.QRwithString(string: address, imageName: "")
        self.view.addSubview(qrcodeImg!)
        qrcodeImg?.isUserInteractionEnabled = true
        let longpress = UILongPressGestureRecognizer.init(target: self, action: #selector(longpressImg(gesture:)))
      
        qrcodeImg?.addGestureRecognizer(longpress)
        
  
        let label = UILabel.init(frame: CGRect(x: 0, y: qrcodeImg!.bottom()+50*heightScale, width: ScreenWidth, height: 20))
        label.text = "当前地址".local
        label.textColor = UIColor.white
        label.font = UIFont.init(name: "PingFangSC-Regular", size: 14)
        label.textAlignment = .center
        self.view.addSubview(label)
        
        addressLabel = UILabel.init(frame: CGRect(x: 10, y: label.bottom()+5*heightScale, width: ScreenWidth-20, height: 18))
        addressLabel?.text = address
        addressLabel?.textColor = UIColor.RGBHex(0xA69A9B)
        addressLabel?.font = UIFont.init(name: "PingFangSC-Regular", size: 13)
        addressLabel?.textAlignment = .center
        self.view.addSubview(addressLabel!)
        
        
        let sureBtn = UIButton.init(frame: CGRect(x: 24*widthScale, y: addressLabel!.bottom() + 50*heightScale, width: ScreenWidth - 2*24*widthScale, height: 44))
        sureBtn.layer.cornerRadius = 6
        sureBtn.setBackgroundImage(UIImage.init(named: "wallet_button_normal"), for: .normal)
        sureBtn.setTitle("复制收款地址".local, for: .normal)
        sureBtn.addTarget(self, action: #selector(copyAddress), for: .touchUpInside)
        sureBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        view.addSubview(sureBtn)
        
        let wiccBtn = UIButton.init(frame: CGRect(x: 24*widthScale, y: sureBtn.bottom() + 20*heightScale, width: ScreenWidth - 2*24*widthScale, height: 44))
        wiccBtn.layer.cornerRadius = 6
        wiccBtn.backgroundColor = UIColor.white
        wiccBtn.setTitle("获取WICC".local, for: .normal)
        wiccBtn.setTitleColor(UIColor.RGBHex(0xD93646), for: .normal)
        wiccBtn.addTarget(self, action: #selector(getWicc), for: .touchUpInside)
        wiccBtn.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        view.addSubview(wiccBtn)
        
    }


}
extension MyQRcodeC{
    
    @objc fileprivate func getWicc(){
     
    }
    
    @objc fileprivate func copyAddress(){
        UmengEvent.eventWithDic(name: "copy_address")
        UIPasteboard.general.string = addressLabel?.text
        addTextHUD(text: NSLocalizedString("地址已复制！".local, comment: "") )
    }
    
    @objc fileprivate func longpressImg(gesture:UILongPressGestureRecognizer){
        if gesture.state.rawValue == 1{
            
            let screenRect = UIScreen.main.bounds
            UIGraphicsBeginImageContext(screenRect.size)
            let ctx:CGContext = UIGraphicsGetCurrentContext()!
            self.view.layer.render(in: ctx)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext();
            
            UIImageWriteToSavedPhotosAlbum(image!,
                                           self,#selector(savedPhotosAlbum(image:didFinishSavingWithError:contextInfo:)), nil)
        }
        
    }
    
    
    @objc func savedPhotosAlbum(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: AnyObject) {
        
        if error != nil {
            addTextHUD(text:NSLocalizedString("二维码保存失败！".local, comment: "")  )
        } else {
            addTextHUD(text: NSLocalizedString("二维码已经保存！".local, comment: "") )
        }
    }
}
