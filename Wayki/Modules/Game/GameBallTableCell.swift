

import UIKit


class GameBallTableCell: FoundationCell {
    
    var timeL:UILabel!
    var priceL:UILabel!
    var rightTeamL:UILabel!
    var leftTeamL:UILabel!
    var rightTeamIV:UIImageView?
    var leftTeamIV:UIImageView?
    var competitionL:UILabel!
    
    class func getCell(table:UITableView,id:String="GameBallTableCell")->GameBallTableCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = GameBallTableCell() }
        return cell as! GameBallTableCell
    }
    
    override func buildCell(frame: CGRect){
        backgroundColor = UIColor.clear
        
        timeL = UILabel(frame: CGRect(x: 20*scale, y: 18*scale, width: 200, height: 18))
        timeL.textColor = UIColor.RGBHex(0xa199a2)
        timeL.font = UIFont(name: kFontTypeMedium, size: 14)
        addSubview(timeL)
        
        let bgdW = ScreenWidth-40*scale
        let bgdH = bgdW*(198/730)
        let bgdY = 45*scale
        let bgdV = UIImageView(frame: CGRect(x: 20*scale, y:bgdY , width: bgdW, height: bgdH))
        bgdV.image = UIImage(named: "bet_game_list_cell_bgd")
        bgdV.isUserInteractionEnabled = true
        addSubview(bgdV)
        
        
        let titleH:CGFloat = bgdH*(59/87)
        let titleW:CGFloat = bgdW/3 //110*scale-5
        
        leftTeamL = UILabel(frame: CGRect(x: 0, y: 0, width: titleW, height: titleH))
        leftTeamL.font = UIFont(name: kFontBold, size: 16)
        leftTeamL.textColor = UIColor.grayColor(v: 76)
        leftTeamL.textAlignment = .center
        bgdV.addSubview(leftTeamL)
        
        rightTeamL = UILabel(frame: CGRect(x: bgdW-titleW , y: 0, width: titleW, height: titleH))
        rightTeamL.font = UIFont(name: kFontTypeMedium, size: 16)
        rightTeamL.textAlignment = .center
        rightTeamL.textColor = UIColor.grayColor(v: 76)
        bgdV.addSubview(rightTeamL)
        
        // 图片
        let imageY:CGFloat = (titleH-40)/2
        leftTeamIV = UIImageView(frame: CGRect(x: leftTeamL.x()+leftTeamL.width(), y: imageY, width: 40, height: 40))
        bgdV.addSubview(leftTeamIV!)
        
        rightTeamIV = UIImageView(frame: CGRect(x: rightTeamL.x()-40, y: imageY, width: 40, height: 40))
        bgdV.addSubview(rightTeamIV!)
        
        if UIDevice.model() == "5" {
            let titleW:CGFloat = (bgdW-50)/2
            rightTeamL.frame =  CGRect(x: 0, y: 0, width: titleW, height: titleH)
            leftTeamL.frame =  CGRect(x: bgdW-titleW , y: 0, width: titleW, height: titleH)
            leftTeamIV?.isHidden = true
            rightTeamIV?.isHidden = true
        }

        let centerH:CGFloat = 12*scale
        let centerW:CGFloat = 19*scale
        let centerY:CGFloat = (titleH-centerH)/2 + bgdY
        let centerIV = UIImageView(frame: CGRect(x: ScreenWidth/2-centerW/2, y:centerY , width: centerW, height: centerH))
        centerIV.image = UIImage(named: "game_list_vs")
        addSubview(centerIV)
        
        let footY:CGFloat = rightTeamL.y()+rightTeamL.height()
        let footH:CGFloat = bgdH*(28/87)
        priceL = UILabel(frame: CGRect(x: 10 , y: footY, width: bgdW*2/3-10, height: footH))
        priceL.font = UIFont(name: kFontTypeMedium, size: 13)
        priceL.textColor = UIColor.grayColor(v: 76)
        bgdV.addSubview(priceL)
        
        competitionL = UILabel(frame: CGRect(x: bgdW*2/3 , y: footY, width: bgdW/3, height: footH))
        competitionL.font = UIFont(name: kFontTypeMedium, size: 12)
        competitionL.textColor = UIColor.grayColor(v: 76)
        bgdV.addSubview(competitionL)
        
        
    }

}

//MARK: - 配置数据
extension GameBallTableCell{
    
    func configureData(model:GameListM) -> Self {
        timeL.text = "2018-06-01 18:30"
        leftTeamL.text = "左侧队伍名称"
        rightTeamL.text = "右侧队伍名称"
        rightTeamIV?.image = UIImage(named: "alert_lock")
        leftTeamIV?.image = UIImage(named: "alert_lock")
        priceL.attributedText = NSAttributedString.gameCellPriceString(count: 10000000)
        competitionL.text = "主队图标主队图标"
//        timeL.text = "\(model.联赛名称) \(model.比赛时间)"+NSLocalizedString("截止", comment: "")
//        leftTeamL?.text = model.主队名称
//        //if let url = URL(string: allimagePath+model.主队图标) { leftTeamIV?.kf.setImage(with: url)}
//        
//        rightTeamL?.text = model.客队名称
//        //if let url = URL(string: allimagePath+model.客队图标) { rightTeamIV?.kf.setImage(with: url)}
//        
//        let poolTitle = NSLocalizedString("总奖池:", comment: "")
//        let poolCount = "\(model.奖池金额) \(model.币符号)"
//        let poolTitleAS = NSMutableAttributedString(string: poolTitle,attributes: [NSAttributedStringKey.font: UIFont(name: kFontTypeMedium, size: 13) as Any,NSAttributedStringKey.foregroundColor:UIColor.RGB(r: 150, g: 158, b: 171) as Any  ])
//        let poolCountAS = NSMutableAttributedString(string: poolCount,attributes: [NSAttributedStringKey.font: UIFont(name: kFontTypeMedium, size: 13) as Any,NSAttributedStringKey.foregroundColor:UIColor.RGB(r: 231, g: 74, b: 64) as Any])
//        poolTitleAS.append(poolCountAS)
//        priceL?.attributedText = poolTitleAS
        return self
    }
    
    func configureBasketball(model:AppBasketballM) -> Self{
        timeL.text = model.比赛时间
        leftTeamL.text = model.主队名称
        rightTeamL.text = model.客队名称
        if let url = URL(string: allimagePath+model.主队图标) { leftTeamIV?.kf.setImage(with: url)}
        if let url = URL(string: allimagePath+model.客队图标) { rightTeamIV?.kf.setImage(with: url)}
        priceL.attributedText = NSAttributedString.gameCellPriceString(count: model.奖池金额)
        competitionL.text = model.联赛名称
        return self
    }
    
    func configureFootball(model:AppFootballM) -> Self{
        timeL.text = model.比赛时间
        leftTeamL.text = model.主队名称
        rightTeamL.text = model.客队名称
        if let url = URL(string: allimagePath+model.主队图标) { leftTeamIV?.kf.setImage(with: url)}
        if let url = URL(string: allimagePath+model.客队图标) { rightTeamIV?.kf.setImage(with: url)}
        priceL.attributedText = NSAttributedString.gameCellPriceString(count: model.奖池金额)
        competitionL.text = model.联赛名称
        return self
    }
}

