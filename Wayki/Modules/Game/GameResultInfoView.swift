

import UIKit
enum GameResultType:Int{
    case event = 0
    case other = 1

}

class GameResultInfoView: FoundationV {
    var topView:UIView?
    var bottomView:GameResultBottomView?

    var types:GameResultType?
    
    var backGroundImageView:UIImageView?
    init(frame: CGRect,type:GameResultType) {
        super.init(frame: frame)
        types = type
        layoutUI(type: type)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    private func layoutUI(type:GameResultType) {
        backGroundImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.width(), height: self.height()))
        backGroundImageView?.image = UIImage(named: "bet_detail_bgd")
        self.addSubview(backGroundImageView!)
        
        let vWidth = self.width()
        let vHeight = self.height()*88.0/430.0
        if type == .event{
            topView = GameResultEventTopView(frame: CGRect(x: 0, y: 0, width: vWidth, height: vHeight))
        }else{
            topView = GameResultOtherTopView(frame: CGRect(x: 0, y: 0, width: vWidth, height: vHeight))
        }
        self.addSubview(topView!)
        topView?.backgroundColor = UIColor.clear
        
        bottomView = GameResultBottomView(frame: CGRect(x: 0, y: vHeight, width: vWidth, height: self.height() - vHeight))
        self.addSubview(bottomView!)
        bottomView?.backgroundColor = UIColor.clear

    }
    
    func refreshData(data:GameResultModel){
        var titleArr:[[String]] = []
        var detailArr:[[String]] = []
        var dataArr1:[String] = []
        var dataArr2:[String] = []
        if types == GameResultType.event {
            let vi = topView as! GameResultEventTopView
            vi.detailLabel?.text = data.事件主体
            vi.remarkLabel?.text = data.事件依据
            titleArr = [["投注选项".local,"投注金额".local,"最终赔率".local,"开奖结果".local,"中奖金额".local],["投注时间".local,"开奖时间".local,"订单编号".local]]
            var tzxStr:String = ""
            if data.投注项 == "主胜" {
                tzxStr = data.主队名称
            }else if data.投注项 == "客胜"{
                tzxStr = data.客队名称
            }else{
                tzxStr = "--"
            }
            dataArr1.append(getShowStr(str: tzxStr))
        }else{
            let vi = topView as! GameResultOtherTopView
            vi.leftLabel?.text = data.主队名称
            vi.rightLabel?.text = data.客队名称
              // (1.待确认,2.未成功,3.未开奖,4.中奖,5.未中奖,6.已关闭)
            if data.投注状态 == 4 || data.投注状态 == 5{
                vi.vsLabel?.text = "\(data.主队得分) : \(data.客队得分)"
                vi.vsLabel?.textColor = UIColor.black
            }else{
                vi.vsLabel?.text = "- : -"
            }
            titleArr = [["场次".local,"竞猜玩法".local,"投注选项".local,"投注金额".local,"最终赔率".local,"开奖结果".local,"中奖金额".local],["投注时间".local,"开奖时间".local,"订单编号".local]]
            
            dataArr1.append(getShowStr(str: data.赛事编号))
            dataArr1.append(getShowStr(str: data.投注玩法))
            dataArr1.append(getShowStr(str: data.投注项))

        }
        
        dataArr1.append(getShowStr(str: getShowStr(str: "\(data.投注金额)") + " \(coinName)"))
        dataArr1.append(getStrWithNum(i: data.本投注项的最终赔率, type: data.投注状态,isAmount:false))
        dataArr1.append(getShowStr(str: data.开奖结果))
        var getMoneyStr = getStrWithNum(i: data.中奖金额, type: data.投注状态,isAmount:true)
        if getMoneyStr != "-" {
            getMoneyStr = getMoneyStr + coinName
        }
        dataArr1.append(getMoneyStr)
        
        dataArr2.append(getDate(dateD: data.投注时间))
        dataArr2.append(getDate(dateD: data.开奖时间))
        dataArr2.append("\(data.订单id)")

        detailArr.append(dataArr1)
        detailArr.append(dataArr2)
        bottomView?.投注状态 = data.投注状态
        bottomView?.refreshData(titleArr: titleArr, data: detailArr)
        
    }
}


extension GameResultInfoView{

    func getDate(dateD:Double) ->String{
        if dateD==0 {
            return "-"
        }else{
            return  Date(timeIntervalSince1970: TimeInterval(dateD/1000)).dateString()
        }
    }
    
    func getShowStr(str:String?) ->String{
        if str == "" {
            return "-"
        }else if str != nil{
            return str!
        }else{
            return "-"
        }
    }
    
    func getStrWithNum(i:Int,type:Int) ->String{
        //如果是1.待确认,2.未成功,3.未开奖 赔率 结果中奖金额为 --
        if type <= 3 {
            if i==0{
                return "-"
            }else{
                return String(format: "%.2f", i)
                // return "\(i)"
            }
        }else{
            return "\(i)"
        }
    }
    
    func getStrWithNum(i:Float,type:Int,isAmount:Bool) ->String{
        //如果是1.待确认,2.未成功,3.未开奖 赔率 结果中奖金额为 --
        if type <= 3 {
            if i==0{
                return "-"
            }else{
                return String(format: "%.2f", i)
                // return "\(i)"
            }
        }else if type==4{
            if isAmount{
                return String(format: "%.2f", i)
            }
            return "\(i)"
        }else{
            return "\(i)"
        }
    }
}

class GameResultEventTopView: FoundationV{
    var detailLabel:UILabel?
    var remarkLabel:UILabel?
    
    override func buildView(frame: CGRect) {
    
        self.backgroundColor = UIColor.clear
        addDetailView()
    }
    
    func addDetailView(){
        let xSpace:CGFloat = 13
        let vHeight  = self.height()*66.0/88.0
        detailLabel = UILabel(frame: CGRect(x: xSpace, y: 0, width: self.width() - 2*xSpace, height: vHeight))
        detailLabel?.textAlignment = .left
        detailLabel?.numberOfLines = 0
        detailLabel?.font = UIFont.systemFont(ofSize: 12)
        self.addSubview(detailLabel!)
        
        remarkLabel = UILabel(frame: CGRect(x: xSpace, y: (detailLabel?.bottom())!, width: self.width() - 2*xSpace, height: 15))
        remarkLabel?.textAlignment = .left
        remarkLabel?.font = UIFont.systemFont(ofSize: 10)
        remarkLabel?.textColor = UIColor.red_new()
        self.addSubview(remarkLabel!)
    }

}


class GameResultOtherTopView: FoundationV{
    var vsLabel:UILabel?
    var leftImageView:UIImageView?
    var leftLabel:UILabel?
    var rightImageView:UIImageView?
    var rightLabel:UILabel?
    override func buildView(frame: CGRect) {
        
        self.backgroundColor = UIColor.clear
        addDetailView()
    }
    
    func addDetailView(){
        let vsTop = self.width()*27.0/327.0
        let vsWidth:CGFloat = 150
        let vsHeight = self.width()*30.0/327.0
        vsLabel = UILabel(frame: CGRect(x: (self.width() - vsWidth)/2.0, y: vsTop, width: vsWidth, height: vsHeight))
        vsLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        vsLabel?.textAlignment = .center
        vsLabel?.backgroundColor = UIColor.clear
        vsLabel?.textColor = UIColor.RGBHex(0xb2b2b2)
        self.addSubview(vsLabel!)
        
//        let xSpace = self.width()*41.0/327.0
//        let imageWidth  = self.width()*41.0/327.0
//        let imageTop = self.width()*17.0/327.0
//        leftImageView = UIImageView(frame: CGRect(x: xSpace, y: imageTop, width: imageWidth, height: imageWidth))
//        self.addSubview(leftImageView!)

//        let leftLabelWidth:CGFloat = 180
//        let leftX = (xSpace + imageWidth/2.0) - leftLabelWidth/2.0
//        let labelTop = (leftImageView?.bottom())! + self.width()*6.0/327.0
//        leftLabel = UILabel()
//        leftLabel?.frame = CGRect(x: leftX, y: labelTop, width: leftLabelWidth, height: self.width()*14.0/327.0)
//        leftLabel?.font = UIFont.systemFont(ofSize: 10)
//        leftLabel?.textAlignment = .center
//        leftLabel?.backgroundColor = UIColor.clear
//        self.addSubview(leftLabel!)

//        let rightImageX = self.width() - self.width()*41.0/327.0 - self.width()*41.0/327.0
//        rightImageView = UIImageView(frame: CGRect(x: rightImageX, y: imageTop, width: imageWidth, height: imageWidth))
//        self.addSubview(rightImageView!)

//        let rightLabelX = self.width() - (xSpace + imageWidth/2.0 + leftLabelWidth/2.0)
//        rightLabel = UILabel()
//        rightLabel?.frame = CGRect(x: rightLabelX, y: labelTop, width: leftLabelWidth, height: self.width()*14.0/327.0)
//        rightLabel?.font = UIFont.systemFont(ofSize: 10)
//        rightLabel?.textAlignment = .center
//        rightLabel?.backgroundColor = UIColor.clear
//        self.addSubview(rightLabel!)
        
        
        let xSpace = self.width()*41.0/327.0
        let imageWidth  = self.width()*41.0/327.0
        
        let leftLabelWidth:CGFloat = 180
        let leftX = (xSpace + imageWidth/2.0) - leftLabelWidth/2.0
        leftLabel = UILabel()
        leftLabel?.frame = CGRect(x: leftX, y: vsTop, width: leftLabelWidth, height: vsHeight)
        leftLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        leftLabel?.textAlignment = .center
        leftLabel?.backgroundColor = UIColor.clear
        self.addSubview(leftLabel!)
        
        let rightLabelX = self.width() - (xSpace + imageWidth/2.0 + leftLabelWidth/2.0)
        rightLabel = UILabel()
        rightLabel?.frame = CGRect(x: rightLabelX, y: vsTop, width: leftLabelWidth, height: vsHeight)
        rightLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        rightLabel?.textAlignment = .center
        rightLabel?.backgroundColor = UIColor.clear
        self.addSubview(rightLabel!)
        
    }
    
}


class GameResultBottomView: FoundationV{
    var tableView:UITableView?
    var 投注状态:Int = 0
    var titileArr:[[String]] = []
    var detailArr:[[String]] = []
    override func buildView(frame: CGRect) {
        self.backgroundColor = UIColor.clear
        tableView = UITableView(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height), style: .plain)
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.rowHeight = ScreenWidth*26.0/375.0
        tableView?.register(UINib.init(nibName: "GameResultCell", bundle: nil), forCellReuseIdentifier: "GameResultCell")
        tableView?.backgroundColor = UIColor.clear
        tableView?.separatorStyle = .none
        self.addSubview(tableView!)
        tableView?.backgroundColor = UIColor.clear

        let footView = UIView()
        tableView?.tableFooterView = footView
    }
    
    func refreshData(titleArr:[[String]], data:[[String]]){
        titileArr = titleArr
        detailArr = data
        tableView?.reloadData()
    }
    
}

extension GameResultBottomView:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameResultCell") as! GameResultCell
        cell.titleLabel.text = titileArr[indexPath.section][indexPath.row]
        cell.detailLabel.text = detailArr[indexPath.section][indexPath.row]
        if (投注状态 == 4||投注状态 == 5)&&(indexPath.row==titileArr[0].count-2) {
            cell.detailLabel.textColor = UIColor.red_new()
        }else{
            cell.detailLabel.textColor = UIColor.RGBHex(0x333333)
        }
        
        cell.backgroundColor = UIColor.clear
        return cell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titileArr[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titileArr.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return UIView()
        }else{
            let vi = UIView()
            let line  = UIView()
            line.backgroundColor = UIColor.grayColor(v: 200)
            line.frame = CGRect(x: 16, y: ScreenWidth*12.0/375, width: tableView.width() - 32, height: 0.5)
            vi.addSubview(line)
            return vi
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return ScreenWidth*24.0/375.0
    }
    
}
