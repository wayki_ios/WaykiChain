

import UIKit
/// 缩放比例
let scale:CGFloat = ScreenWidth/375

enum GameType:String {
    case 篮球 = "篮球竞猜"
    case 足球 = "足球竞猜"
    case 事件 = "事件竞猜"
}

class GameListC: FoundationC {
    
    var cellAction:((Int)->Void)?
    /// 上拉加载，下拉刷新
    var action:((Int)->Void)?
    var gameType:GameType = .足球
    

    private var table:UITableView!
    private var basketballDatas:[AppBasketballM] = []
    private var footballDatas:[AppFootballM] = []
    private var eventDatas:[AppEventBetM] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildV()
        配置导航条()
        table.reloadData()

    }

    override func viewWillAppear(_ animated: Bool) {
        TableViewAnimationKit.show(with: .fall, tableView: table)
    }
}

// MARK:- 创建界面
extension GameListC{
    private func device()->String{
        return UIDevice.isX() ? "_x":""
    }
    
    func buildV(){
        view.backgroundColor = UIColor.clear
        let bgdV = UIImageView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight))
        bgdV.alpha = 0.7
        if gameType == .事件{
            if eventDatas.count == 0{
                bgdV.image = UIImage(named: "main_event_nodata"+device())
            }else{
                bgdV.image = UIImage(named: "main_event"+device())
            }
        }else if gameType == .篮球{
            if basketballDatas.count == 0{
                bgdV.image = UIImage(named: "main_basket_nodata"+device())
            }else{
                bgdV.image = UIImage(named: "main_basket"+device())
            }
        }else{
            if footballDatas.count == 0{
                bgdV.image = UIImage(named: "main_foot_nodata"+device())
            }else{
                bgdV.image = UIImage(named: "main_foot"+device())
            }
        }
        
        view.addSubview(bgdV)
        view.backgroundColor = UIColor.black
        table = UITableView(frame: CGRect(x: 0, y: naviHeight, width: ScreenWidth, height: ScreenHeight-naviHeight))
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.backgroundColor = UIColor.clear
        view.addSubview(table)
    }
    
    func setDatas(basketballData:[AppBasketballM]){
        basketballDatas = basketballData
    }
    
    func setDatas(footballData:[AppFootballM]){
        footballDatas = footballData
    }
    
    func setDatas(eventData:[AppEventBetM]){
        eventDatas = eventData
    }
    
    
    private func 配置导航条(){
 
        let titleL = UILabel(frame: CGRect(x: 0, y: naviHeight-50, width: ScreenWidth, height: 50))
        titleL.text = gameType.rawValue.local
        titleL.textColor = UIColor.white
        titleL.textAlignment = .center
        view.addSubview(titleL)
        
        let rightBtn = UIButton(frame: CGRect(x: 0, y: naviHeight-50, width: 50, height: 50))
        rightBtn.layer.cornerRadius = 15
        rightBtn.setImage(UIImage(named: "main_back"), for: .normal)
        rightBtn.setImage(UIImage(named: "main_back"), for: .highlighted)
        rightBtn.addTarget(self, action: #selector(GameListC.back), for: .touchUpInside)
        view.addSubview(rightBtn)
    }
}


// MARK:- 列表代理
extension GameListC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140*scale
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cellAction != nil{
            cellAction!(indexPath.row)
            navigationController?.popViewController(animated: true)
        }
    }
}

// MARK:- 列表数据源代理
extension GameListC:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if gameType == .事件{
            return eventDatas.count
        }else if gameType == .篮球{
            return basketballDatas.count
        }else{
            
            return footballDatas.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if gameType == .事件{
            let model:AppEventBetM = eventDatas[indexPath.row]
            return GameEventTableCell.getCell(table: tableView).configureData(model: model)
        }else if gameType == .篮球{
             let model:AppBasketballM = basketballDatas[indexPath.row]
            return GameBasketballTableCell.getCell(table: tableView).configureBasketball(model: model)
        }else{
            let model:AppFootballM = footballDatas[indexPath.row]
            return GameFootballTableCell.getCell(table: tableView).configureFootball(model: model)
        }
    }
}

// MARK:- 界面事件
extension GameListC{
    
    @objc func back(){
       navigationController?.popViewController(animated: true) // dismiss(animated: false, completion: nil)
    }
    
    @objc func loadMoreData(){ if action != nil{ action!(1)} }
    
    @objc func refreshData(){ if action != nil{action!(0)} }
    
}
