

import UIKit

class GameEventTableCell: FoundationCell {
    
    var timeL:UILabel!
    var priceL:UILabel!
    var contentL:UILabel!
    var competitionL:UILabel!
    
    class func getCell(table:UITableView,id:String="GameEventTableCell")->GameEventTableCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = GameEventTableCell() }
        
        return cell as! GameEventTableCell
    }
    
    override func buildCell(frame: CGRect){
        backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
        
        timeL = UILabel(frame: CGRect(x: 20*scale, y: 18*scale, width: 200, height: 18))
        timeL.textColor = UIColor.RGBHex(0xa199a2)
        timeL.font = UIFont(name: kFontTypeMedium, size: 12)
        addSubview(timeL)
        
        let bgdW = ScreenWidth-40*scale
        let bgdH = bgdW*(198/730)
        let bgdY = 45*scale
        let bgdV = UIImageView(frame: CGRect(x: 20*scale, y:bgdY , width: bgdW, height: bgdH))
        bgdV.image = UIImage(named: "bet_game_list_cell_bgd")
        bgdV.isUserInteractionEnabled = true
        addSubview(bgdV)
        
        
        let titleH:CGFloat = bgdH*(59/87)
        let titleW:CGFloat = bgdW-20
        
        contentL = UILabel(frame: CGRect(x: 10 , y: 0, width: titleW, height: titleH))
        contentL.font = UIFont(name: kFontTypeMedium, size: 14)
        contentL.numberOfLines = 2
        contentL.textColor = UIColor.RGBHex(0x312b2c)
        bgdV.addSubview(contentL)
        
        let footY:CGFloat = contentL.y()+contentL.height()
        let footH:CGFloat = bgdH*(28/87)
        priceL = UILabel(frame: CGRect(x: 10 , y: footY, width: bgdW*2/3-10, height: footH))
        priceL.font = UIFont(name: kFontTypeMedium, size: 13)
        priceL.textColor = UIColor.grayColor(v: 76)
        bgdV.addSubview(priceL)
        
        competitionL = UILabel(frame: CGRect(x: bgdW*2/3 , y: footY, width: bgdW/3, height: footH))
        competitionL.font = UIFont(name: kFontBold, size: 12)
        competitionL.textColor = UIColor.RGBHex(0x312b2c)
        bgdV.addSubview(competitionL)
        
    }
    
}

//MARK: - 配置数据
extension GameEventTableCell{
    
    func configureData(model:AppEventBetM) -> Self {
        timeL.text = model.截止时间
        contentL.text = model.事件主体
        
//        let subStrings = model.事件主体.split(separator: "】")
//        if subStrings.count == 2{
//            let titleStr = subStrings[0] + "】"
//            let contentStr = subStrings[1]
//            let titleAS = NSMutableAttributedString(string: String(titleStr),attributes: [NSAttributedStringKey.font: UIFont(name: kFontTypeMedium, size: 14) as Any,NSAttributedStringKey.foregroundColor:UIColor.black as Any  ])
//            let contentAS = NSMutableAttributedString(string: String(contentStr),attributes: [NSAttributedStringKey.font: UIFont(name: kFontTypeMedium, size: 14) as Any,NSAttributedStringKey.foregroundColor:UIColor.black as Any  ])
//            titleAS.append(contentAS)
//            contentL.attributedText = titleAS
//            contentL.text = String(contentStr)
//        }
        
        priceL.attributedText = NSAttributedString.gameCellPriceString(count: model.奖池金额)
        //competitionL.text = "主队图标主队图标"
        
        return self
    }
}



