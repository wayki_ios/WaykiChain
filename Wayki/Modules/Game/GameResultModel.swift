

import UIKit

class GameResultModel: NSObject {
    
    var 开奖结果:String = "" // awardResult
    var 开奖时间:Double = 0 // awardTime
    var 投注时间:Double = 0 // bettime0
    var 比赛时间:Double = 0 // bjdate
    var 投注玩法:String = "" // bettype
    var 投注项:String = "" // betvalue
    var 币种符号:String = "" // csymbol
    var 主队名称:String = "" // hometeam
    
    var isEvent:Bool = false
    var 联赛名称:String = "" // league 篮球、足球
    var 事件主体:String = "" // league 事件
    var 事件依据:String = "" // league
    
    var 赛事编号:String = "" // lnum
    var 客队名称:String = "" // visitingteam
    
    var 投注金额:Int = 0 // betmoney
    var 本投注项的最终赔率:Float = 0 // betodds
    var 投注状态:Int = 0 //betstatus (1.待确认,2.未成功,3.未开奖,4.中奖,5.未中奖,6.已关闭)
    var 中奖金额:Float = 0 // bonusmoney
    var 是否已派奖:Int = 0 // fisbonus
    var 订单id:Int = 0 // orderid
    var 主队得分:Int = 0 // homeScore
    var 客队得分:Int = 0 // visitingScore
    
    class func getModel(json:JSON)->GameResultModel{
        let model = GameResultModel()
        if let dic = json.dictionary{
            if let status = dic["status"]?.intValue{if status != 1 {return model }}
            if let result = dic["result"]?.dictionary{
                model.analysisData(json:result)
            }
        }
        return model
    }
    
    func analysisData(json:[String:JSON]){
        
        if let val = json["awardResult"]?.string{ 开奖结果 = val }
        if let val = json["awardTime"]?.double{ 开奖时间 = val }
        if let val = json["bettime"]?.double{ 投注时间 = val}
        if let val = json["bjdate"]?.double{ 比赛时间 = val }
        if let val = json["bettype"]?.string{ 投注玩法 = val }
        if let val = json["betvalue"]?.string{ 投注项 = val}
        
        if let val = json["csymbol"]?.string{ 币种符号 = val }
        if let val = json["hometeam"]?.string{ 主队名称 = val }
        if let val = json["league"]?.string{
            if val.contains("【"){
                isEvent = true
                事件主体 = val
            }else{
                isEvent = false
                联赛名称 = val
            }
        }
        if let val = json["lnum"]?.string{ 事件依据 = val }
        if let val = json["lnum"]?.string{ 赛事编号 = val}
        if let val = json["visitingteam"]?.string{ 客队名称 = val}
        
        if let val = json["betmoney"]?.int{ 投注金额 = val }
        if let val = json["betodds"]?.float{ 本投注项的最终赔率 = val }
        if let val = json["betstatus"]?.int{ 投注状态 = val }
        if let val = json["bonusmoney"]?.float{ 中奖金额 = val }
        if let val = json["fisbonus"]?.int{ 是否已派奖 = val }
        if let val = json["orderid"]?.int{ 订单id = val }
        if let val = json["homeScore"]?.int{ 主队得分 = val }
        if let val = json["visitingScore"]?.int{ 客队得分 = val }
        
    }
    
}
