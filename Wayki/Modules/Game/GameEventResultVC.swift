

import UIKit

class GameEventResultVC: NavBaseVC {
    
    var dataModel:GameResultModel? =  GameResultModel()
    var type:GameResultType? = .other
    
    var statusLabel:UILabel?

    var showBackView:UIView?
    var showVi:GameResultInfoView?
    var nextBtn:UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


extension GameEventResultVC{
    func layoutUI() {
        titleLabel?.text = "我的竞猜".local
        addUpperBackShowImageView(imageName: "wallet_header_bgd_short")
        addBV()
        addStatusLabel()
        addBtn()
    }
    
    func addStatusLabel(){
        let lTop = naviHeight
        let lHeight = (showImageView?.bottom())! - ScreenWidth*30.0/375.0 - lTop
        statusLabel = UILabel(frame: CGRect(x: 0, y: lTop, width: ScreenWidth, height: lHeight))
        statusLabel?.textAlignment = .center
        statusLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        statusLabel?.textColor = UIColor.white
        self.view.addSubview(statusLabel!)
    }
    
    func addBV(){
        let vWidth = ScreenWidth*327.0/375.0
        let vHeight = ScreenWidth*430.0/375.0
        let xSpace = (ScreenWidth - vWidth)/2.0
        let vTop = (showImageView?.bottom())! - ScreenWidth*30.0/375.0

        showBackView = UIView(frame: CGRect(x: xSpace, y: vTop, width: vWidth, height: vHeight))
        showBackView?.backgroundColor = UIColor.white
        showBackView?.layer.cornerRadius = 6
        showBackView?.clipsToBounds = true
        self.view.addSubview(showBackView!)
    
        
    }
    
    func addBtn(){
        let btnWidth = ScreenWidth*327.0/375.0
        let btnHeight = btnWidth*44.0/327.0
        let btnTop = (ScreenHeight - (showBackView?.bottom())! - btnHeight)/2.0 + (showBackView?.bottom())!
        
        nextBtn = UIButton(type:.custom)
        nextBtn?.frame = CGRect(x: (ScreenWidth - btnWidth)/2.0, y: btnTop, width: btnWidth, height: btnHeight)
        nextBtn?.setTitle("继续竞猜".local, for: .normal)
        nextBtn?.setBackgroundImage(UIImage(named: "wallet_button_normal"), for: .normal)
        nextBtn?.addTarget(self, action: #selector(complete), for: .touchUpInside)
        self.view.addSubview(nextBtn!)
    }
}

//MARK: - event
extension GameEventResultVC{
    @objc func complete(){
        let currentCIndex = navigationController?.viewControllers.count
        let viewCs = navigationController?.viewControllers
        navigationController?.popToViewController(viewCs![currentCIndex!-1-2], animated: true)
    }
}

//MARK: - Datas
extension GameEventResultVC{
    func refreshUI(data:GameResultModel,resultType:GameResultType){
        if showVi?.superview == showBackView{
            showVi?.removeFromSuperview()
        }
        type = resultType
        self.view.backgroundColor = .red
        showVi = GameResultInfoView(frame: CGRect(x: 0, y: 0, width: (showBackView?.width())!, height: (showBackView?.height())!), type: type!)
 
        showBackView?.addSubview(showVi!)
        
        showVi?.refreshData(data: data)
        changeStatusLabel(result: data.投注状态, amount: data.中奖金额)

    }
    
    func changeStatusLabel(result:Int,amount:Float) -> Void {
        switch result {
        case 1:
            statusLabel?.text = "待确定".local
        case 2:
            statusLabel?.text = "未成功".local
        case 3:
            statusLabel?.text = "待开奖".local

        case 4:
            let amountS = getStrWithNum(i: amount, type: 4, isAmount: true) + " \(coinName)"
            statusLabel?.text = "中奖 ".local + amountS
        case 5:
            statusLabel?.text = "未中奖".local
        case 6:
            statusLabel?.text = "竞猜取消".local

        default:
            break
        }
        
    }
    
    func getStrWithNum(i:Float,type:Int,isAmount:Bool) ->String{
        //如果是1.待确认,2.未成功,3.未开奖 赔率 结果中奖金额为 --
        if type <= 3 {
            if i==0{
                return "-"
            }else{
                return String(format: "%.2f", i)
                // return "\(i)"
            }
        }else if type==4{
            if isAmount{
                return String(format: "%.2f", i)
            }
            return "\(i)"
        }else{
            return "\(i)"
        }
    }
}
