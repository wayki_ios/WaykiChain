//
//  GameBasketballTableCell.swift
//  WaykiChain
//
//  Created by louis on 2018/6/5.
//  Copyright © 2018年 wk. All rights reserved.
//

import UIKit

class GameBasketballTableCell: FoundationCell {
    
    var timeL:UILabel!
    var priceL:UILabel!
    var rightTeamL:UILabel!
    var leftTeamL:UILabel!
    var rightTeamIV:UIImageView?
    var leftTeamIV:UIImageView?
    var siteImageView:UIImageView?
    var signImageView:UIImageView?
    
    var competitionL:UILabel!
    
    class func getCell(table:UITableView,id:String="GameBasketballTableCell")->GameBasketballTableCell{
        var cell = table.dequeueReusableCell(withIdentifier: id)
        if cell == nil{ cell = GameBasketballTableCell() }
        return cell as! GameBasketballTableCell
    }
    
    override func buildCell(frame: CGRect){
        backgroundColor = UIColor.clear
        let titleL = UILabel(frame: CGRect(x: 0, y: 10, width: 90*scale, height: 40))
        titleL.font = UIFont(name: kFontBold, size: 16)
        titleL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.6)
        titleL.textAlignment = .center
        addSubview(titleL)
        
        let bgdW = 266*scale
        let bgdH = bgdW*126/532
        let bgdY = 32*scale
        let bgdX = 101*scale
        let bgdV = UIImageView(frame: CGRect(x:bgdX, y:bgdY , width: bgdW, height: bgdH))
        bgdV.image = UIImage(named: "bet_list_basket_ground")
        bgdV.isUserInteractionEnabled = true
        siteImageView = bgdV
        addSubview(bgdV)
        
        
        let imageY:CGFloat = -10*scale
        leftTeamIV = UIImageView(frame: CGRect(x:38.5*scale, y: imageY, width: 40*scale, height: 40*scale))
        bgdV.addSubview(leftTeamIV!)
        rightTeamIV = UIImageView(frame: CGRect(x: bgdV.width()-79.5*scale, y: imageY, width: 40*scale, height: 40*scale))
        bgdV.addSubview(rightTeamIV!)
        
        // --- //
        let teamLabelHeight = 32*scale
        
        leftTeamL = UILabel()
        leftTeamL.bounds = CGRect(x: 0, y: 0, width: bgdV.width()/2, height: teamLabelHeight)
        leftTeamL.center = CGPoint(x: (leftTeamIV?.center.x)!, y: bgdV.height() + teamLabelHeight/2.0)
        leftTeamL.font = UIFont(name: "Helvetica", size: 13)
        leftTeamL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.6)
        leftTeamL.textAlignment = .center
        bgdV.addSubview(leftTeamL)
        
        rightTeamL = UILabel()
        rightTeamL.bounds = CGRect(x: 0, y: 0, width: bgdV.width()/2, height: teamLabelHeight)
        rightTeamL.center = CGPoint(x: (rightTeamIV?.center.x)!, y: bgdV.height() + teamLabelHeight/2.0)
        rightTeamL.font = UIFont(name: "Helvetica", size: 13)
        rightTeamL.textAlignment = .center
        rightTeamL.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.6)
        bgdV.addSubview(rightTeamL)
        
        competitionL = UILabel()
        competitionL.frame = CGRect(x: bgdV.width()/2.0 - 50*scale, y: 0, width: 100*scale, height: 19*scale)
        competitionL.font = UIFont(name: "PingFangSC-Medium", size: 10)
        competitionL.textColor = UIColor.RGB(r: 0, g: 0, b: 0, alpha: 0.8)
        competitionL.textAlignment = .center
        bgdV.addSubview(competitionL)
        
        addLeftView()
        
    }
    
    func addLeftView(){
        let leftSpace = 14*scale
        let dateTLabel = UILabel(frame: CGRect(x: leftSpace, y: 25*scale, width: 87*scale, height: 14*scale))
        dateTLabel.font = UIFont(name: "PingFangSC-Medium", size: 10)
        dateTLabel.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.5)
        dateTLabel.textAlignment = .left
        dateTLabel.text = "截止时间".local
        addSubview(dateTLabel)
        
        timeL = UILabel(frame: CGRect(x: leftSpace, y: dateTLabel.bottom()+2*scale, width: 87*scale, height: 16*scale))
        timeL.textColor = UIColor.white
        timeL.font = UIFont(name: "AvenirNext-DemiBoldItalic", size: 12)
        addSubview(timeL)
        
        let sumTLabel = UILabel(frame: CGRect(x: leftSpace, y: timeL.bottom()+11*scale, width: 87*scale, height: 14*scale))
        sumTLabel.font = UIFont(name: "PingFangSC-Medium", size: 10)
        sumTLabel.textColor = UIColor.RGB(r: 255, g: 255, b: 255, alpha: 0.5)
        sumTLabel.textAlignment = .left
        sumTLabel.text = "总奖池".local + "  (\(coinName))"
        addSubview(sumTLabel)
        
        priceL = UILabel(frame: CGRect(x: leftSpace, y: sumTLabel.bottom() + 2*scale, width: 87*scale, height: 16*scale))
        priceL.textColor = UIColor.white
        priceL.font = UIFont(name: "AvenirNext-DemiBoldItalic", size: 12)
        addSubview(priceL)
        
    }
}

//MARK: - 配置数据
extension GameBasketballTableCell{
    
    func configureBasketball(model:AppBasketballM) -> Self{
        timeL.text = model.比赛时间
        leftTeamL.text = model.主队名称
        rightTeamL.text = model.客队名称
        //if let url = URL(string: allimagePath+model.主队图标) { leftTeamIV?.kf.setImage(with: url)}
        //if let url = URL(string: allimagePath+model.客队图标) { rightTeamIV?.kf.setImage(with: url)}
        if let url = URL(string: model.客队图标){ rightTeamIV?.kf.setImage(with: url, placeholder: UIImage(named: "basketball_visit_icon"), options: nil, progressBlock: nil, completionHandler: nil) }
        if let url = URL(string: model.主队图标){ leftTeamIV?.kf.setImage(with: url,placeholder: UIImage(named: "basketball_host_icon"), options: nil, progressBlock: nil, completionHandler: nil) }
        priceL.text = "\(model.奖池金额)"
        competitionL.text = model.联赛名称
        return self
    }
}

