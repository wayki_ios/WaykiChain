

import UIKit



class GameCommonCountdownView: UIView {
    private var countdownLabel:UILabel?
    var bjdate:Double = 0
    var dateFormatter:DateFormatter = DateFormatter()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
        //config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

//MARK: - UI
extension GameCommonCountdownView{
    private func layoutUI(){
        self.backgroundColor = UIColor.clear

        countdownLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.width(), height: self.height()))
        countdownLabel?.textAlignment = .center
        countdownLabel?.textColor = UIColor.white
        countdownLabel?.font = UIFont.systemFont(ofSize: 18)
        self.addSubview(countdownLabel!)
    }
    
}

//MARK: - logic

extension GameCommonCountdownView{
    func timerRepeats() {
        repeats()
    }
    
    private func config(){
        let tool = InstanceTools.instance
        tool.setTimerWithView(vi: self, selStr: "repeats")
    }
    
    @objc func repeats(){
        if bjdate == 0 { return }
        countdownLabel?.font = UIFont.systemFont(ofSize: 18)
        let bsDate = Date.gUTCFormatDate(date: Date(timeIntervalSince1970: bjdate))
        let nowDate = Date.gUTCFormatDate(date:Date())
        let distance =   bsDate.timeIntervalSince(nowDate)
        if distance <= 0 {
            countdownLabel?.text = "赛事已截止".local
            return
        }

        let hours = Int(distance/3600)
        let mins = distance.truncatingRemainder(dividingBy: 3600)/60
        let secs = distance.truncatingRemainder(dividingBy: 59)

        countdownLabel?.text = String(format: "%02d : %02.f : %02.f",hours,mins,secs)
    }
    
    func setTime(time:Double){
        bjdate = time
    }
    
}

//MARK：- 只显示截止时间，不使用倒计时
extension GameCommonCountdownView{
    func setOnlyTime(time:Double){
        bjdate = time
        countdownLabel?.font = UIFont.systemFont(ofSize: 14)
        let bsDate = Date.gUTCFormatDate(date: Date(timeIntervalSince1970: bjdate))
        let nowDate = Date.gUTCFormatDate(date:Date())
        let distance =   bsDate.timeIntervalSince(nowDate)
        if distance <= 0 {
            countdownLabel?.text = "赛事已截止".local
            return
        }
        dateFormatter.dateFormat = "MM-dd HH:mm"
        if let dateStr:String =  dateFormatter.string(from:  Date(timeIntervalSince1970: bjdate)){
            countdownLabel?.text = "截止时间".local + ": " + dateStr
        }
        
    }
    
    
}


