

import UIKit


class APPGameBetView: UIView {
    private var gType:GameType?
    var selectedAmountSum:Int = 0
    var sumNumn:Int = 0
    var betBlock:((Int,(index:Int,type:Int,bet:Int)) ->Void)?
    var closeBlock:(() ->Void)?

    var truple:(index:Int,type:Int,bet:Int)? 
    var titleText:String = ""
    
    private var backgroundView:UIImageView?
    private var topImageView:UIImageView?
    private var titleLabel:UILabel?
    
    private var showBackView:UIView?
    private var inputAmountLabel:UILabel?
    private var coinImageView:UIImageView?

    private var amountSelectedView:AmountSelectedView?
    
    private var sumLabel:UILabel?

    private var betBtn:UIButton?
    
    init(frame: CGRect,type:GameType) {
        super.init(frame: frame);
        gType = type
        layoutUI(frame: frame);

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK: - UI
extension APPGameBetView{
    func layoutUI(frame:CGRect){
        addBackView()
        addShowAmountView()
        addSumView()
        addAmountSelectedView()
        addBetBtn()
    }
    
    //背景图片，上端动画图片，取消按钮
    func addBackView(){
        backgroundView = UIImageView(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: ScreenHeight))
        var image:UIImage = UIImage(named: "main_foot")!
        if gType == GameType.事件 {
            image = UIImage(named: "main_event")!
        }else if gType == GameType.篮球{
            image = UIImage(named: "main_basket")!
        }else{
            image = UIImage(named: "main_foot")!
        }
        backgroundView?.image = image
        backgroundView?.isUserInteractionEnabled = true
        
        let btnWidth = ScreenWidth*28.0/375.0
        let btnTop = naviHeight - 35
        let closeBtn = UIButton(type: .custom)
        closeBtn.frame = CGRect(x: self.width() - btnWidth - ScreenWidth*16.0/375.0, y: btnTop, width: btnWidth, height: btnWidth)
        closeBtn.setBackgroundImage(UIImage(named: "game_bet_cancle"), for: .normal)
        closeBtn.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        backgroundView?.addSubview(closeBtn)

        let imageTop:CGFloat = 0
        let imageWidth = ScreenWidth
        let imageHeight = imageWidth*864.0/750.0
        topImageView = UIImageView(frame: CGRect(x: (ScreenWidth - imageWidth)/2.0, y: imageTop, width: imageWidth, height: imageHeight))
        topImageView?.image = UIImage(named: "game_bet_cup")
        backgroundView?.addSubview(topImageView!)
        
        let labelSpace  = scale*80
        let labelWidth = (ScreenWidth - 2*labelSpace)
        let labelTop = scale*220
        let vY = ScreenHeight/2.0 - 24
        let vHeight = vY - labelTop
        
        titleLabel = UILabel(frame: CGRect(x: labelSpace, y: labelTop, width: labelWidth, height: vHeight))
        titleLabel?.font = UIFont.systemFont(ofSize: 12)
        titleLabel?.textColor = UIColor.white
        titleLabel?.textAlignment = .center
        titleLabel?.numberOfLines = 0
        backgroundView?.addSubview(titleLabel!)
        
        
        self.addSubview(backgroundView!)
    }
    
    //选择的金额及相关图片
    func addShowAmountView(){
        let bTop = ScreenHeight/2.0 - 24
        let bWidth = ScreenWidth*185.0/375.0
        let bHeight = ScreenHeight - bTop
        showBackView = UIView(frame: CGRect(x: 0, y: bTop, width: bWidth, height: bHeight))
        showBackView?.backgroundColor = UIColor.clear
        backgroundView?.addSubview(showBackView!)
        
        inputAmountLabel = UILabel(frame: CGRect(x: 0, y: 0, width: bWidth, height: 24))
        inputAmountLabel?.textAlignment = .center
        inputAmountLabel?.font = UIFont.italicSystemFont(ofSize: 20)
        inputAmountLabel?.textColor = UIColor.white
        inputAmountLabel?.text = "+\(selectedAmountSum)"
        showBackView?.addSubview(inputAmountLabel!)
        
        let symbolLabel = UILabel(frame: CGRect(x: 0, y: 30, width: bWidth, height: 14))
        symbolLabel.textAlignment = .center
        symbolLabel.font = UIFont.italicSystemFont(ofSize: 10)
        symbolLabel.textColor = UIColor.RGBHex(0xA09DA9)
        symbolLabel.text = "\(coinName)"
        showBackView?.addSubview(symbolLabel)
        
        
        let coinsTop = symbolLabel.bottom() + ScreenWidth*30.0/375.0
        let coinsWidth = ScreenWidth*52.0/375.0
        let coinsHeight =  coinsWidth*78.0/105.0
        let coinsImageView = UIImageView(image: UIImage(named: "game_bet_coins"))
        coinsImageView.frame = CGRect(x: (bWidth - coinsWidth)/2.0, y: coinsTop, width: coinsWidth, height: coinsHeight)
        coinImageView = coinsImageView
        showBackView?.addSubview(coinsImageView)
    }
    
    //总spc及相关图片
    func addSumView(){
        let imageX = ScreenWidth*12.0/375.0
        let imageWidth:CGFloat = ScreenWidth*110.0/375.0
        let imageHeight:CGFloat = ScreenWidth*37.0/375.0
        
        var imageTop = ScreenHeight - ScreenWidth*12.0/375.0 - imageHeight
        if UIDevice.isX() {
            imageTop = ScreenHeight - ScreenWidth*32.0/375.0 - imageHeight
        }
        let sumBackView = UIView(frame: CGRect(x: imageX, y: imageTop, width: imageWidth, height: imageHeight))
        sumBackView.backgroundColor = UIColor.RGBHex(0x000000, alpha: 0.6)
        sumBackView.layer.cornerRadius = 9
        backgroundView?.addSubview(sumBackView)

        let coinImageView = UIImageView(image: UIImage(named: "game_bet_coin"))
        let coinHeight = ScreenWidth*20.0/375.0
        let coinWidth = ScreenWidth*19.0/375.0
        let coinX = ScreenWidth*10.0/375.0
        coinImageView.frame = CGRect(x: coinX, y: (imageHeight - coinHeight)/2.0, width: coinWidth, height: coinHeight)
        sumBackView.addSubview(coinImageView)
        
        
        let labelX = coinImageView.right() + ScreenWidth*12.0/375.0
        let labelWidth = imageWidth - labelX
        sumLabel = UILabel(frame: CGRect(x: labelX, y: 0, width: labelWidth, height: imageHeight))
        sumLabel?.textAlignment = .left
        sumLabel?.font = UIFont.italicSystemFont(ofSize: 11)
        sumLabel?.textColor = UIColor.white
        sumLabel?.text = "0"
        sumBackView.addSubview(sumLabel!)
    }
    
    //选择金额
    func addAmountSelectedView(){
        let vX = ScreenWidth*185.0/375.0
        let vWidth = ScreenWidth - vX
        let vHeight = ScreenWidth*280.0/375.0
        
        let vY = (showBackView?.y())! + (coinImageView?.y())! + (coinImageView?.height())!/2.0  - vHeight/2.0
        amountSelectedView = AmountSelectedView(frame: CGRect(x: vX, y: vY, width: vWidth, height: vHeight))
        amountSelectedView?.sumAmount = sumNumn
        backgroundView?.addSubview(amountSelectedView!)
        amountSelectedView?.setNumBlock = { (selectedNum) in
            self.selectedAmountSum = selectedNum
            self.setSelectedAmount(amount: selectedNum)
        }
    }
    
    //投注按钮
    func addBetBtn(){
        let btnWidth = ScreenHeight*90.0/667.0
        let btnHeight = ScreenHeight*90.0/667.0
        let btnTop = ScreenHeight - ScreenHeight*8.0/667.0 - btnHeight
        let btnX = (ScreenWidth - btnWidth)/2.0
        betBtn = UIButton(type: .custom)
        betBtn?.frame = CGRect(x: btnX, y: btnTop, width: btnWidth, height: btnHeight)
        betBtn?.setBackgroundImage(UIImage(named: "game_bet_m_s_n".local), for: .normal)
        betBtn?.setBackgroundImage(UIImage(named: "game_bet_m_s_h".local), for: .highlighted)
        betBtn?.titleEdgeInsets = UIEdgeInsets(top:  -ScreenHeight*10.0/667.0, left: 0, bottom: ScreenHeight*10.0/667.0, right: 0)
        betBtn?.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        betBtn?.addTarget(self, action: #selector(bet), for: .touchUpInside)
        backgroundView?.addSubview(betBtn!)
    }
    
}

//MARK: - event
extension APPGameBetView{
    func show() {
        let showWindow = UIApplication.shared.keyWindow
        showWindow?.endEditing(true)
        for vi in (showWindow?.subviews)!{
            if  vi.isKind(of: APPGameBetView.self) {
                for view in vi.subviews{
                    view.removeFromSuperview()
                }
                vi.removeFromSuperview()
            }
        }
        showWindow?.addSubview(self)
        
        topImageView?.frame.origin.y = 20
        topImageView?.alpha = 0.1
        titleLabel?.alpha = 0.1
        titleLabel?.frame.origin.y = (titleLabel?.frame.origin.y)! + 20
        UIView.animate(withDuration: 1, delay: 0, options: .curveEaseInOut, animations: {
            self.topImageView?.frame.origin.y = 0
            self.topImageView?.alpha = 1
            self.titleLabel?.alpha = 1
            self.titleLabel?.frame.origin.y = (self.titleLabel?.frame.origin.y)! - 20

        }, completion: nil)
        
        amountSelectedView?.selectAmountCollectionView?.animationShootOut()
        betAnimation()
    }
    
    @objc func dismiss() {
        for view in self.subviews{
            view.removeFromSuperview()
        }
        self.removeFromSuperview()
        if closeBlock != nil {
           closeBlock!()
        }
    }
    
    func setSelectedAmount(amount:Int){
        if self.sumNumn == 0{
            let showStr = coinName+"余额不足".local
            UILabel.showFalureHUD(text: showStr)
            return
        }
        selectedAmountSum = amount
        inputAmountLabel?.text = "+\(selectedAmountSum)"
    }
    
    //设置显示选项内容（足球、篮球）
    func setTitleText(typeStr:String, leftName:String,rightName:String,betTypeStr:String,betStr:String){
        let showStr = leftName + " VS " + rightName + "\n" + betTypeStr + "-" + betStr
        let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment  =  .center
        let attStr = NSAttributedString(string: showStr, attributes: [NSAttributedStringKey.paragraphStyle :paragraphStyle])
        titleLabel?.attributedText = attStr
    }
    
    //设置显示选项内容（事件）
    func setTitleText(timeStr:String, betStr:String){
        let showStr = "截止时间".local + "  " + timeStr + "\n" + betStr
        let paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment  =  .center
        let attStr = NSAttributedString(string: showStr, attributes: [NSAttributedStringKey.paragraphStyle :paragraphStyle])
        titleLabel?.attributedText = attStr
    }
    
    @objc func bet(){
        
        if #available(iOS 10.0, *) {
            let imp = UIImpactFeedbackGenerator.init(style: .light)
            imp.impactOccurred()
        }
        if sumNumn == 0{
            let showStr = coinName+"余额不足".local
            UILabel.showFalureHUD(text: showStr)
            return
        }
        
        if selectedAmountSum == 0{
            return
        }
        UmengEvent.eventWithDic(name: "bet")
        if betBlock != nil { betBlock!(selectedAmountSum,truple!) }
        dismiss()
    }
    
    //按钮动画
    func  betAnimation(){
        self.betBtn?.animationBigSmall()
    }
    
}

//MARK: - dataSet
extension APPGameBetView{
    func setSumAmount(amount:Double){
        sumNumn = Int(amount)
        amountSelectedView?.sumAmount = sumNumn
        sumLabel?.text = String(format: "%d",Int(amount))
        LHRequest.getSumWealth { [weak self] (spcCount) in
            self?.sumNumn = Int(amount)
            self?.amountSelectedView?.sumAmount = Int(amount)
            self?.sumLabel?.text = String(format: "%d",Int(amount))
        }
    }
}


class AmountSelectedView: UIView {
    var selectAmountPickView:UIPickerView?
    var selectAmountCollectionView:AppGameCollectionView?
    private var selectTitleArrs = ["","","0","10","100","1000","10000","100000","All In","",""]
    var sumAmount:Int = 0
    var selectNumAmount:Int = 0
    var setNumBlock:((Int) ->Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildView(frame: frame)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildView(frame:CGRect){
        let imageWidth = ScreenWidth*28.0/375.0
        let imageHeight = ScreenWidth*16.0/375.0
        let imageTop = frame.size.height/2.0 - imageHeight/2.0
        let arrowImageView = UIImageView(image: UIImage(named: "game_bet_arrow"))
        arrowImageView.frame = CGRect(x: 0, y: imageTop, width: imageWidth, height: imageHeight)
        self.addSubview(arrowImageView)

        let pickX = ScreenWidth*16.0/375.0 + arrowImageView.right()
        let pickWidth = frame.size.width - pickX
        let layout = APPGameLayout()
        layout.scale = 1.1
        layout.spacing = ScreenWidth*8.0/375.0
        layout.scrollDirection = .vertical
        layout.edgeInset = UIEdgeInsets(top: 0, left:0, bottom: 0, right:0)
        layout.itemSize = CGSize(width: ScreenWidth*100.0/375.0, height: ScreenWidth*50.0/375.0)
//        layout.headerReferenceSize = CGSize(width: ScreenWidth*200.0/375.0, height: ScreenWidth*50.0/375.0 + 16)
//        layout.footerReferenceSize = CGSize(width: ScreenWidth*200.0/375.0, height: ScreenWidth*50.0/375.0 + 16)

        selectAmountCollectionView = AppGameCollectionView(frame: CGRect(x: pickX, y: 0, width: pickWidth , height: frame.size.height), collectionViewLayout: layout)
        selectAmountCollectionView?.isPagingEnabled = false
        selectAmountCollectionView?.backgroundColor = UIColor.clear
        selectAmountCollectionView?.dataSource = self
        selectAmountCollectionView?.delegate = self
        selectAmountCollectionView?.showsVerticalScrollIndicator = false
        selectAmountCollectionView?.showsHorizontalScrollIndicator = false
        selectAmountCollectionView?.register(AmountCell.self, forCellWithReuseIdentifier: "AmountCell")
        selectAmountCollectionView?.register(AppCollectionViewReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader , withReuseIdentifier: "head")
        selectAmountCollectionView?.register(AppCollectionViewReusableView.self, forSupplementaryViewOfKind:UICollectionElementKindSectionFooter , withReuseIdentifier: "footer")
        selectAmountCollectionView?.decelerationRate = 0.2
        selectAmountCollectionView?.delaysContentTouches = false
        selectAmountCollectionView?.canCancelContentTouches = true
        addSubview(selectAmountCollectionView!)
        
    }
    
    func setSelectedAmount(title:String){
        let num = Int(title)
        if num == nil {
            //All in
            selectNumAmount = sumAmount
        }else if num == 0{
            selectNumAmount = 0
        }else{
            let addNum = selectNumAmount + num!
            if addNum>sumAmount{
                selectNumAmount = sumAmount
            }else{
                selectNumAmount = addNum
            }
        }
        if self.setNumBlock != nil {
            self.setNumBlock!(selectNumAmount)
        }
    }
    
    
    @objc func clickRow( btn:UIButton) {

        if #available(iOS 10.0, *) {
            let imp = UIImpactFeedbackGenerator.init(style: .light)
            imp.impactOccurred()
        } 
        clickRow(btnTag: btn.tag)
        let  cell:AmountCell = btn.superview as! AmountCell
        cell.cellDidSelected()
    }
    @objc func clickRow( btnTag:Int) {
        let title = selectTitleArrs[btnTag]

        setSelectedAmount(title: title)
        selectAmountCollectionView?.scrollToItem(at: IndexPath(item: btnTag, section: 0), at: UICollectionViewScrollPosition.centeredVertically, animated: true)
    }

}

extension AmountSelectedView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectTitleArrs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:AmountCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AmountCell", for: indexPath) as! AmountCell
        cell.setTitle(title: selectTitleArrs[indexPath.row])
        cell.backBtn?.tag = indexPath.row
        cell.backBtn?.addTarget(self, action: #selector(clickRow(btn:)), for: .touchUpInside)
        if selectTitleArrs[indexPath.row] == "" { cell.isHidden = true }else{ cell.isHidden = false }
        return cell
    }
    
}

class AmountCell: UICollectionViewCell {
    var backBtn:UIButton?
    var clickBlock:((Int) ->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backBtn = UIButton(type: .custom)
        backBtn?.frame =  CGRect(x: 0, y: 0, width: frame.width, height: frame.height)

        backBtn?.setTitle("", for: .normal)
        backBtn?.titleLabel?.font = UIFont.italicSystemFont(ofSize: 16)
        backBtn?.setTitleColor(UIColor.white, for: .normal)
        backBtn?.setBackgroundImage(UIImage(named: "betbutton_n"), for: .normal)
        backBtn?.setBackgroundImage(UIImage(named: "betbutton_h"), for: .highlighted)
        backBtn?.setBackgroundImage(UIImage(named: "betbutton_h"), for: .selected)

        self.addSubview(backBtn!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTitle(title:String) {
        let num = Int(title)
        if num == nil {
            backBtn?.setTitle(title, for: .normal)

        }else{
            if num == 0{
                backBtn?.setTitle("0", for: .normal)
            }else{
                backBtn?.setTitle("+"+title, for: .normal)
            }
        }
    }
    
    func cellDidSelected(){
        //
       //backBtn?.isHighlighted = true
        //backBtn?.setBackgroundImage(UIImage(named: "betbutton_h"), for: .normal)
    }

}


class AppCollectionViewReusableView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
