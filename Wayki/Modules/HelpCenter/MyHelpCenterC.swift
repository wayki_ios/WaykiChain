

import UIKit
import WebKit

class MyHelpCenterC: NavBaseVC {

    fileprivate var wkWeb:WKWebView? = nil
    fileprivate lazy var progressView:UIProgressView = UIProgressView.init()
    override func viewDidLoad() {
        super.viewDidLoad()
        clearData()
        
        titleLabel?.text = "帮助中心".local
        let config = WKWebViewConfiguration.init()
        wkWeb = WKWebView.init(frame: CGRect(x: 0, y: naviHeight, width: ScreenWidth, height: ScreenHeight - naviHeight), configuration: config)
        wkWeb?.load(URLRequest.init(url: URL.init(string: 帮助中心URL)!))
        self.view.addSubview(wkWeb!)

        wkWeb?.scrollView.bounces = false
        wkWeb?.scrollView.showsVerticalScrollIndicator = false
        wkWeb?.allowsBackForwardNavigationGestures = true
        wkWeb?.navigationDelegate = self

        wkWeb?.addObserver(self, forKeyPath: "estimatedProgress", options: NSKeyValueObservingOptions.new, context: nil)
        
        progressView.frame = CGRect(x: 0, y: naviHeight, width: ScreenHeight, height: 2)
        self.view.addSubview(progressView)

        
    }
    
    deinit {
        wkWeb?.removeObserver(self, forKeyPath: "estimatedProgress")
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress"{
            progressView.progress = Float(self.wkWeb!.estimatedProgress)
            print(self.wkWeb!.estimatedProgress)
        }
    }
    
    func clearData(){
        let data = WKWebsiteDataStore.allWebsiteDataTypes()
        let date = Date(timeIntervalSince1970: 0)
        WKWebsiteDataStore.default().removeData(ofTypes: data, modifiedSince: date) {
            
        }
    }
   

}
extension MyHelpCenterC:WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.progressView.alpha = 0
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.progressView.alpha = 1
        self.progressView.progress = 0
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.progressView.alpha = 0
    
    }
}
